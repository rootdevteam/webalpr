// App Libraries
import React from 'react';

// View Libraries
import {Root} from "native-base";

// Redux
import { Provider, connect } from 'react-redux';
import Store from './app/redux/Store';

// App Booting Component
import Boot from './Boot';

export default class App extends React.Component {

  render() {
    return (
        <Root>
          <Provider store={Store}>
            <ConnectedBootContainer />
          </Provider>
        </Root>
    );
  }
}

const mapStateToProps = (state) => ({
	state
});

const ConnectedBootContainer = connect(mapStateToProps,null)(Boot);

console.disableYellowBox = ['componentWillMount has been renamed'];