const Env = require('../../Env');

// Setup API Host Paths
var host;
const developmentHost = Env.host;
const productionHost = 'webalpr.herokuapp.com';

// Set host endpoints for WEB API based on running status
if (__DEV__) {
  host = developmentHost;
} else {
  host = productionHost;
}

// App URL route endpoints to WEB API
 export const Url = {
    headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
    },
    multipart_headers: {
     'Accept': 'application/json',
     'Content-Type': 'multipart/form-data'
    },
    stripe_headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    sign_up: 'http://' + host + '/api/v1/auth/',
    sign_in: 'http://' + host + '/api/v1/auth/sign_in',
    sign_out: 'http://' + host + '/api/v1/auth/sign_out',
    validate_token: 'http://' + host + '/api/v1/auth/validate_token',
    check_confirmable: 'http://' + host + '/api/v1/auth_check_confirmable',
    devices: 'http://' + host + '/api/v1/devices/',
    locations: 'http://' + host + '/api/v1/locations/',
    vehicles: 'http://' + host + '/api/v1/vehicles/',
    toll_devices_index: 'http://' + host + '/api/v1/toll_devices/',
    toll_devices_get_vehicle_history: 'http://' + host + '/api/v1/get_vehicle_history',
    toll_devices_check_declined: 'http://' + host + '/api/v1/check_declined',
    analyze: 'http://' + host + '/api/v1/analyze',
    stripe_tokens: 'https://api.stripe.com/v1/tokens',
    stripe_customers: 'https://api.stripe.com/v1/customers',
    stripe_payments: 'https://api.stripe.com/v1/payments',
    payments_submit_payment: 'http://' + host + '/api/v1/submit_payment',

};