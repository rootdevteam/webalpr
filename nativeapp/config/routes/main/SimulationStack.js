// App Navigation Libraries
import { createStackNavigator } from 'react-navigation-stack';

// Simulation Views
import SimulationIndex from '../../../app/views/main/simulation/Index';

// Simulation Router
export const SimulationStack = createStackNavigator(
  {
      SimulationIndex: {
      screen: SimulationIndex
    },
  },
  // Default route, Custom Header
  {
    initialRouteName: 'SimulationIndex',
    headerMode: 'none'
  }
);