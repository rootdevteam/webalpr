// App Navigation Libraries
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';

// Translations
import { t } from '../../../translations/services/i18n';

// Global Theme
const GlobalTheme = require('../../../app/components/stylesheets/GlobalTheme');

// App Navigation Components
import { HomeStack } from './HomeStack';
import { VehicleStack } from './VehicleStack';
import { PaymentStack } from './PaymentStack';
import { SimulationStack } from './SimulationStack';
import AboutScreen from "../../../app/views/global/About";
import SettingsScreen from "../../../app/views/global/Settings";

// Router
export const MainDrawer = createDrawerNavigator(
  {
      Home: {
          screen: HomeStack,
          navigationOptions: ({ navigation }) => ({
              title: t("Home:nav-title"),
          }),
      },
      Simulation: {
          screen: SimulationStack,
          navigationOptions: ({ navigation }) => ({
              title: t("Global:simulation"),
          }),
      },
      Vehicles: {
          screen: VehicleStack,
          navigationOptions: ({ navigation }) => ({
              title: t("Vehicle:plural"),
          }),
      },
      Payments: {
          screen: PaymentStack,
          navigationOptions: ({ navigation }) => ({
              title: t("Payment:plural"),
          }),
      },
      About: {
          screen: AboutScreen,
          navigationOptions: ({ navigation }) => ({
              title: t("About:nav-title"),
          }),
      },
      Settings: {
          screen: SettingsScreen,
          navigationOptions: ({ navigation }) => ({
              title: t("Settings:nav-title"),
          }),
      },
  },
  // Default route
  {
    initialRouteName: 'Home',
    drawerBackgroundColor: GlobalTheme.primaryBackgroundColor,
    contentOptions: {
        inactiveTintColor: GlobalTheme.secondaryTextColor,
        activeTintColor: GlobalTheme.primaryTextColor,
    }
  }
);