// App Navigation Libraries
import { createStackNavigator } from 'react-navigation-stack';

// Payments Views
import PaymentsNew from '../../../app/views/main/payments/New';
import PaymentsEdit from '../../../app/views/main/payments/Edit';
import PaymentsShow from '../../../app/views/main/payments/Show';
import PaymentsIndex from '../../../app/views/main/payments/Index';

// Payments Router
export const PaymentStack = createStackNavigator(
  {
    PaymentsNew: {
      screen: PaymentsNew
    },
    PaymentsEdit: {
      screen: PaymentsEdit
    },
    PaymentsShow: {
      screen: PaymentsShow
    },
    PaymentsIndex: {
      screen: PaymentsIndex
    },
  },
  // Default route, Custom Header
  {
    initialRouteName: 'PaymentsIndex',
    headerMode: 'none'
  }
);