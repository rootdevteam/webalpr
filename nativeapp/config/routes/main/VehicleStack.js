// App Navigation Libraries
import { createStackNavigator } from 'react-navigation-stack';

// Vehicle Views
import VehiclesNew from '../../../app/views/main/vehicles/New';
import VehiclesEdit from '../../../app/views/main/vehicles/Edit';
import VehiclesShow from '../../../app/views/main/vehicles/Show';
import VehiclesIndex from '../../../app/views/main/vehicles/Index';

// Vehicle Router
export const VehicleStack = createStackNavigator(
  {
    VehiclesNew: {
      screen: VehiclesNew
    },
    VehiclesEdit: {
      screen: VehiclesEdit
    },
    VehiclesShow: {
      screen: VehiclesShow
    },
    VehiclesIndex: {
      screen: VehiclesIndex
    }
  },
  // Default route, Custom Header
  {
    initialRouteName: 'VehiclesIndex',
    headerMode: 'none'
  }
);