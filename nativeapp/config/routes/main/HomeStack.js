// App Navigation Libraries
import { createStackNavigator } from 'react-navigation-stack';

// Screens
import HomeScreen from '../../../app/views/main/Home';

// Router
export const HomeStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen
    }
  },
  // Default route
  {
    initialRouteName: 'Home',
    headerMode: 'none'
  }
);