// App Navigation Libraries
import { createStackNavigator } from 'react-navigation-stack';

// App Navigation Components
import { VehicleStack } from './VehicleStack';
import { MainDrawer } from './MainDrawer';

// Screens
import HomeScreen from '../../../app/views/main/Home';

// Router
export const MainStack = createStackNavigator(
    {
      MainDrawer: MainDrawer
    },
    // Default route
    {
      initialRouteName: 'MainDrawer',
      headerMode: 'none'
    }
);