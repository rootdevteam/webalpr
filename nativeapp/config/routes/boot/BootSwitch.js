// App Navigation Libraries
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

// App Navigation Components
import { AuthDrawer } from '../auth/AuthDrawer';
import { MainDrawer } from '../main/MainDrawer';

// Screens
import LoadingScreen from '../../../app/views/boot/Loading';

const BootSwitch = createSwitchNavigator(
  {
    // Loading Screen route
    Loading: LoadingScreen,
    // Authentication route
    Auth: AuthDrawer,
    // App Signed In User Home route
    Main: MainDrawer
  },
  {
    initialRouteName: 'Loading'
  }
);

const AppContainer = createAppContainer(BootSwitch);
export default AppContainer;