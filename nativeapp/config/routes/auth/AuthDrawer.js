// App Navigation Libraries
import { createDrawerNavigator } from 'react-navigation-drawer';

// Translations
import { t } from '../../../translations/services/i18n';

// Global Theme
const GlobalTheme = require('../../../app/components/stylesheets/GlobalTheme');

// Screens
import WelcomeScreen from "../../../app/views/auth/Welcome";
import SignInScreen from '../../../app/views/auth/SignIn';
import SignUpScreen from '../../../app/views/auth/SignUp';
import AboutScreen from "../../../app/views/global/About";
import SettingsScreen from "../../../app/views/global/Settings";

// Router
export const AuthDrawer = createDrawerNavigator(
  {
    Welcome: {
      screen: WelcomeScreen,
      navigationOptions: ({ navigation }) => ({
        title: t("Welcome:nav-title"),
      }),
    },
    SignIn: {
      screen: SignInScreen,
        navigationOptions: ({ navigation }) => ({
            title: t("Signin:nav-title"),
        }),
    },
    SignUp: {
      screen: SignUpScreen,
        navigationOptions: ({ navigation }) => ({
            title: t("Signup:nav-title"),
        }),
    },
    About: {
      screen: AboutScreen,
        navigationOptions: ({ navigation }) => ({
            title: t("About:nav-title"),
        }),
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: ({ navigation }) => ({
          title: t("Settings:nav-title"),
      }),
    },
  },
  // Default routes
  {
    initialRouteName: 'Welcome',
    drawerBackgroundColor: GlobalTheme.primaryBackgroundColor,
      contentOptions: {
          inactiveTintColor: GlobalTheme.secondaryTextColor,
          activeTintColor: GlobalTheme.primaryTextColor,
      }
  }
);