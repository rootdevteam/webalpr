// Export Global Theme Colors

module.exports = {
  // Global theme variables
  primaryBackgroundColor: "#000000",
  secondaryBackgroundColor: "#ffffff",
  primaryTextColor: "#00b7ff",
  secondaryTextColor: "#ffffff",
  primaryBorderColor: "#00b7ff",

  // Tab color variables
  primaryTabBackgroundColor: "#002f50",
  activeTabBackgroundColor: "#008dcd",
  // Status color variables
  statusPendingTextColor: "#B3AD18",
  statusPaidTextColor: "#00AB09",
  statusRedColor: "#C70100",

  // Form text field style variables
  textFieldTextColor: "#ffffff",
  textFieldBackgroundColor: "#006c9a",
  textFieldPlaceholderColor: "#c3c3c3",

};