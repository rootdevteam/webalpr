'use strict';

// App View Libraries
var React = require('react-native');
var { Dimensions, StyleSheet } = React;

// Global Theme Variables
const GlobalTheme = require('./GlobalTheme');

var deviceWidth = Dimensions.get('window').width;

// Export Global Style Classes
module.exports = StyleSheet.create({

  // General page component variables
  vehicleImage: {
    resizeMode: "cover",
    width: null,
    flex: 1,
    borderRadius: 10,
  },
  logo: {
    width: 250,
    height: 250
  },
  buttonIcon: {
    color: GlobalTheme.primaryTextColor,
    height: 40,
    width: 40,
    borderWidth: 1,
    borderColor: GlobalTheme.primaryBorderColor,
    borderRadius: 5,
    textAlign: 'center',
    padding: 3
  },
  defaultPicker: {
    width: deviceWidth-20
  },
  defaultCard:{
    backgroundColor: GlobalTheme.primaryBackgroundColor,
    borderColor: GlobalTheme.primaryBackgroundColor
  },
  borderedCardItem:{
    height: 80,
    borderWidth: 1,
    borderColor: GlobalTheme.primaryBorderColor,
  },
  clearBorder: {
    borderWidth: 0,
    borderRadius: 0,
    borderColor: "#FFFFFF",
  },
  primaryBorder: {
    borderWidth: 1,
    borderColor: GlobalTheme.primaryBorderColor,
  },
  primaryButton: {
    backgroundColor: GlobalTheme.primaryBackgroundColor,
    borderWidth: 1,
    borderColor: GlobalTheme.primaryBorderColor,
    marginHorizontal: 40,
    marginVertical: 10,
    height: 50,
    flex: 0
  },
  closeButton: {
    backgroundColor: GlobalTheme.primaryBackgroundColor,
    alignSelf: "flex-end",
    paddingBottom: 10,
    height: 40,
    width: 40,
    flex: 0,
  },
  textButton:{
    backgroundColor: '#ffffff',
    borderWidth: 0,
    marginHorizontal: 0,
    marginVertical: 5,
    height: 25,
    flex: 0
  },
  primaryBackgroundColor: {
    backgroundColor: GlobalTheme.primaryBackgroundColor,
  },
  secondaryBackgroundColor: {
    backgroundColor: GlobalTheme.secondaryBackgroundColor,
  },
  primaryTextColor: {
    color: GlobalTheme.primaryTextColor,
  },
  secondaryTextColor: {
    color: GlobalTheme.secondaryTextColor,
  },
  primaryTabBackgroundColor: {
    backgroundColor: GlobalTheme.primaryTabBackgroundColor,
  },
  activeTabBackgroundColor: {
    backgroundColor: GlobalTheme.activeTabBackgroundColor,
  },
  statusPaidColor: {
    color: GlobalTheme.statusPaidTextColor,
  },
  statusPendingColor: {
    color: GlobalTheme.statusPendingTextColor,
  },
  baseText: {
    fontFamily: 'Cochin'
  },
  titleText: {
    fontSize: 40,
    fontWeight: 'bold'
  },
  labelText: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  closeText: {
    fontSize: 30,
    fontWeight: 'bold'
  },

  // General utility styles
  bgContainer: {
    flex: 1,
    // remove width and height to override fixed static size
    width: null,
    height: null,
  },
  contentCenter: {
    flex: 1,
    justifyContent: 'center'
  },
  contentChildCenter: {
   flex: -1
  },
  contentItemsCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  flexDirectionRow: {
    flexDirection: 'row',
    flex: 1
  },
  textCenter: {
    alignSelf: "center",
    textAlign: 'center'
  },

  // General shortcut styles

  // Side Margin
  primaryMarginHorizontal: {
    marginHorizontal: 15
  },

  // Margin Top
  mt10: {
    marginTop: 10
  },
  mt15: {
    marginTop: 15
  },
  mt20: {
    marginTop: 20
  },
  mt25: {
    marginTop: 25
  },
  mt30: {
    marginTop: 30
  },
  mt40: {
    marginTop: 40
  },

  // Margin Bottom

  mb10: {
    marginBottom: 10
  },
  mb15: {
    marginBottom: 15
  },
  mb20: {
    marginBottom: 20
  },
  mb40: {
    marginBottom: 40
  },

  // Margin Left

  ml10: {
    marginLeft: 10
  },
  ml15: {
    marginLeft: 15
  },
  ml20: {
    marginLeft: 20
  },
  ml40: {
    marginLeft: 40
  },

  // Margin Right
  mr10: {
    marginRight: 10
  },
  mr15: {
    marginRight: 15
  },
  mr20: {
    marginRight: 20
  },
  mr40: {
    marginRight: 40
  },

});