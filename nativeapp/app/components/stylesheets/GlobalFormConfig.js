'use strict';

// Global Theme & Styles
const GlobalTheme = require('./GlobalTheme');
const GlobalStyles = require('./GlobalStyles');

// Export Global Form Configuration Classes
module.exports = {

  // Form Theme styling variables
  theme: {
    backgroundColor: GlobalTheme.primaryBackgroundColor,
    textInputBgColor: GlobalTheme.textFieldBackgroundColor,
    textInputIconColor: GlobalTheme.textFieldTextColor,

    pickerBgColor: 'transparent',
    pickerColorSelected: GlobalTheme.textFieldTextColor,

    inputBorderColor: GlobalTheme.primaryBorderColor,
    borderWidth: 0,
    inputColorPlaceholder: GlobalTheme.textFieldPlaceholderColor,
    inputColor: GlobalTheme.textFieldTextColor,
    inputFontSize: 15,
    labelActiveColor: '#575757',

    changeTextInputColorOnError: true,
    textInputErrorIcon: 'close-circle',
    errorMsgColor: '#c70100',
  },

  // Form Text Field prop settings
  default_text_props: {
    placeholderTextColor: GlobalTheme.textFieldPlaceholderColor,
    style: [GlobalStyles.textCenter, {color: GlobalTheme.textFieldTextColor, backgroundColor: GlobalTheme.textFieldBackgroundColor}]
  },

  // Form Scrollview prop settings
  disableAutoScroll: {
    enableAutomaticScroll: false
  },

};

