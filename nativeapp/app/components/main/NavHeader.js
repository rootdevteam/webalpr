// App Libraries
import React from 'react';

// View Libraries
import {Button, Body, Header, Icon, Left, Right, Text, Title, View, Content} from 'native-base';

// Global
const GlobalStyles = require('../stylesheets/GlobalStyles');

// Custom Models
import Device from '../../models/Device';

export class NavHeader extends React.Component {
    constructor(props){
        super(props);

        // Required
        this.title = ((this.props.settings.title !== undefined) ? this.props.settings.title : "");
        this.navigation = this.props.settings.navigation;

        // Optional
        this.backPath = this.props.settings.backPath;
        this.newPath = this.props.settings.newPath;
        this.signOutPath = this.props.settings.signOutPath;
        this.searchPath = this.props.settings.searchPath;

        // Automatic location saving, enabled by default, set to False to disable
        this.location  = ((this.props.settings.location !== undefined) ? this.props.settings.location : true);

        // NavHeader settings constructor example for view pages
        // var previous = ((this.headerSettings != undefined) ? this.headerSettings.current : "Home");
        // this.headerSettings = {
        //     title: "Vehicles",
        //     navigation: this.props.navigation,
        //     back: "Home",
        //     onNew={() => Object.newPath(this)}
        // }
    }

    async saveDeviceLocation() {
        Device.saveLocation();
    }

    // When component loads
    componentDidMount() {
        // Check if location option enabled as current page parameter
        if (this.location == true){
            this.saveDeviceLocation();
            // Start Device location polling
            this.saveLocationTimer = setInterval(this.saveDeviceLocation, 60000);
        }
    }

    // When component unloads
    componentWillUnmount() {
        if (this.location){
            clearInterval(this.saveLocationTimer);
        }
    }

    render() {
        return (
            <View>
                <Header style={[GlobalStyles.primaryBackgroundColor,{borderBottomWidth: 0}]}>
                    <Left style={[GlobalStyles.flexDirectionRow]}>
                        { this.backPath &&
                            <Button transparent onPress={() => this.navigation.navigate(this.backPath)}>
                                <Icon name='arrow-back' style={[GlobalStyles.primaryTextColor]}/>
                            </Button>
                        }
                        { this.navigation.toggleDrawer &&
                            <Button transparent onPress={() => this.navigation.toggleDrawer()}
                                style={{marginLeft: 10}}>
                                <Icon name='menu' style={[GlobalStyles.primaryTextColor]}  />
                            </Button>
                        }
                    </Left>
                    <Body style={GlobalStyles.flexDirectionRow}>
                        <Title style={[GlobalStyles.contentItemsCenter,GlobalStyles.primaryTextColor,{fontSize: 20 }]}>{this.title}</Title>
                    </Body>
                    <Right style={GlobalStyles.flexDirectionRow}>
                        { this.searchPath &&
                           <Button transparent>
                               <Icon name='search' style={[GlobalStyles.primaryTextColor]} />
                           </Button>
                        }
                        { this.newPath &&
                            <Button transparent onPress={this.newPath}>
                                <Icon name='add' style={[GlobalStyles.primaryTextColor]} />
                            </Button>
                        }
                        { this.signOutPath &&
                            <Button transparent onPress={this.signOutPath}>
                                <Icon name='log-out' style={[GlobalStyles.primaryTextColor]} />
                            </Button>
                        }
                    </Right>
                </Header>
            </View>
        );
  }
}


