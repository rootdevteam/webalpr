// App Libraries
import React from 'react';

// View Libraries
import { SwipeListView } from 'react-native-swipe-list-view';
import { Dimensions, StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import { Button, Icon } from 'native-base';

// Global
const GlobalStyles = require('../stylesheets/GlobalStyles');

export class ListSwiper extends React.Component {
    constructor(props) {
    super(props);
      this.state = {
          basic: true,
          listData: []
      };
    };

    deleteRow(data,rowMap) {
        this.props.onDelete(data.item);
        if (rowMap[data.item.key]) {
            rowMap[data.item.key].closeRow();
        }
        const newData = [...this.state.listData];
        const prevIndex = this.state.listData.findIndex(
            item => item.key === rowKey
        );
        newData.splice(prevIndex, 1);
        this.setState({ listViewData: newData });
    }

    render() {
    return(
            <SwipeListView
                  data={this.props.dataSource}
                  leftOpenValue={75}
                  keyExtractor={(data, rowMap) => rowMap.toString()}
                  renderItem={(data, rowMap) => (

                      <TouchableHighlight
                          onPress={() => this.props.onShow(data.item)}
                          style={[styles.rowFront,GlobalStyles.secondaryBackgroundColor,GlobalStyles.primaryBorder]}
                      >
                          <View>
                              <Text style={[]}>{data.item.name}</Text>
                          </View>
                      </TouchableHighlight>

                  )}
                  rightOpenValue={-75}
                  renderHiddenItem={(data, rowMap) => (
                      <View style={styles.rowBack}>
                          <Button success onPress={() => this.props.onEdit(data.item)} style={[
                              styles.rowBtn
                          ]}>
                              <Icon active name="ios-list" />
                          </Button>
                          <Button full danger onPress={() => this.deleteRow(data,rowMap)} style={[
                              styles.rowBtn,
                              styles.rowBtnRight,
                          ]}>
                              <Icon active name="trash" />
                          </Button>
                      </View>

                  )}
            />
    );
    }

}

// Styles
const styles = StyleSheet.create({
    rowFront: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 75,
        marginTop: 10
    },
    rowBack: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 75,
        marginTop: 10
    },
    rowBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        height: 75,
        width: 75,
    },
    rowBtnRight: {
        right: 0,
    },
});