// App Libraries
import React from 'react';

// Expo
import { Notifications } from 'expo';

// Translations
import {t} from "../../../translations/services/i18n";

// View Libraries
import {Button, Body, Header, Icon, Left, Right, Text, Title, View, Content} from 'native-base';
import Modal from "react-native-modal";

// Global
const GlobalStyles = require('../stylesheets/GlobalStyles');
const GlobalTheme = require('../stylesheets/GlobalTheme');

// Custom Components
import {PaymentSettings} from "../../views/main/payments/components/PaymentSettings";

// Custom Models
import TollDevice from "../../models/TollDevice";
import UserPayment from '../../models/UserPayment';

export class PushNotifyManager extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            requestHistoryData: undefined,
            parkingNotification: undefined,
            isUserParkingNotificationModalVisible: false,
            paymentNotification: undefined,
            isUserPaymentNotificationModalVisible: false,
            userParkingPaymentAmount: undefined,
            userCurrentStripePaymentSourceType: undefined,
            userCurrentStripePaymentSource: undefined
        };

    }

    // When component loads
    componentDidMount() {
        // Handle notifications that are received or selected while the app
        // is open. If the app was closed and then opened by tapping the
        // notification (rather than just tapping the app icon to open it),
        // this function will fire on the next tick after the app starts
        // with the notification data.
        this._notificationSubscription = Notifications.addListener(this._HandleNotification);
    }

    _HandleNotification = (notification) => {
        let UserRequestHistory = notification.data.request_history;

        if(notification.data.parking_state === "entered") {
            this.setState({
                parkingNotification: notification,
                requestHistoryData: UserRequestHistory,
                isUserParkingNotificationModalVisible: true,
            });
        } else if (notification.data.parking_state === "exited") {

            this.setState({
                paymentNotification: notification,
                requestHistoryData: UserRequestHistory,
                userParkingPaymentAmount: notification.data.payment_amount,
                isUserPaymentNotificationModalVisible: true
            });

        }
    };

    onUserParkingRequest(declined){
        var requestHistoryID = this.state.requestHistoryData.id;
        TollDevice.sendUserParkingRequest(this,declined,requestHistoryID);
    }

    async onUpdateUserPaymentSource(userCurrentStripePaymentSourceType,userCurrentStripePaymentSource){
        this.setState({
            userCurrentStripePaymentSourceType: userCurrentStripePaymentSourceType,
            userCurrentStripePaymentSource: userCurrentStripePaymentSource,
        });
    }

    onUserPaymentRequest(){
        var requestHistoryID = this.state.requestHistoryData.id;
        var currentPaymentType = this.state.userCurrentStripePaymentSourceType;
        var currentPaymentSource = this.state.userCurrentStripePaymentSource;
        UserPayment.sendUserPaymentRequest(this,requestHistoryID,currentPaymentType,currentPaymentSource);
    }

    render() {
        return (
            <View>
                {this.state.parkingNotification &&
                        <Modal isVisible={this.state.isUserParkingNotificationModalVisible}>
                            <View style={[GlobalStyles.secondaryBackgroundColor,GlobalStyles.primaryBorder]}>
                                <Text>You are attempting to enter the parking. Please confirm your request.</Text>
                                <Button block onPress={() => this.onUserParkingRequest(false)}
                                        style={[GlobalStyles.primaryButton]}>
                                    <Text style={[GlobalStyles.primaryTextColor]}>{t("Global:accept")}</Text>
                                </Button>
                                <Button block onPress={() => this.onUserParkingRequest(true)}
                                        style={[GlobalStyles.primaryButton]}>
                                    <Text style={[GlobalStyles.primaryTextColor]}>{t("Global:decline")}</Text>
                                </Button>
                            </View>
                        </Modal>
                }

                {this.state.paymentNotification &&
                <Modal isVisible={this.state.isUserPaymentNotificationModalVisible}>

                    <View style={[GlobalStyles.secondaryBackgroundColor,GlobalStyles.primaryBorder]}>
                        <Text>You are attempting to exit the parking. Please confirm your payment for the amount: ${this.state.userParkingPaymentAmount}</Text>
                        <PaymentSettings
                            userStripeCustomerID={this.props.parent.props.reduxState.Auth.user.stripe_customer_id}
                            onSetUserPayment={(currentType,currentSource) => this.onUpdateUserPaymentSource(currentType,currentSource)}
                        />

                        {this.state.userCurrentStripePaymentSourceType !== "none" &&
                            <Button block onPress={() => this.onUserPaymentRequest()}
                                    style={[GlobalStyles.primaryButton]}>
                                <Text style={[GlobalStyles.primaryTextColor]}>{t("Global:pay")}</Text>
                            </Button>
                        }
                    </View>
                </Modal>
                }
            </View>
        );
    }
}
