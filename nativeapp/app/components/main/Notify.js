// App Libraries
import React from 'react';

// View Libraries
import {Toast} from "native-base";
import {Alert} from 'react-native';

// Translations
import {t} from "../../../translations/services/i18n";

// Global
const GlobalStyles = require('../stylesheets/GlobalStyles');

// Notify System
class NotifySystem {

    success(text="A success occurred",buttonText="Okay",duration=10000){
        Toast.show({
            type: 'success',
            text: text,
            buttonText: buttonText,
            position: 'top',
            duration: duration
        })
    }

    warning(text="A warning occurred",buttonText="Okay",duration=10000){
        Toast.show({
            type: 'warning',
            text: text,
            buttonText: buttonText,
            position: 'top',
            duration: duration
        })
    }

    error(text="An error occurred",buttonText="Okay",duration=10000){
        Toast.show({
            type: 'danger',
            text: text,
            buttonText: buttonText,
            position: 'top',
            duration: duration
        })
    }

    // Devise Confirmable Support
    confirmationSent(user,page,login) {
        Toast.show({
            type: 'warning',
            text: 'A confirmation link has been sent to your email!',
            buttonText: 'Resend?',
            position: 'top',
            duration: 5000,
            onClose: (reason) => {
                if (reason === 'user') {
                    Alert.alert(
                        // Title
                        'Resend Confirmation?',
                        // Message
                        'Would you like us to resend the confirmation email?',
                        [
                            {text: 'No', onPress: () => {
                                    console.log('No Pressed');
                                    user.getConfirmedStatus(page,login)
                                }
                            },
                            {text: 'Yes', onPress: () => {
                                    console.log('Yes Pressed');
                                    user.getConfirmedStatus(page,login)
                                }
                            },
                        ],
                        { cancelable: true }
                        //clicking out side of alert will not cancel
                    );
                }
                else { // reason === 'timeout'
                    user.getConfirmedStatus(page,login)
                }
            }
        });
    }

    _template_(text="Something Here",buttonText="Okay",duration=10000){
        Toast.show({
            type: 'success', // success,warning,danger
            text: text,
            buttonText: buttonText,
            //buttonStyle: [GlobalStyles.contentCenter, {backgroundColor: 'white'}],
            position: 'top',
            duration: duration,
            style: [
                //GlobalStyles.contentCenter,
                {
                    //backgroundColor: "Red"
                }
            ],
            onClose: (reason) => {
                if (reason === 'user') {
                    Alert.alert(
                        // Title
                        'Resend Confirmation?',
                        // Message
                        'Would you like us to resend the confirmation email?',
                        [
                            {text: 'No', onPress: () => {
                                    console.log('No Pressed');
                                }
                            },
                            {text: 'Yes', onPress: () => {
                                    console.log('Yes Pressed');
                                }
                            },
                        ],
                        { cancelable: true }
                        //clicking out side of alert will not cancel
                    );
                }
                else { // reason === 'timeout'
                    console.log('notify timed out');
                }
        }
        });
    }
}
const Notify = new NotifySystem();
export default Notify;