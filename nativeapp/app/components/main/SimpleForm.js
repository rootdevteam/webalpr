// App Libraries
import React from 'react';

// View Libraries
import { ScrollView } from 'react-native';
import { Button, Icon, Item, Input, Label, Text, View } from 'native-base';
import GenerateForm from './form_builder/FormBuilder';

// Translations
import {t} from "../../../translations/services/i18n";

// Global
const GlobalTheme = require('../stylesheets/GlobalTheme');
const GlobalFormConfig = require('../stylesheets/GlobalFormConfig');
const GlobalStyles = require('../stylesheets/GlobalStyles');

// Custom Components
import { ImageUpload } from './ImageUpload';

export class SimpleForm extends React.Component {
    constructor(props){
        super(props);
        // Default form fields new/edit
        this.formData = this.props.formData ? this.props.formData : undefined;

        // Alpr Support
        this.alpr = this.props.alpr ? this.props.alpr : undefined;

        // Image Field & Alpr Support
        this.state={
            imageField: this.props.imageField ? this.props.imageField : undefined,
            imageBase64: undefined,
            remoteImageField: this.props.remoteImageField ? this.props.remoteImageField : undefined,
            remoteImageEndpoint: this.props.remoteImageEndpoint ? this.props.remoteImageEndpoint : undefined,
            remoteImageUrl: undefined,
            licensePlate: undefined,
            analyzedLicensePlate: undefined
        };
    }

    componentDidMount = () => {
        this.initFormData();
    };

    initFormData(){
        // Get/Set formData from state if it exists for edit/update actions, otherwise clear it
        if (this.props.formData !== undefined){
            this.formData = this.props.formData;
            // Get/Set object Id for update actions
            if (this.formData.id !== undefined){
                this.objectId = this.formData.id;
            }

            // Get/Set Image for edit/update
            if (this.state.imageField !== undefined){
                this.state.imageBase64 = this.formData[this.state.imageField] ? this.formData[this.state.imageField] : undefined;
            }
        }
    }


    onSubmit(){
        // Get this forms values, and send them back to our parent's function
        const formValues = this.formGenerator.getValues();

        // Default base64 image field support
        if (this.state.imageField !== undefined) {
            // Set values created by custom components
            formValues[this.state.imageField] = this.state.imageBase64;
        }

        // Remote image field support
        if (this.state.remoteImageField !== undefined) {
            // Set values created by custom components
            formValues[this.state.remoteImageField] = this.state.remoteImageUrl;
        }

        this.props.onSubmit(formValues,this.objectId)
    }

    async setImageField(imageSrc){

        // If Endpoint upload to remote server, use remote url for field post
        if (this.state.remoteImageEndpoint !== undefined) {
            console.log("SUCCESS DURING USING REMOTE URL setImageField (Remote untested)", JSON.stringify(imageSrc));
            this.setState({
                remoteImageUrl: imageSrc
            });
        } else {
            console.log("SUCCESS DURING USING setImageField (Base64 only)");
            this.setState({
                imageBase64: imageSrc
            });
        }

    }

    render() {
        return (
            <ScrollView>
                {this.state.imageField && // Image Support if imageField is passed in
                    <ImageUpload
                        imageBase64={this.state.imageBase64} // Base64 permanent
                        afterUpload={(imgSrc) => this.setImageField(imgSrc)} // What to do after upload
                        remoteImageEndpoint={this.state.remoteImageEndpoint} // (Optional upload to remote server)
                        remotePayloadKey='file' // Field name
                        remoteImageUrl={this.state.remoteImageUrl} // Uploaded Image url
                    />
                }

                <GenerateForm
                    ref={(c) => {
                        this.formGenerator = c;
                    }}
                    fields={this.props.fields}
                    formData={this.formData}
                    theme = {GlobalFormConfig.theme}
                    scrollViewProps={GlobalFormConfig.disableAutoScroll}
                />

                <Button block onPress={() => this.onSubmit()} style={[GlobalStyles.primaryButton]}>
                    <Text style={[GlobalStyles.primaryTextColor]}>{this.props.buttonText}</Text>
                </Button>
            </ScrollView>
        );
    }
}
