// App Libraries
import React from 'react';

// Image Upload Requirements
import { ActivityIndicator, Alert, Dimensions, Image, Linking, Platform, StyleSheet, TouchableOpacity } from 'react-native';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';

// UID generator library
import uid from 'uuid/v4';

// App API Requests & Routes Setup
import Axios from 'axios';
import { Url } from '../../../config/api/Routes';

// View Libraries
import {Button, Content, Icon, Text, View} from 'native-base';

// Global
const GlobalStyles = require('../stylesheets/GlobalStyles');

export class ImageUpload extends React.Component{
    constructor(props){
        super(props);
        this.askPermission = this.askPermission.bind(this);
        this.showAlert = this.showAlert.bind(this);
        this.state={
            imageBase64: this.props.imageBase64 ? this.props.imageBase64 : undefined,
            remoteImageEndpoint: this.props.remoteImageEndpoint ? this.props.remoteImageEndpoint : undefined,
            remotePayloadKey: this.props.remotePayloadKey ? this.props.remotePayloadKey : undefined,
            remoteImageUrl: this.props.remoteImageUrl ? this.props.remoteImageUrl : undefined,
            token: this.props.token ? this.props.token : undefined,
            loading: false
        };
        var defaultProps = {
            onSuccess: undefined,
            onFailure: undefined,
            onStartUpload: undefined,
            alertTitle: 'Please Allow Access',
            alertMessage: [
                'This applicaton needs access to your photo library to upload images.',
                '\n\n',
                'Please go to Settings of your device and grant permissions to Photos.'
            ].join(''),
            alertNo: 'Not Now',
            alertYes: 'Settings'
        };
    }

    async askPermission() {
        // only if user allows permission to camera roll
        const { status: cameraRollPerm  } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        // On Android users are prompted every time,
        // so no need to show additional Alert
        if (cameraRollPerm !== 'granted') {
            if (Platform.OS === 'ios') this.showAlert();
            return;
        }
    }
    showAlert() {
        const { alertMessage, alertTitle, alertYes, alertNo } = this.props;
        Alert.alert(
            'Please Allow Access',
            [
                'This applicaton needs access to your photo library to upload images.',
                '\n\n',
                'Please go to Settings of your device and grant permissions to Photos.'
            ].join(''),
            [
                { text: 'Not Now', style: 'cancel' },
                { text: 'Settings', onPress: () => Linking.openURL('app-settings:') }
            ]
        );
    }

    async initUpload(page,imageOptions) {
        const self = page;
        const { status: cameraRollPerm } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        if (imageOptions === "Camera"){

            const { status: cameraPerm } = await Permissions.askAsync(Permissions.CAMERA);

            //if status not granted it will guide to grant permission from settings
            if (cameraRollPerm !== 'granted' && cameraPerm !== 'granted') {
                if (Platform.OS === 'ios') {
                    this.showAlert();
                    return;
                }
            }

            ImagePicker.launchCameraAsync({
                base64: true,
                allowsEditing: true
            }).then((file)=>{
                if(!file.cancelled){
                    this.setState({
                        loading:true
                    });

                    self.uploadImageAsync(self,file);

                }
            });
        }

        if (imageOptions === "Photopicker"){

            //if status not granted it will guide to grant permission from settings
            if (cameraRollPerm !== 'granted') {
                if (Platform.OS === 'ios') {
                    this.showAlert();
                    return;
                }
            }

            ImagePicker.launchImageLibraryAsync({
                base64: true,
                mediaTypes:'Images'
            }).then((file)=>{
                if(!file.cancelled){
                    this.setState({
                        loading:true
                    });

                    self.uploadImageAsync(self,file);

                }
            });
        }

    }

    async uploadImageAsync(page,file) {

        const base64 = 'data:image/png;base64,'+file.base64; // Use base64 in all cases

        // If Vgyme Remote upload enabled, field & end point are set, perform Vgyme upload, if 3rd Party remote server, else default to base64

        const endpoint = this.state.remoteImageEndpoint; // If Endpoint upload to remote server, set url in state as remoteImageUrl

        if (endpoint !== undefined) {
            const uri = file.uri;
            const uriParts = uri.split('.');
            const fileExt = uriParts[uriParts.length - 1];

            const payloadKey = this.state.remotePayloadKey; // Define PayloadKey here Ex. 'file'
            const formData = new FormData();

            formData.append(payloadKey, {
                uri,
                type: `image/${fileExt}`,
                name: 'image-' + uid() + '.' + fileExt
            });

            await Axios.post(endpoint, formData,{headers: Object.assign(Url.multipart_headers)}).then(function (response)
            { // ON SUCCESS
                if (response !== undefined){
                    const imageUrl = response.data.image;
                    page.props.afterUpload(imageUrl);
                    page.setState({
                        loading:false,
                        imageBase64: base64,
                        remoteImageUrl: imageUrl
                    });
                    console.log("SUCCESS DURING REMOTE ENDPOINT UPLOAD uploadImageAsync (base64 included)", JSON.stringify(response));

                    return response
                }
                else {
                    console.log("TIMEOUT DURING uploadImageAsync (base64 included)");
                }
            }).catch(function (error) {
                // Return error if SignIn is invalid
                console.log("ERROR DURING uploadImageAsync", error);
            });
        } else {
            page.props.afterUpload(base64);
            page.setState({
                loading:false,
                imageBase64: base64
            });
            console.log("SUCCESS DURING uploadImageAsync (base64 only)");
        }

    }

    render(){
        if(this.state.loading){
            return(
                <View style={GlobalStyles.contentCenter}>
                    <ActivityIndicator size="large" color="#FFFFFF" />
                </View>
            )
        }
        return(
            <Content contentContainerStyle={[GlobalStyles.contentItemsCenter]}>
                {this.state.imageBase64 ? <Image source={{uri: this.state.imageBase64, isStatic: true}} style={[GlobalStyles.vehicleImage,GlobalStyles.primaryBorder, GlobalStyles.mb20,{height: 200,width: 200}]}/>  : <Image source={{uri:'https://via.placeholder.com/200.png'}} style={[GlobalStyles.vehicleImage,GlobalStyles.primaryBorder,GlobalStyles.mb20,{height: 200,width: 200}]}/> }
                <View style={GlobalStyles.flexDirectionRow}>
                    <TouchableOpacity
                        onPress={()=>{
                            this.initUpload(this,"Camera");
                        }}
                    >
                        <Icon name='camera' style={[GlobalStyles.buttonIcon, GlobalStyles.mr20]} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={()=>{
                            this.initUpload(this,"Photopicker");
                        }}
                    >
                        <Icon name='photos' style={[GlobalStyles.buttonIcon]} />
                    </TouchableOpacity>
                </View>
            </Content>
        )
    }
}