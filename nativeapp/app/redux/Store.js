import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { AsyncStorage } from 'react-native';
import { persistStore, autoRehydrate } from 'redux-persist';
import reducers from './reducers';

const Store = createStore(reducers, compose(
    applyMiddleware(thunk, logger),
    autoRehydrate()
));

persistStore(Store, {storage: AsyncStorage});

export default Store;