import { ON_SUCCESS_LOGIN, SET_BY_STORAGE, UPDATE_ACCESS_TOKEN, SET_LANGUAGE, SET_CONFIRMED } from '../actions/types';
import { REHYDRATE } from 'redux-persist/constants'

const INITIAL_STATE = {
  connected: false,
  confirmed: true,
  token: {},
  user: {},
  language: "en"
};

const Auth = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    // Set user language
    case SET_LANGUAGE:
      return Object.assign({}, state, {
        language: action.payload.language,
        connected: action.payload.connected
      });

    // Set user confirmed
    case SET_CONFIRMED:
      return Object.assign({}, state, {
        confirmed: action.payload.confirmed
      });

    // On user login set the token & object
    case ON_SUCCESS_LOGIN:
      return Object.assign({}, state, {
        connected: true,
        token: Object.assign({}, state.token, {
          "access-token": action.payload.accessToken["access-token"],
          client: action.payload.accessToken.client,
          uid: action.payload.accessToken.uid
        }),
        user: action.payload.success_data.data.data
        });

    // On update set token & object
    case UPDATE_ACCESS_TOKEN:
      return Object.assign({}, state, {
        token: Object.assign({}, state.token, {
          "access-token": action.payload.new_token
          })
        });

    // When app opened set the last state
    case REHYDRATE:
      return {...state, ...action.payload.Auth, rehydrated: true};

    // Set app state from last state in memory/storage
    case SET_BY_STORAGE:
        return action.payload.lastState;

    default:
        return state;

  }
};

export default Auth;