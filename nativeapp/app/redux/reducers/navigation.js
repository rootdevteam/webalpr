import { MainDrawer } from '../../../config/routes/main/MainDrawer';

// we get the props of navigation and store them in redux to centralize everything
const initialState = MainDrawer.router.getStateForAction(
    MainDrawer.router.getActionForPathAndParams("Home")
);

const navigation = (state = initialState, action) => {
  const newState = MainDrawer.router.getStateForAction(action, state);
  return newState || state;
};

export default navigation;
