export const ON_SUCCESS_LOGIN = 'user_login_successful';
export const SET_BY_STORAGE = 'set_user_by_local_storage';
export const UPDATE_ACCESS_TOKEN = 'update_the_access_token';
export const SET_LANGUAGE = 'set_the_user_language';
export const SET_CONFIRMED = 'set_user_confirmed';
