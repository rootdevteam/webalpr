import { ON_SUCCESS_LOGIN, SET_BY_STORAGE, UPDATE_ACCESS_TOKEN, SET_LANGUAGE, SET_CONFIRMED } from "./types";

function authSuccess(success_data, accessToken) {
	return {
		type: ON_SUCCESS_LOGIN,
		payload: {success_data: JSON.parse(success_data), accessToken: accessToken, status: 200}
	}
}

function updateAccessToken(new_token) {
	return {
		type: UPDATE_ACCESS_TOKEN,
		payload: {new_token}
	}
}

function setLanguage(language,connected=false) {
	return {
		type: SET_LANGUAGE,
		payload: {language: language, connected: connected}
	}
}

function setConfirmed(confirmed=false) {
	return {
		type: SET_CONFIRMED,
		payload: {confirmed: confirmed}
	}
}
export { authSuccess, updateAccessToken, setLanguage, setConfirmed }

