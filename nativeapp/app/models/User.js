// App Libraries
import React from 'react';

// Async Storage (Used with Redux)
import { AsyncStorage } from 'react-native';
import { Linking } from 'expo';

// Translations
import {t} from "../../translations/services/i18n";

// App API Requests & Routes Setups
import Axios from 'axios';
import { Url } from '../../config/api/Routes';

// Globals
const GlobalFormConfig = require('../components/stylesheets/GlobalFormConfig');

// Custom Components
import Notify from '../components/main/Notify';

// Custom Models
import Device from './Device';

// User Model
class UserModel {

    // User sign in form fields
    signInFields(){
        return  [
            {
                type: 'text',
                name: 'login',
                required: true,
                label: t("User:placeholder-login"),
                props: GlobalFormConfig.default_text_props
            },
            {
                type: 'password',
                name: 'password',
                required: true,
                label: t("User:placeholder-password"),
                props: GlobalFormConfig.default_text_props
            }
        ];
    }

    // Sign In User
    newSessionRequest(page,formData) {
        // Post the request
        Axios.post(Url.sign_in, formData, {headers: Object.assign(Url.headers)}).then(function (response)
        { // ON SUCCESS

            // Devise Confirmable support
            page.props.setConfirmed(true);

            User.validateTokenRequest(page,response);
            console.log("SUCCESS DURING SIGN IN newSessionRequest", JSON.stringify(response));
        }).catch(function (response) {
            // Return error
            if(response.response){
                console.log("ERROR DURING SIGN IN newSessionRequest", JSON.stringify(response.response.data.errors));
                // Notify user
                Notify.error(response.response.data.errors[0]);
            } else{
                console.log("ERROR DURING SIGN IN newSessionRequest, request failed, check API/IP configuration.");
            }
        });
    } // End of newSessionRequest()

    // Validate User Token
    validateTokenRequest(page,response){
        // Valid token received in the headers response
        var accessToken = {
            "access-token": response.headers["access-token"],
            "client": response.headers.client,
            "uid": response.headers.uid
        };

        // Save Email incase we need it for email confirmation resend
        var email = response.data.data.email;

        // Validate token
        Axios.get(Url.validate_token, {headers: Object.assign(Url.headers, accessToken)}).then(function (response)
        { // ON SUCCESS TOKEN VALIDATION
            // Token is validated then user is connected => save user data in reduxState
            page.props.authSuccess(JSON.stringify(response), accessToken);

            // Notify user
            Notify.success(t("User:notify-signin-success"));

            // Go to the App router (signed in users)
            page.props.navigation.navigate('Main');

            // Update Device Info
            Device.saveInfo();

            console.log("SUCCESS DURING validateTokenRequest", JSON.stringify(response));
        }).catch(function (response) {
            // Return error
            if(response.response){
                // If an error occurs during validation
                page.props.navigation.navigate('SignIn');
                // Devise Confirmable support
                User.getConfirmedStatus(page,email);
                // Return error
                console.log("ERROR DURING validateTokenRequest", JSON.stringify(response.response.data.errors));
            } else{
                console.log("ERROR DURING validateTokenRequest, request failed, check API/IP configuration.");
            }
        });
    } // End of validateTokenRequest()

    // Sign Out requests
    destroySessionRequest(page){
        var accessToken = page.props.reduxState.Auth.token;

        // Sign Out User Devise Session using DELETE method
        Axios.delete(Url.sign_out, {headers: Object.assign(Url.headers, accessToken)}).then(function (response) { // ON SUCCESS
            console.log("SUCCESS DURING SIGN OUT destroySessionRequest", JSON.stringify(response));
            // Notify user
            Notify.success('You have successfully signed out!');
        })
        .catch(function (response) {
            // Return error
            if(response.response){
                // If an error occurs during sign out
                console.log("ERROR DURING SIGN OUT destroySessionRequest", JSON.stringify(response.response.data.errors));
                // Notify user
                Notify.error(response.response.data.errors[0]);
            } else{
                console.log("ERROR DURING SIGN OUT destroySessionRequest, request failed, check API/IP configuration.");
            }
        });

        // Save User Lang to Temp Var
        var language = page.props.reduxState.Auth.language;

        // Clear Device State
        AsyncStorage.clear();

        // Load User Lang from Temp Var, and set connected to false
        page.props.setLanguage(language);

        page.props.navigation.navigate('Auth');
    }// End Sign Out request

    // User sign up form fields
    signUpFields(){
        return  [
            {
                type: 'text',
                name: 'username',
                required: true,
                label: t("User:placeholder-username"),
                props: GlobalFormConfig.default_text_props
            },
            {
                type: 'text',
                name: 'email',
                required: true,
                label: t("User:placeholder-email"),
                props: GlobalFormConfig.default_text_props
            },
            {
                type: 'text',
                name: 'first_name',
                required: false,
                label: t("User:placeholder-firstname"),
                props: GlobalFormConfig.default_text_props
            },
            {
                type: 'text',
                name: 'last_name',
                required: false,
                label: t("User:placeholder-lastname"),
                props: GlobalFormConfig.default_text_props
            },
            {
                type: 'password',
                name: 'password',
                required: true,
                label: t("User:placeholder-password"),
                props: GlobalFormConfig.default_text_props
            },
            {
                type: 'password',
                name: 'password_confirmation',
                required: true,
                label: t("User:placeholder-password-confirmation"),
                props: GlobalFormConfig.default_text_props
            }
        ];
    }

    // Sign Up User
    newRegistrationRequest(page,formData) {

        // Default redirect path after user confirms email
        formData.confirm_success_url = Linking.makeUrl();

        // Post the request
        Axios.post(Url.sign_up, formData, {headers: Object.assign(Url.headers)}).then(function (response) { // ON SUCCESS
            console.log("SUCCESS DURING SIGN UP newRegistrationRequest", JSON.stringify(response));
            User.validateTokenRequest(page,response);
        })
        .catch(function (response) {
            // Return error
            if(response.response){
                // Return error
                console.log("ERROR DURING SIGN UP newRegistrationRequest", JSON.stringify(response.response.data.errors));
                // Notify user
                Notify.error(response.response.data.errors[0]);
            } else{
                console.log("ERROR DURING SIGN UP newRegistrationRequest, request failed, check API/IP configuration.");
            }
        });
    } // End of newRegistrationRequest()

    // Auto Devise Confirmation Checking
    async getConfirmedStatus(page,login){
        // Devise Confirmable support
        if (login) {
            // Post the request
            return await Axios.post(Url.check_confirmable,{login: login},{headers: Object.assign(Url.headers)}).then(function (response) { // ON SUCCESS
                var errors = response.data.errors;
                if (errors) {
                    console.log("SUCCESS ERROR DURING getConfirmedStatus ", JSON.stringify(errors));
                    return false
                } else {
                    var confirmed = response.data.confirmed;

                    // Save user confirmable status in reduxState
                    page.props.setConfirmed(confirmed);
                    if (confirmed) {
                        // Notify user
                        Notify.success('You have successfully confirmed your email!');
                    }
                    else {
                        // Notify user
                        Notify.confirmationSent(User,page,login);
                    }
                    return confirmed
                }
            })
            .catch(function (response) {
                // Return error
                if(response.errors){
                    // Return error
                    console.log("ERROR DURING getConfirmedStatus ", JSON.stringify(response.errors));
                    // Save user confirmable status in reduxState
                    page.props.setConfirmed(false);
                    return false
                } else{
                    console.log("ERROR DURING SIGN UP newRegistrationRequest, request failed, check API/IP configuration.");
                }
            });
        }
        else {
            console.log("ERROR DURING getConfirmed User login not found in method.");
            // Save user confirmable status in reduxState
            page.props.setConfirmed(false);
            return false
        }
    }
}
const User = new UserModel();
export default User;