// App Libraries
import React from 'react';

// Translations
import {t} from "../../translations/services/i18n";

// App API Requests & Routes Setup
import Axios from 'axios';
import { Url } from '../../config/api/Routes';

// Globals
const GlobalFormConfig = require('../components/stylesheets/GlobalFormConfig');
const Env = require('../../Env');

// Custom Components
import Notify from '../components/main/Notify';
import Device from "./Device";

// User Payment Model
class UserPaymentModel {

    // User Charges
    fetchUserChargesRequest(page,customerID) {
        var stripeAuth = {Authorization: `Bearer ${Env.stripe_api_sk}`};
        var data = {};
        data["customer"] = customerID;

        Axios.get(Url.stripe_charges, {headers: Object.assign(Url.stripe_headers, stripeAuth), params: data}, ).then(function (response)
        {
            if(response.headers["access-token"] != "") {
                page.props.updateAccessToken(response.headers["access-token"])
            }
            page.setState({
                userPaymentData: response.data.data
            });
            console.log("SUCCESS DURING fetchPayments", JSON.stringify(response));
        }).catch(function (response) {
            page.props.navigation.navigate('Auth');
            console.log("ERROR DURING fetchPayments",  JSON.stringify(response.response.data.errors));
            // Notify user
            Notify.error(response.response.data.errors[0]);
        });
    }

    // User Payments
    async sendUserPaymentRequest(page,requestHistoryID,paymentGateway,paymentSource) {
        var accessToken = await page.props.parent.props.reduxState.Auth.token;
        console.log("payment_gateway DURING sendUserPaymentRequest", JSON.stringify(paymentGateway));
        console.log("card_token DURING sendUserPaymentRequest", JSON.stringify(paymentSource));

        var data = {};
        data["request_history"] = requestHistoryID;
        data["payment_gateway"] = paymentGateway;
        data["card_token"] = paymentSource;

        // Post the request
        await Axios.post(Url.payments_submit_payment, data, {headers: Object.assign(Url.headers, accessToken)}).then(function (response)
        { // ON SUCCESS
            if(response.headers["access-token"] != "") {
                page.props.parent.props.updateAccessToken(response.headers["access-token"])
            }
            page.setState({
                isUserPaymentNotificationModalVisible: false,
            });
            // Notify user
            Notify.success('Your payment completed successfully. Thank You!');
            console.log("SUCCESS DURING sendUserPaymentRequest", JSON.stringify(response));

        }).catch(function (response) {
            page.setState({
                isUserPaymentNotificationModalVisible: false,
            });
            // Return error
            if(response.response){
                // Return errors
                if(response.response.data) {
                    // Return error
                    console.log("ERROR DURING sendUserParkingRequest", JSON.stringify(response.response.data));
                    // Notify user
                    Notify.error(response.response.data.errors[0]);
                } else {
                    // Return response data
                    console.log("ERROR DURING sendUserPaymentRequest", JSON.stringify(response.response));
                }
            } else{
                console.log("ERROR DURING sendUserPaymentRequest, request failed, check API/IP configuration.");
            }

        })
    }


    // Stripe Api Methods
    async getStripeCustomer(page) {

        var stripeCustomerID = page.state.userStripeCustomerID;
        var stripeAuth = {Authorization: `Bearer ${Env.stripe_api_sk}`};

        await Axios.get(Url.stripe_customers+"/"+stripeCustomerID, {headers: Object.assign(Url.headers, stripeAuth)}).then(function (response) {

            var defaultPaymentSource = response.data.default_source;
            var defaultPaymentSourceType;
            // Check default payment source type
            if (defaultPaymentSource  && defaultPaymentSource.indexOf("card") > -1)
            {
                defaultPaymentSourceType = "stripe"
            }

            page.setState({
                userStripeDefaultPaymentSource: response.data.default_source,
                userStripeDefaultPaymentSourceType: defaultPaymentSourceType,
                userStripePaymentSources: response.data.sources.data,
            });

            console.log("SUCCESS DURING getStripeCustomer", JSON.stringify(response));
        }).catch(function (response) {
            console.log("ERROR DURING getStripeCustomer", JSON.stringify(response));
            // Notify user
            Notify.error(response.data);
        });

    }

    async updateStripeCustomer(page,customerParams) {
        if (customerParams != null)
        {
            var stripeCustomerID = page.state.userStripeCustomerID;
            var stripeAuth = {Authorization: `Bearer ${Env.stripe_api_sk}`};
            var encodedData = Object.keys(customerParams).map(key => key + '=' + customerParams[key]).join('&');

            await Axios.post(Url.stripe_customers+"/"+stripeCustomerID, encodedData, {headers: Object.assign(Url.stripe_headers, stripeAuth)}).then(function (response) {
                console.log("SUCCESS DURING updateStripeCustomer", JSON.stringify(response));
            }).catch(function (response) {
                console.log("ERROR DURING updateStripeCustomer", JSON.stringify(response));
                // Notify user
                Notify.error(response.data);
            });
        }
    }

    async getCardStripeToken(page,userCreditCardData) {
        var card = {
            'card[number]': userCreditCardData.values.number.replace(/ /g, ''),
            'card[exp_month]': userCreditCardData.values.expiry.split('/')[0],
            'card[exp_year]': userCreditCardData.values.expiry.split('/')[1],
            'card[cvc]': userCreditCardData.values.cvc
        };
        var encodedCard = Object.keys(card).map(key => key + '=' + card[key]).join('&');
        var stripeAuth = {Authorization: `Bearer ${Env.stripe_api_pk}`};

        await Axios.post(Url.stripe_tokens, encodedCard, {headers: Object.assign(Url.stripe_headers, stripeAuth)}).then(function (response) {

            var cardToken = response.data.id;

            UserPayment.addCardToStripeCustomer(page.state.userStripeCustomerID,cardToken);
            page.props.onNewPaymentCardSubmit();

            console.log("SUCCESS DURING getCardStripeToken", JSON.stringify(response));
            return cardToken
        }).catch(function (response) {
            console.log("ERROR DURING getCardStripeToken", JSON.stringify(response));
            // Notify user
            Notify.error(response.response.data.error.message);
        });

    }

    async addCardToStripeCustomer(userStripeCustomerID,cardToken) {

        var stripeCustomerID = userStripeCustomerID;
        var stripeAuth = {Authorization: `Bearer ${Env.stripe_api_sk}`};
        var data = {
            'source': cardToken
        };
        var encodedData = Object.keys(data).map(key => key + '=' + data[key]).join('&');

        await Axios.post(Url.stripe_customers+"/"+stripeCustomerID+"/sources", encodedData, {headers: Object.assign(Url.stripe_headers, stripeAuth)}).then(function (response) {

            console.log("SUCCESS DURING addCardToStripeCustomer", JSON.stringify(response));

        }).catch(function (response) {
            console.log("ERROR DURING addCardToStripeCustomer", JSON.stringify(response));
            // Notify user
            Notify.error(response.response.data.error.message);
        });

    }

    async deleteCardFromStripeCustomer(userStripeCustomerID,cardId) {

        var stripeCustomerID = userStripeCustomerID;
        var stripeAuth = {Authorization: `Bearer ${Env.stripe_api_sk}`};

        await Axios.delete(Url.stripe_customers+"/"+stripeCustomerID+"/sources/"+cardId, {headers: Object.assign(Url.stripe_headers, stripeAuth)}).then(function (response) {

            console.log("SUCCESS DURING deleteCardFromStripeCustomer", JSON.stringify(response));

        }).catch(function (response) {
            console.log("ERROR DURING deleteCardFromStripeCustomer", JSON.stringify(response));
            // Notify user
            Notify.error(response.response.data.error.message);
        });

    }
}
const UserPayment = new UserPaymentModel();
export default UserPayment;