// App Libraries
import React from 'react';

// Translations
import {t} from "../../translations/services/i18n";

// App API Requests & Routes Setup
import Axios from 'axios';
import { Url } from '../../config/api/Routes';

// Globals
const GlobalFormConfig = require('../components/stylesheets/GlobalFormConfig');

// Custom Components
import Notify from '../components/main/Notify';

// Vehicle Model
class VehicleModel {

    // CRUD Vehicle form fields
    imageField = 'vgyme_image_url';
    remoteImageField = 'vgyme_image_url';

    // Vehicle form fields
    fields(){
        return  [
            {
                type: 'text',
                name: 'name',
                required: true,
                label: t("Global:name"),
                props: GlobalFormConfig.default_text_props
            },
            {
                type: 'text',
                name: 'description',
                required: true,
                label: t("Global:description"),
                props: GlobalFormConfig.default_text_props
            }
        ];
    }

    fetchRequest(page) {
        var accessToken = page.props.reduxState.Auth.token;

        Axios.get(Url.vehicles, {headers: Object.assign(Url.headers, accessToken)}).then(function (response)
        {
            if(response.headers["access-token"] != "") {
                page.props.updateAccessToken(response.headers["access-token"])
            }
            page.setState({
                vehicleData: response.data.vehicles
            });
            console.log("SUCCESS DURING fetchVehicles", JSON.stringify(response));
        }).catch(function (response) {
            // Return error
            if(response.response){
                // Return error
                page.props.navigation.navigate('Auth');
                console.log("ERROR DURING fetchVehicles",  JSON.stringify(response.response.data.errors));
                // Notify user
                Notify.error(response.response.data.errors[0]);
            } else{
                console.log("ERROR DURING fetchVehicles, request failed, check API/IP configuration.");
            }
        });
    }

    newPath(page) {
        page.props.navigation.navigate("VehiclesNew", { refreshComponent: () => page.refreshComponent() })
    }

    createRequest(page,formData) {
        var accessToken = page.props.reduxState.Auth.token;

        // Post the request
        Axios.post(Url.vehicles, formData, {headers: Object.assign(Url.headers, accessToken)}).then(function (response)
        { // ON SUCCESS
            if(response.headers["access-token"] != "") {
                page.props.updateAccessToken(response.headers["access-token"])
            }
            page.props.navigation.state.params.refreshComponent();
            page.props.navigation.navigate("VehiclesIndex");
            console.log("SUCCESS DURING createVehicle", JSON.stringify(response))
        }).catch(function (response) {
            // Return error
            if(response.response){
                // Return error
                page.props.navigation.navigate('Auth');
                console.log("ERROR DURING createVehicle", JSON.stringify(response.response.data.errors));
                // Notify user
                Notify.error(response.response.data.errors[0]);
            } else{
                console.log("ERROR DURING createVehicle, request failed, check API/IP configuration.");
            }
        });
    }

    editPath(page,vehicle) {
        page.props.navigation.navigate("VehiclesEdit", {vehicle: vehicle, refreshComponent: () => page.refreshComponent()})
    }

    updateRequest(page,formData,objectId) {
        var accessToken = page.props.reduxState.Auth.token;

        // Update the Object
        Axios.put(Url.vehicles+objectId, formData, {headers: Object.assign(Url.headers, accessToken)}).then(function (response)
        { // ON SUCCESS
            if(response.headers["access-token"] != "") {
                page.props.updateAccessToken(response.headers["access-token"])
            }
            page.props.navigation.state.params.refreshComponent();
            page.props.navigation.navigate("VehiclesIndex");
            console.log("SUCCESS DURING updateVehicle", JSON.stringify(response));
        }).catch(function (response) {
            // Return error
            if(response.response){
                // Return error
                page.props.navigation.navigate('Auth');
                console.log("ERROR DURING updateVehicle", JSON.stringify(response.response.data.errors));
                // Notify user
                Notify.error(response.response.data.errors[0]);
            } else{
                console.log("ERROR DURING updateVehicle, request failed, check API/IP configuration.");
            }
        });
    }

    showPath(page,vehicle) {
        page.props.navigation.navigate("VehiclesShow", {vehicle})
    }

    deleteRequest(page,vehicle) {
        var accessToken = page.props.reduxState.Auth.token;

        Axios.delete(Url.vehicles+vehicle.id, {headers: Object.assign(Url.headers, accessToken)}).then(function (response)
        {
            if(response.headers["access-token"] != "") {
                page.props.updateAccessToken(response.headers["access-token"])
            }
            page.setState((prevState) => ({
                vehicleData: prevState.vehicleData.filter(e => e !== vehicle)
            }));
            console.log("SUCCESS DURING deleteVehicle"+vehicle.id, JSON.stringify(response));
        }).catch(function (response) {
            // Return error
            if(response.response){
                // Return error
                page.props.navigation.navigate('Auth');
                console.log("ERROR DURING deleteVehicle ID: "+vehicle.id, JSON.stringify(response.response.data.errors));
                // Notify user
                Notify.error(response.response.data.errors[0]);
            } else{
                console.log("ERROR DURING deleteVehicle, request failed, check API/IP configuration.");
            }
        });
    }

    async analyzeImage(page,imageUrl) {
        var data = {};
        data["url"] = imageUrl;

        // Post the request
        await Axios.post(Url.analyze, data, {headers: Object.assign(Url.headers)}).then(function (response)
        { // ON SUCCESS
            if (response !== undefined){
                var analyzedLicensePlate = response.data.result;
                page.setState({
                    analyzedLicensePlate: analyzedLicensePlate
                });
                console.log("SUCCESS DURING analyzeImage", JSON.stringify(response));
            }
            else {
                console.log("TIMEOUT DURING analyzeImage");
            }

        }).catch(function () {
            console.log("ERROR DURING analyzeImage");
            // Notify user
            Notify.error("ERROR analyzing image!");
        })
    }
}
const Vehicle = new VehicleModel();
export default Vehicle;