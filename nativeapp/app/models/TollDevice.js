// App Libraries
import React from 'react';

// Translations
import {t} from "../../translations/services/i18n";

// App API Requests & Routes Setup
import Axios from 'axios';
import { Url } from '../../config/api/Routes';

// Globals
const GlobalFormConfig = require('../components/stylesheets/GlobalFormConfig');

// Custom Components
import Notify from '../components/main/Notify';
import Device from "./Device";

// TollDevice Model
class TollDeviceModel {

    async fetchRequest(page) {
        var accessToken = page.props.reduxState.Auth.token;

        await Axios.get(Url.toll_devices_index, {headers: Object.assign(Url.headers, accessToken)}).then(function (response)
        {
            if(response.headers["access-token"] != "") {
                page.props.updateAccessToken(response.headers["access-token"])
            }
            page.setState({
                tollDeviceData: response.data.toll_devices
            });
            console.log("SUCCESS DURING fetchTollDevices", JSON.stringify(response));
        }).catch(function (response) {
            // Return error
            if(response.response){
                // Return errors
                if(response.response.data) {
                    // Return error
                    console.log("ERROR DURING fetchTollDevices",  JSON.stringify(response.response.data));
                    // Notify user
                    Notify.error(response.response.data.errors[0]);
                } else {
                    // Return response data
                    console.log("ERROR DURING fetchTollDevices", JSON.stringify(response.response));
                }
            } else{
                console.log("ERROR DURING fetchTollDevices, request failed, check API/IP configuration.");
            }
        });
    }

    async getVehicleHistory(page,deviceToken,imageUrl) {
        var accessToken = page.props.reduxState.Auth.token;
        var data = {};

        data["device_token"] = deviceToken;
        data["url"] = imageUrl;
        data["hours"] = 1;

        // Post the request
        await Axios.post(Url.toll_devices_get_vehicle_history, data, {headers: Object.assign(Url.headers, accessToken)}).then(function (response)
        { // ON SUCCESS
            if(response.headers["access-token"] != "") {
                page.props.updateAccessToken(response.headers["access-token"]);
            }

            console.log("SUCCESS DURING getVehicleHistory", JSON.stringify(response));

        }).catch(function (response) {
            // Return error
            if(response.response){
                // Return errors
                if(response.response.data) {
                    // Return error
                    console.log("ERROR DURING getVehicleHistory", JSON.stringify(response.response.data));
                    // Notify user
                    Notify.error(JSON.stringify(response.response.data.errors));
                } else {
                    // Return response data
                    console.log("ERROR DURING getVehicleHistory", JSON.stringify(response.response));

                }
            } else{
                console.log("ERROR DURING getVehicleHistory, request failed, check API/IP configuration.");
            }
        })
    }


    async sendUserParkingRequest(page,declined,requestHistoryID) {
        var accessToken = page.props.parent.props.reduxState.Auth.token;

        var data = {};
        data["declined"] = declined;
        data["request_history"] = requestHistoryID;
        data["pushToken"] = await Device.getPushNotificationToken();

        // Post the request
        await Axios.post(Url.toll_devices_check_declined, data, {headers: Object.assign(Url.headers, accessToken)}).then(function (response)
        { // ON SUCCESS
            if(response.headers["access-token"] != "") {
                page.props.parent.props.updateAccessToken(response.headers["access-token"])
            }
            page.setState({
                isUserParkingNotificationModalVisible: false,
            });
            // Notify user
            Notify.success('Your parking request has been confirmed!');
            console.log("SUCCESS DURING sendUserParkingRequest", JSON.stringify(response));

        }).catch(function (response) {
            page.setState({
                isUserParkingNotificationModalVisible: false,
            });
            // Return error
            if(response.response){
                // Return errors
                if(response.response.data) {
                    // Return error
                    console.log("ERROR DURING sendUserParkingRequest", JSON.stringify(response.response.data));
                    // Notify user
                    Notify.error(response.response.data.errors[0]);

                } else {
                    // Return response data
                    console.log("ERROR DURING sendUserParkingRequest", JSON.stringify(response.response));
                }
            } else{
                console.log("ERROR DURING sendUserParkingRequest, request failed, check API/IP configuration.");
            }

        })
    }
}
const TollDevice = new TollDeviceModel();
export default TollDevice;