// App Libraries
import React from 'react';

// Expo & React Native Platform & Location Libraries
import { Platform } from 'react-native';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import Constants from 'expo-constants';

// App API Requests & Routes Setup
import Axios from 'axios';
import { Url } from '../../config/api/Routes';

// Device Model
class DeviceModel {

    generateUDID(string){
        var hash = 0;
        if (string.length === 0) return hash;
        for (i = 0; i < string.length; i++) {
            char = string.charCodeAt(i);
            hash = ((hash<<5)-hash)+char;
            hash = hash & hash; // Convert to 32bit integers
        }
        return hash;
    }

    async getInfo(option = "all"){
        var deviceInfo = {};

        if (option === "all") {
            deviceInfo["pushToken"] = await Device.getPushNotificationToken();
            deviceInfo["appOwnership"] = Constants["appOwnership"];
            deviceInfo["expoVersion"] = Constants["expoVersion"];
            deviceInfo["installationId"] = Constants["installationId"];
            deviceInfo["platform"] = JSON.stringify(Constants["platform"]);
            deviceInfo["isDevice"] = Constants["isDevice"];
            deviceInfo["sessionId"] = Constants["sessionId"];
            deviceInfo["systemVersion"] = Platform["Version"];
        }

        if (option === "udid" || option === "all") {
            deviceInfo["deviceName"] = Constants["deviceName"];
            deviceInfo["deviceYearClass"] = Constants["deviceYearClass"];
            deviceInfo["systemName"] = Platform["OS"];

            var uniqueInfo = deviceInfo["deviceName"] + deviceInfo["deviceYearClass"] + deviceInfo["systemName"];
            deviceInfo.udid = Device.generateUDID(uniqueInfo);
        }

        console.log("SUCCESS DURING getInfo", JSON.stringify(deviceInfo));

        return deviceInfo
    }

    async saveInfo(){
        var deviceInfo = await Device.getInfo();

        // Post the request
        Axios.post(Url.devices, deviceInfo).then(function (response) { // ON SUCCESS
            console.log("SUCCESS DURING saveInfo", JSON.stringify(response));
        }).catch(function (response) {
            // Return error
            console.log("ERROR DURING saveInfo", JSON.stringify(response.response.data.errors));
        })
    }

    async getPushNotificationToken() {
        const { status: existingStatus } = await Permissions.getAsync(
            Permissions.NOTIFICATIONS
        );
        let finalStatus = existingStatus;

        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }

        // Stop here if the user did not grant permissions
        if (finalStatus !== 'granted') {
            return;
        }

        // Get the token that uniquely identifies this device
        return await Notifications.getExpoPushTokenAsync();
    }

    // Get Device Location
    async getLocation() {
        if (Platform.OS === 'android' && !Constants.isDevice) {
            this.setState({
                errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!'
            });
        } else {
            let { status } = await Permissions.askAsync(Permissions.LOCATION);
            if (status !== 'granted') {
                console.log("PERMISSIONS DENIED DURING getLocation");
                return "denied"
            }
            let location = await Location.getCurrentPositionAsync({});
            console.log("SUCCESS DURING getLocation ", JSON.stringify(location));
            return location
        }
    } // End of getLocation()

    // Save User Device Location in DB & APP
    async saveLocation() {
        var location = await Device.getLocation();
        if (location !== "denied"){
            location.coords.udid = Device.getInfo("udid").udid;

            // Post the request
            Axios.post(Url.locations, location).then(function (response)
            { // ON SUCCESS
                console.log("SUCCESS DURING saveLocation", JSON.stringify(response));
            }).catch(function (response) {
                // Return error
                if(response.response){
                    console.log("ERROR DURING saveLocation", JSON.stringify(response.response.data.errors));
                } else{
                    console.log("ERROR DURING saveLocation, request failed, check API/IP configuration.");
                }
            });
        }
    } // End of saveLocation()

}
const Device = new DeviceModel();
export default Device;