// App Libraries
import React from 'react';

// Redux
import { connect } from 'react-redux';
import Auth from '../../redux/reducers/auth';

// Translations
import { t } from '../../../translations/services/i18n';

// View Libraries
import {Container, Content, Text, View} from 'native-base';
import {ImageBackground} from "react-native";

// Global
const GlobalStyles = require('../../components/stylesheets/GlobalStyles');

// Custom Components
import { NavHeader } from '../../components/main/NavHeader';

export class About extends React.Component {

    constructor(props) {
        super(props);
        // Nav Header Settings
        this.headerSettings = {
            title: t("About:nav-title"),
            navigation: this.props.navigation,
            backPath: this.props.reduxState.Auth.connected ? "Home" : "Welcome",
            location: false,
        }
    };

    render() {
        return (

            <Container>

                <NavHeader settings={this.headerSettings} />

                <Content contentContainerStyle={ [GlobalStyles.primaryBackgroundColor,GlobalStyles.contentItemsCenter] }>
                    <ImageBackground source={require('../../../assets/images/icon.png')} style={[GlobalStyles.logo,GlobalStyles.textCenter,GlobalStyles.mb40]}>
                    </ImageBackground>
                    <Text style={[GlobalStyles.titleText,GlobalStyles.primaryTextColor,GlobalStyles.primaryMarginHorizontal,GlobalStyles.mb15]}>{t(`About:content-header`)}</Text>
                    <Text style={[GlobalStyles.secondaryTextColor,GlobalStyles.primaryMarginHorizontal]}>{t(`About:content-body`)}</Text>
                </Content>

            </Container>
        );
    }
}

// Redux
const mapStateToProps = (state) => ({
    reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
    Auth
})(About);