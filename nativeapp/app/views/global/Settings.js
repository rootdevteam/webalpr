// App Libraries
import React from 'react';

// Redux
import { connect } from 'react-redux';
import Auth from '../../redux/reducers/auth';
import { setLanguage } from '../../redux/actions/auth'

// Translations
import il8n,{ t } from '../../../translations/services/i18n';

// View Libraries
import {Body, Container, Content, Left, ListItem, Right, Switch, Text, View} from 'native-base';

// Global
const GlobalTheme = require('../../components/stylesheets/GlobalTheme');
const GlobalStyles = require('../../components/stylesheets/GlobalStyles');

// Custom Components
import { NavHeader } from '../../components/main/NavHeader';

export class Settings extends React.Component {

    constructor(props) {
        super(props);
        // Nav Header Settings
        this.headerSettings = {
            title: t("Settings:nav-title"),
            navigation: this.props.navigation,
            backPath: this.props.reduxState.Auth.connected ? "Home" : "Welcome",
            location: false,
        }
    };

    render() {
        return (
            <Container >

                <NavHeader settings={this.headerSettings} />
                <Content contentContainerStyle={ [GlobalStyles.primaryBackgroundColor,{flex: 1}]}>
                    <ListItem itemHeader first>
                        <Text style={[GlobalStyles.primaryTextColor,GlobalStyles.labelText]}>{t(`Settings:language-plural`)}</Text>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Text style={[GlobalStyles.secondaryTextColor]}>{t(`Settings:language-english`)}</Text>
                        </Left>
                        <Body>
                        </Body>
                        <Right>
                            <Switch value={this.props.reduxState.Auth.language === "en"} onTrackColor={GlobalTheme.primaryTextColor} onValueChange={() => il8n.switchLanguage(this,"en") } />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Text style={[GlobalStyles.secondaryTextColor]}>{t(`Settings:language-russian`)}</Text>
                        </Left>
                        <Body>
                        </Body>
                        <Right>
                            <Switch value={this.props.reduxState.Auth.language === "ru"} onTrackColor={GlobalTheme.primaryTextColor} onValueChange={() => il8n.switchLanguage(this,"ru") } />
                        </Right>
                    </ListItem>
                </Content>

            </Container>
        );
    }
}

// Redux
const mapStateToProps = (state) => ({
    reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
    Auth,
    setLanguage
})(Settings);