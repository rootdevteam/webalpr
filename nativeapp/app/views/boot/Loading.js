// App Libraries
import React from 'react';

// Redux
import { connect } from 'react-redux';
import Auth from '../../redux/reducers/auth';
import { setConfirmed } from '../../redux/actions/auth';

// Translations
import i18next from 'i18next';

// View Libraries
import { ActivityIndicator, StatusBar, View } from 'react-native';

// Global
const GlobalStyles = require('../../components/stylesheets/GlobalStyles');

// Custom Models
import User from '../../models/User';

class Loading extends React.Component {
    constructor(props) {
        super(props);
        // Enter in this case if the user just logged in or if there was data stored in reduxPersist
        if( this.props.reduxState.Auth.connected || this.props.reduxState.Auth.rehydrated) {
            // Set App Language based on Redux state
            i18next.changeLanguage(this.props.reduxState.Auth.language);
            // Devise Confirmable support
            if(!this.props.reduxState.Auth.confirmed) {
                if(this.props.reduxState.Auth.user) {
                    User.getConfirmedStatus(this,this.props.reduxState.Auth.user.email);
                }
            }
            // Go to proper view based on state
            this.checkIfConnected();
        }
    }

    checkIfConnected = () => {
        // In this case user just logged or user have session in local storage
        if(this.props.reduxState.Auth.connected) {
            this.props.navigation.navigate('Main');
        }
        // In this case user have session but it had been destroyed when it was last connected
        else {
            this.props.navigation.navigate('Auth');
        }

    };

    // Render any loading content that you like here
    render() {
        return (
            <View style={GlobalStyles.contentCenter}>
                <ActivityIndicator />
                <StatusBar barStyle="default"  />
            </View>
        );
    }
}

// Redux
const mapStateToProps = (state) => ({
    reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
    Auth,
    setConfirmed
})(Loading);