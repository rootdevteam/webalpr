// App Libraries
import React from 'react';

// Redux
import { connect } from 'react-redux';
import Auth from '../../redux/reducers/auth';
import { authSuccess, setConfirmed } from '../../redux/actions/auth';

// View Libraries
import { Container, Content, View } from 'native-base';
import {ImageBackground} from 'react-native';

// Translations
import { t } from '../../../translations/services/i18n';

// Global
const GlobalStyles = require('../../components/stylesheets/GlobalStyles');

// Custom Components
import { NavHeader } from '../../components/main/NavHeader';
import { SimpleForm } from '../../components/main/SimpleForm';

// Custom Models
import User from '../../models/User';

export class SignIn extends React.Component {
    constructor(props) {
        super(props);
        // Nav Header Settings
        this.headerSettings = {
            title: t("Signin:nav-title"),
            navigation: this.props.navigation,
            backPath: "Welcome",
            location: false,
        }
    };


    render() {
        return (
            <Container>

                <NavHeader settings={this.headerSettings} />

                <ImageBackground source={require('../../../assets/images/background.jpg')} style={GlobalStyles.bgContainer}>
                    <Content contentContainerStyle={ GlobalStyles.contentCenter }>

                        <View style={ GlobalStyles.contentChildCenter }>
                            <ImageBackground source={require('../../../assets/images/icon.png')} style={[GlobalStyles.logo,GlobalStyles.textCenter,GlobalStyles.mb20]}>
                            </ImageBackground>
                            <SimpleForm
                                fields={User.signInFields()}
                                onSubmit={(data) => User.newSessionRequest(this,data)}
                                buttonText={t(`Signin:nav-title`)}
                                formData={this.props.navigation.state.params}
                                navigation={this.props.navigation}
                            />
                        </View>
                    </Content>
                </ImageBackground>


            </Container>
        );
    }
}

// Redux
const mapStateToProps = (state) => ({
    reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
    Auth,
    authSuccess,
    setConfirmed
})(SignIn);