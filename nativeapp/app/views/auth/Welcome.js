// App Libraries
import React from 'react';

// Redux
import { connect } from 'react-redux';
import Auth from '../../redux/reducers/auth';

// Translations
import { t } from '../../../translations/services/i18n';

// View Libraries
import { Button, Container, Content, Text, View } from 'native-base';
import {ImageBackground} from "react-native";

// Globals
const GlobalStyles = require('../../components/stylesheets/GlobalStyles');

// Custom Components
import { NavHeader } from '../../components/main/NavHeader';

export class Welcome extends React.Component {

    constructor(props) {
        super(props);
        this.headerSettings = {
          title: t("Global:app-name"),
          navigation: this.props.navigation,
          location: false,
        }
    }

    render() {
        return (

            <Container>

                <NavHeader settings={this.headerSettings} />

                <ImageBackground source={require('../../../assets/images/background.jpg')} style={GlobalStyles.bgContainer}>

                    <Text style={[GlobalStyles.titleText,GlobalStyles.secondaryTextColor,GlobalStyles.textCenter,GlobalStyles.mt20]}>{t(`Welcome:content-header`)}</Text>
                    <Text style={[GlobalStyles.titleText,GlobalStyles.secondaryTextColor,GlobalStyles.textCenter,GlobalStyles.mb10]}>{t(`Global:app-name`)}</Text>

                    <ImageBackground source={require('../../../assets/images/icon.png')} style={[GlobalStyles.logo,GlobalStyles.textCenter]}>
                    </ImageBackground>

                    <Content style={GlobalStyles.mt20}>
                        <Button block onPress={() => this.props.navigation.navigate('SignIn')}  style={[GlobalStyles.primaryButton]}>
                            <Text style={[GlobalStyles.primaryTextColor]}>{t(`Signin:nav-title`)}</Text>
                        </Button>
                        <Button block onPress={() => this.props.navigation.navigate('SignUp')  } style={[GlobalStyles.primaryButton]} >
                          <Text style={[GlobalStyles.primaryTextColor]}>{t(`Signup:nav-title`)}</Text>
                        </Button>
                    </Content>

                </ImageBackground>

            </Container>

        );
    }
}

// Redux
const mapStateToProps = (state) => ({
    reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
    Auth
})(Welcome);