// App Libraries
import React from 'react';

// Redux
import { connect } from 'react-redux';
import Auth from '../../../redux/reducers/auth';
import { updateAccessToken } from '../../../redux/actions/auth'

// Translations
import {t} from "../../../../translations/services/i18n";

// View Libraries
import {ScrollView, Text} from "react-native";
import {Container, Content, Tabs, Tab, Card, CardItem, Left, Body, Picker, Icon, Button, View} from 'native-base';

// Utility
import _ from 'lodash';

// Global
const GlobalStyles = require('../../../components/stylesheets/GlobalStyles');
const GlobalTheme = require('../../../components/stylesheets/GlobalTheme');

// Custom Components
import { NavHeader } from '../../../components/main/NavHeader';
import {ImageUpload} from "../../../components/main/ImageUpload";

// Custom Models
import TollDevice from '../../../models/TollDevice';

export class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultTollDeviceSelected: "none",
            tollDeviceData: undefined,
            requestHistoryData: undefined,
            imageBase64: undefined,
            remoteImageField: this.props.remoteImageField ? this.props.remoteImageField : undefined,
            remoteImageEndpoint: this.props.remoteImageEndpoint ? this.props.remoteImageEndpoint : undefined,
            remoteImageUrl: undefined,

        };
        // Nav Header Settings
        this.headerSettings = {
          title: t("Global:simulation"),
          navigation: this.props.navigation,
          backPath: "Home",
          searchPath: true,
          location: false,
        }
    };

    componentDidMount() {
        this.refreshComponent();
    }

    async refreshComponent() {
        await TollDevice.fetchRequest(this);
    }

    onTollDeviceValueChange(value) {
        this.setState({
            defaultTollDeviceSelected: value
        });
    }

    async setImageField(imageSrc){
        // If Endpoint upload to remote server, use remote url for field post
        if (this.state.remoteImageEndpoint !== undefined) {
            //console.log("SUCCESS DURING USING REMOTE URL setImageField (Remote untested)", JSON.stringify(imageSrc));
            this.setState({
                remoteImageUrl: imageSrc
            });
        } else {
            //console.log("SUCCESS DURING USING setImageField (Base64 only)");
            this.setState({
                imageBase64: imageSrc
            });
        }
    }

    async onSimulate(){
        var deviceToken = this.state.defaultTollDeviceSelected;
        var imageUrl = this.state.imageBase64;
        await TollDevice.getVehicleHistory(this,deviceToken,imageUrl);
    }

    render() {
        return(

          <Container>

            <NavHeader settings={this.headerSettings} />

            <Content contentContainerStyle={[GlobalStyles.primaryBackgroundColor,GlobalStyles.contentCenter]}>

                <Card style={[GlobalStyles.defaultCard, GlobalStyles.mt10 ]}>

                    <CardItem style={[GlobalStyles.clearBorder]}>
                        <Left><Text>Select a toll device:</Text></Left>
                    </CardItem>
                    <CardItem style={[GlobalStyles.clearBorder]}>
                        {!_.isArray(this.state.tollDeviceData) && // No toll device data
                            <Body style={[GlobalStyles.contentItemsCenter]}>
                            <Text>Loading Toll Devices...</Text>
                            </Body>
                        }
                        {_.isArray(this.state.tollDeviceData) && // Toll Devices
                            <Left>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Methods"
                                    iosIcon={<Icon name="arrow-down" style={{ color: GlobalTheme.primaryTextColor, fontSize: 25}} />}
                                    placeholder="None"
                                    placeholderStyle={{ color: "#000000" }}
                                    style={ GlobalStyles.defaultPicker }
                                    selectedValue={this.state.defaultTollDeviceSelected}
                                    onValueChange={this.onTollDeviceValueChange.bind(this)}
                                >
                                    <Picker.Item label="None" value="none" />

                                    {_.isArray(this.state.tollDeviceData) &&
                                        this.state.tollDeviceData.map((data, key) => {
                                            return (
                                                <Picker.Item key={key} label={data.name} value={data.device_token} />
                                            )
                                        })
                                    }
                                </Picker>
                            </Left>
                        }
                    </CardItem>

                    {this.state.defaultTollDeviceSelected !== "none" &&
                        <CardItem style={[GlobalStyles.clearBorder]}>
                            <Left><Text>Pick a vehicle to check:</Text></Left>
                        </CardItem>
                    }

                    {this.state.defaultTollDeviceSelected !== "none" &&
                        <CardItem style={[GlobalStyles.clearBorder]}>
                            <ScrollView>
                                <ImageUpload
                                    imageBase64={this.state.imageBase64} // Base64 permanent
                                    afterUpload={(imgSrc) => this.setImageField(imgSrc)} // What to do after upload
                                    remoteImageEndpoint={this.state.remoteImageEndpoint} // (Optional upload to remote server)
                                    remotePayloadKey='file' // Field name
                                    remoteImageUrl={this.state.remoteImageUrl} // Uploaded Image url
                                />

                                {this.state.imageBase64 &&
                                    <Button block onPress={() => this.onSimulate()} style={[GlobalStyles.primaryButton]}>
                                        <Text style={[GlobalStyles.primaryTextColor]}>{t("Global:simulation")}</Text>
                                    </Button>
                                }
                            </ScrollView>
                        </CardItem>
                    }

                </Card>

            </Content>

          </Container>
        );
    }
}

// Redux
const mapStateToProps = (state) => ({
	reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
  Auth,
  updateAccessToken
})(Index);