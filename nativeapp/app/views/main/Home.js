// App Libraries
import React from 'react';
import {ImageBackground} from "react-native";

// Redux
import { connect } from 'react-redux';
import Auth from '../../redux/reducers/auth';
import { setLanguage } from '../../redux/actions/auth'

// Translations
import {t} from "../../../translations/services/i18n";

// View Libraries
import { Button, Container, Content, Text, View } from 'native-base';

// Global
const GlobalStyles = require('../../components/stylesheets/GlobalStyles');

// Custom Components
import { NavHeader } from '../../components/main/NavHeader';

// Custom Models
import User from '../../models/User';

class Home extends React.Component {
  constructor(props) {
      super(props);
      this.headerSettings = {
          title: t("Home:nav-title"),
          navigation: this.props.navigation,
          signOutPath: () => User.destroySessionRequest(this)
      }
  }

  render() {
    return (

        <Container>

            <NavHeader settings={this.headerSettings} />

            <ImageBackground source={require('../../../assets/images/background.jpg')} style={GlobalStyles.bgContainer}>
                <Content contentContainerStyle={GlobalStyles.contentCenter}>
                    <Text style={[GlobalStyles.secondaryTextColor,GlobalStyles.titleText,GlobalStyles.textCenter,GlobalStyles.mb10]}>{t(`Home:content-header`)}</Text>
                    <View style={ GlobalStyles.contentChildCenter }>
                        <ImageBackground source={require('../../../assets/images/icon.png')} style={[GlobalStyles.logo,GlobalStyles.textCenter]}>
                        </ImageBackground>
                    </View>
                </Content>
            </ImageBackground>


        </Container>
    );
  }
}

// Redux
const mapStateToProps = (state) => ({
	reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
	Auth,
    setLanguage
})(Home);