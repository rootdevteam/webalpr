// App Libraries
import React from 'react';

// Redux
import { connect } from 'react-redux';
import Auth from '../../../redux/reducers/auth';
import { updateAccessToken } from '../../../redux/actions/auth'

// Translations
import {t} from "../../../../translations/services/i18n";

// View Libraries
import { Container, Content } from 'native-base';

// Global
const GlobalStyles = require('../../../components/stylesheets/GlobalStyles');

// Custom Components
import { NavHeader } from '../../../components/main/NavHeader';
import { ListSwiper } from '../../../components/main/ListSwiper';

// Custom Models
import Vehicle from '../../../models/Vehicle';

export class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          vehicleData: []
        };
        // Nav Header Settings
        this.headerSettings = {
          title: t("Vehicle:plural"),
          navigation: this.props.navigation,
          backPath: "Home",
          newPath: () => Vehicle.newPath(this),
          searchPath: true
        }
    };

    componentDidMount = () => {
        this.refreshComponent();
    };

    refreshComponent() {
        Vehicle.fetchRequest(this);
    }

    render() {
        return(
          <Container>

            <NavHeader settings={this.headerSettings} />

            <Content contentContainerStyle={[GlobalStyles.primaryBackgroundColor,GlobalStyles.contentCenter]}>
                <ListSwiper
                    dataSource={this.state.vehicleData}
                    onEdit={(data) => Vehicle.editPath(this,data)}
                    onShow={(data) => Vehicle.showPath(this,data)}
                    onDelete={(data) => Vehicle.deleteRequest(this,data)}
                />
            </Content>

          </Container>
        );
    }
}

// Redux
const mapStateToProps = (state) => ({
	reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
  Auth,
  updateAccessToken
})(Index);