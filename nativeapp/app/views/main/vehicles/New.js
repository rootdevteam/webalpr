// App Libraries
import React from 'react';

// Redux
import { connect } from 'react-redux';
import Auth from '../../../redux/reducers/auth';
import { updateAccessToken } from '../../../redux/actions/auth'

// Translations
import {t} from "../../../../translations/services/i18n";

// View Libraries
import { Container, Content, View } from 'native-base';

// Global
const GlobalStyles = require('../../../components/stylesheets/GlobalStyles');

// Custom Components
import { NavHeader } from '../../../components/main/NavHeader';
import { VehicleForm } from './components/VehicleForm';

// Custom Models
import Vehicle from '../../../models/Vehicle';

export class New extends React.Component {
  constructor(props){
    super(props);
    // Nav Header Settings
    this.headerSettings = {
      title: t("Global:new"),
      navigation: this.props.navigation,
      backPath: "VehiclesIndex"
    }
  }

  render() {
      return (

        <Container>

            <NavHeader settings={this.headerSettings} />

            <Content contentContainerStyle={[GlobalStyles.primaryBackgroundColor,GlobalStyles.contentCenter]}>
                <View style={ GlobalStyles.contentChildCenter }>

                    <VehicleForm
                        fields={Vehicle.fields()}
                        imageField={Vehicle.imageField}
                        onSubmit={(data) => Vehicle.createRequest(this,data)}
                        buttonText={t(`Global:create`)}
                        formData={this.props.navigation.state.params}
                        navigation={this.props.navigation}
                    />

                </View>
            </Content>

        </Container>

    );
  }
}

// Redux
const mapStateToProps = (state) => ({
    reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
    Auth,
    updateAccessToken
})(New);