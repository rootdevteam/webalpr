// App Libraries
import React from 'react';

// Translations
import {t} from "../../../../translations/services/i18n";

// View Libraries
import { Image } from "react-native";
import { Container, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';

// Global
const GlobalStyles = require('../../../components/stylesheets/GlobalStyles');

//Custom Components
import { NavHeader } from '../../../components/main/NavHeader';

// Custom Models
import Vehicle from '../../../models/Vehicle';

export default class Show extends React.Component {
  constructor(props) {
    super(props);
    this.vehicle = this.props.navigation.state.params.vehicle;
    this.vehicle_image_url = this.vehicle[Vehicle.imageField] ? this.vehicle[Vehicle.imageField] : this.vehicle[Vehicle.remoteImageField];

    this.headerSettings = {
      title: this.vehicle.name,
      navigation: this.props.navigation,
      backPath: "VehiclesIndex"
    }
  }
  render() {
    return (
        <Container>

          <NavHeader settings={this.headerSettings} />

          <Content contentContainerStyle={[GlobalStyles.primaryBackgroundColor,GlobalStyles.contentCenter]}>

            <Card transparent>
              <CardItem style={[GlobalStyles.primaryBackgroundColor,GlobalStyles.clearBorder]}>
                {this.vehicle_image_url ? <Image source={{uri: this.vehicle_image_url, isStatic: true}} style={[GlobalStyles.vehicleImage,GlobalStyles.primaryBorder,{height: 400,width: 400}]}/>  : <Image source={{uri:'https://via.placeholder.com/400.png'}} style={[GlobalStyles.vehicleImage,GlobalStyles.primaryBorder,{height: 400,width: 400}]}/> }
              </CardItem>
              <CardItem style={[GlobalStyles.primaryBackgroundColor,GlobalStyles.clearBorder]}>
                <Left>
                  <Text style={[GlobalStyles.labelText,GlobalStyles.primaryTextColor]}>
                    {t(`Vehicle:analyzed-license-plate`)}
                  </Text>
                </Left>
                <Right>
                  <Text style={[GlobalStyles.labelText,GlobalStyles.secondaryTextColor]}>
                    {this.vehicle.alpr_license_plate !== "false" ? this.vehicle.alpr_license_plate : t(`Vehicle:no-result`)}
                  </Text>
                </Right>
              </CardItem>
              <CardItem style={[GlobalStyles.primaryBackgroundColor,GlobalStyles.clearBorder]}>
                <Left>
                  <Text style={[GlobalStyles.labelText,GlobalStyles.primaryTextColor]}>
                    {t(`Vehicle:license-plate`)}
                  </Text>
                </Left>
                <Right>
                  <Text style={[GlobalStyles.labelText,GlobalStyles.secondaryTextColor]}>
                    {this.vehicle.license_plate !== "false" ? this.vehicle.license_plate : t(`Vehicle:no-result`)}
                  </Text>
                </Right>
              </CardItem>
              <CardItem style={[GlobalStyles.primaryBackgroundColor,GlobalStyles.clearBorder]}>
                <Left>
                  <Text style={[GlobalStyles.labelText,GlobalStyles.primaryTextColor]}>
                    {t("Global:description")}
                  </Text>
                </Left>
              </CardItem>
              <CardItem style={[GlobalStyles.textCenter,GlobalStyles.primaryBackgroundColor,GlobalStyles.clearBorder]}>
                <Text style={[GlobalStyles.secondaryTextColor]}>
                  {this.vehicle.description}
                </Text>
              </CardItem>
            </Card>
          </Content>

        </Container>
    );
  }
}