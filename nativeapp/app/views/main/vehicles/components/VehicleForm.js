// App Libraries
import React from 'react';

// View Libraries
import { ScrollView } from 'react-native';
import { Button, Icon, Item, Input, Label, Text, View } from 'native-base';
import GenerateForm from '../../../../components/main/form_builder/FormBuilder';

// Translations
import {t} from "../../../../../translations/services/i18n";

// Global
const GlobalTheme = require('../../../../components/stylesheets/GlobalTheme');
const GlobalFormConfig = require('../../../../components/stylesheets/GlobalFormConfig');
const GlobalStyles = require('../../../../components/stylesheets/GlobalStyles');

// Custom Components
import { ImageUpload } from '../../../../components/main/ImageUpload';

// Custom Models
import Vehicle from '../../../../models/Vehicle';

export class VehicleForm extends React.Component {
    constructor(props){
        super(props);
        // Default form fields new/edit
        this.formData = this.props.formData ? this.props.formData : undefined;

        // Alpr Support
        this.alpr = this.props.alpr ? this.props.alpr : undefined;

        // Image Field & Alpr Support
        this.state={
            imageField: this.props.imageField ? this.props.imageField : undefined,
            imageBase64: undefined,
            remoteImageField: this.props.remoteImageField ? this.props.remoteImageField : undefined,
            remoteImageEndpoint: this.props.remoteImageEndpoint ? this.props.remoteImageEndpoint : undefined,
            remoteImageUrl: undefined,
            licensePlate: undefined,
            analyzedLicensePlate: undefined
        };
    }

    componentDidMount = () => {
        this.initFormData();
    };

    initFormData(){
        // Get/Set formData from state if it exists for edit/update actions, otherwise clear it
        if (this.props.formData !== undefined){
            this.formData = this.props.formData;
            // Get/Set object Id for update actions
            if (this.formData.id !== undefined){
                this.objectId = this.formData.id;
            }

            // Get/Set Image for edit/update
            if (this.state.imageField !== undefined){
                this.state.imageBase64 = this.formData[this.state.imageField] ? this.formData[this.state.imageField] : undefined;

                // Get/Set License Plate edit/update
                if (this.state.imageBase64 !== undefined){
                    this.state.licensePlate = this.formData["license_plate"] ? this.formData["license_plate"] : undefined;
                    this.state.analyzedLicensePlate = this.formData["alpr_license_plate"] ? this.formData["alpr_license_plate"] : undefined;
                }
            }
        }
    }

    onSubmit(){
        // Get this forms values, and send them back to our parent's function
        const formValues = this.formGenerator.getValues();

        // Default base64 image field support
        if (this.state.imageField !== undefined) {
            // Set values created by custom components
            formValues[this.state.imageField] = this.state.imageBase64;
        }

        // Remote image field support
        if (this.state.remoteImageField !== undefined) {
            // Set values created by custom components
            formValues[this.state.remoteImageField] = this.state.remoteImageUrl;
        }

        if (this.state.licensePlate !== undefined) {
            console.log("license plate setImageField ", JSON.stringify(this.state.licensePlate));
            // On edit/update if initially false (no result), set values to analyzed plate
            if (this.state.licensePlate === "false") {
                formValues["license_plate"] = this.state.analyzedLicensePlate;
            } else{
                // Set values created by custom components
                formValues["license_plate"] = this.state.licensePlate;
            }
        } else {
            // Set values created by custom components
            formValues["license_plate"] = this.state.analyzedLicensePlate;
        }
        if (this.state.analyzedLicensePlate !== undefined) {
            // Set values created by custom components
            console.log("license plate setImageField ", JSON.stringify(this.state.analyzedLicensePlate));

            formValues["alpr_license_plate"] = this.state.analyzedLicensePlate;
        }

        this.props.onSubmit(formValues,this.objectId)
    }

    async setImageField(imageSrc){

        // If Endpoint upload to remote server, use remote url for field post
        if (this.state.remoteImageEndpoint !== undefined) {
            console.log("SUCCESS DURING USING REMOTE URL setImageField (Remote untested)", JSON.stringify(imageSrc));
            this.setState({
                remoteImageUrl: imageSrc,
                analyzedLicensePlate: undefined
            });
            await Vehicle.analyzeImage(this,imageSrc);
        } else {
            console.log("SUCCESS DURING USING setImageField (Base64 only)");
            this.setState({
                imageBase64: imageSrc,
                analyzedLicensePlate: undefined
            });
            await Vehicle.analyzeImage(this,imageSrc);
        }

    }

    render() {
        return (
            <ScrollView>
                {this.state.imageField && // Image Support if imageField is passed in
                    <ImageUpload
                        imageBase64={this.state.imageBase64} // Base64 permanent
                        afterUpload={(imgSrc) => this.setImageField(imgSrc)} // What to do after upload
                        remoteImageEndpoint={this.state.remoteImageEndpoint} // (Optional upload to remote server)
                        remotePayloadKey='file' // Field name
                        remoteImageUrl={this.state.remoteImageUrl} // Uploaded Image url
                    />
                }

                <GenerateForm
                    ref={(c) => {
                        this.formGenerator = c;
                    }}
                    fields={this.props.fields}
                    formData={this.formData}
                    theme = {GlobalFormConfig.theme}
                    scrollViewProps={GlobalFormConfig.disableAutoScroll}
                />
                <View style={[GlobalStyles.primaryMarginHorizontal]}>

                {this.state.imageBase64 &&  // Image Support if imageField is passed in
                    <Item style={[GlobalStyles.mt15,{backgroundColor: GlobalTheme.textFieldBackgroundColor}]}>
                        <Label style={[GlobalStyles.ml10,GlobalStyles.secondaryTextColor]}>{t(`Vehicle:license-plate`)}</Label>
                        <Icon name='information-circle' style={GlobalStyles.secondaryTextColor} />
                        <Input
                            onChangeText={(licensePlate) => {this.setState({
                                licensePlate: licensePlate
                            }); }}
                            value={this.state.licensePlate !== "false" ? this.state.licensePlate : undefined}
                            placeholder={this.state.analyzedLicensePlate ? (this.state.analyzedLicensePlate !== "false" ? this.state.analyzedLicensePlate + '*(' + t("Global:verify") + ')' : t("Vehicle:no-license-plate-found")) : t("Vehicle:analyzing")}
                            placeholderTextColor={GlobalTheme.textFieldPlaceholderColor}
                            style={[GlobalStyles.secondaryTextColor]}
                        />
                    </Item>
                }
                {this.state.imageBase64 &&  // Image Support if imageField is
                    <Item disabled style={[GlobalStyles.mt25,GlobalStyles.mb20, GlobalStyles.clearBorder,{backgroundColor: GlobalTheme.textFieldBackgroundColor}]}>
                        <Label style={[GlobalStyles.ml10,GlobalStyles.secondaryTextColor]}>{t(`Vehicle:analyzed-license-plate`)}</Label>
                        <Icon name='information-circle' style={GlobalStyles.secondaryTextColor} />
                        <Input disabled placeholder={this.state.analyzedLicensePlate ? (this.state.analyzedLicensePlate !== "false" ? this.state.analyzedLicensePlate : t("Vehicle:no-license-plate-found")) : t("Vehicle:analyzing")}
                               placeholderTextColor={GlobalTheme.secondaryTextColor}
                               style={[ GlobalStyles.clearBorder,GlobalStyles.secondaryTextColor]}
                         />
                    </Item>
                }
                </View>

                <Button block onPress={() => this.onSubmit()} style={[GlobalStyles.primaryButton]}>
                    <Text style={[GlobalStyles.primaryTextColor]}>{this.props.buttonText}</Text>
                </Button>
            </ScrollView>
        );
    }
}
