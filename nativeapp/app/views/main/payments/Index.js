// App Libraries
import React from 'react';

// Redux
import { connect } from 'react-redux';
import Auth from '../../../redux/reducers/auth';
import { updateAccessToken } from '../../../redux/actions/auth'

// Translations
import {t} from "../../../../translations/services/i18n";

// View Libraries
import {Container, Content, Tabs, Tab, View} from 'native-base';

// Global
const GlobalStyles = require('../../../components/stylesheets/GlobalStyles');
const GlobalTheme = require('../../../components/stylesheets/GlobalTheme');

// Custom Components
import { NavHeader } from '../../../components/main/NavHeader';

// Custom Models
import UserPayment from '../../../models/UserPayment';
import {PaymentHistory} from "./components/PaymentHistory";
import {PaymentSettings} from "./components/PaymentSettings";

export class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          userStripeCustomerID: this.props.userStripeCustomerID ? this.props.userStripeCustomerID : undefined,
          userPaymentData: [],
          userPaymentSettingsData: [],
          userCurrentStripePaymentSourceType: undefined,
          userCurrentStripePaymentSource: undefined
        };
        // Nav Header Settings
        this.headerSettings = {
          title: t("Payment:plural"),
          navigation: this.props.navigation,
          backPath: "Home",
          searchPath: true,
          location: false,
        }
    };

    async onUpdateUserPaymentSource(userCurrentStripePaymentSourceType,userCurrentStripePaymentSource) {
        this.setState({
            userCurrentStripePaymentSourceType: userCurrentStripePaymentSourceType,
            userCurrentStripePaymentSource: userCurrentStripePaymentSource,
        });
    }

    componentDidMount = () => {
        this.refreshComponent();
    };

    refreshComponent() {
        UserPayment.fetchUserChargesRequest(this,this.props.reduxState.Auth.user.stripe_customer_id);
    }

    render() {
        return(
          <Container>

            <NavHeader settings={this.headerSettings} />

            <Content contentContainerStyle={[GlobalStyles.primaryBackgroundColor,GlobalStyles.contentCenter]}>
                <Tabs tabBarUnderlineStyle={GlobalStyles.secondaryBackgroundColor}>
                    <Tab heading={t("Global:history")} style={[GlobalStyles.primaryBackgroundColor]} tabStyle={GlobalStyles.primaryTabBackgroundColor} activeTabStyle={GlobalStyles.activeTabBackgroundColor} textStyle={GlobalStyles.secondaryTextColor} activeTextStyle={GlobalStyles.secondaryTextColor}>
                        <PaymentHistory
                            tabLabel={t("Global:history")}
                            userPaymentData={this.state.userPaymentData}
                            onShow={(data) => UserPayment.showPath(this,data)}
                        />
                    </Tab>
                    <Tab heading={t("Settings:nav-title")} style={[GlobalStyles.primaryBackgroundColor]} tabStyle={GlobalStyles.primaryTabBackgroundColor} activeTabStyle={GlobalStyles.activeTabBackgroundColor} textStyle={GlobalStyles.secondaryTextColor} activeTextStyle={GlobalStyles.secondaryTextColor}>
                        <PaymentSettings
                            userStripeCustomerID={this.props.reduxState.Auth.user.stripe_customer_id}
                            onSetUserPayment={(currentType,currentSource) => this.onUpdateUserPaymentSource(currentType,currentSource)}
                        />
                    </Tab>
                </Tabs>
            </Content>

          </Container>
        );
    }
}

// Redux
const mapStateToProps = (state) => ({
	reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
  Auth,
  updateAccessToken
})(Index);