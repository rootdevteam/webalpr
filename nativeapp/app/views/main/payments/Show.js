// App Libraries
import React from 'react';

// Translations
import {t} from "../../../../translations/services/i18n";

// View Libraries
import { Image } from "react-native";
import { Container, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';

// Global
const GlobalStyles = require('../../../components/stylesheets/GlobalStyles');

//Custom Components
import { NavHeader } from '../../../components/main/NavHeader';

// Custom Models
import UserPayment from '../../../models/UserPayment';

export default class Show extends React.Component {
  constructor(props) {
    super(props);
    this.payment = this.props.navigation.state.params.payment;
    this.payment_image_url = this.payment[UserPayment.imageField] ? this.payment[UserPayment.imageField] : this.payment[UserPayment.remoteImageField];

    this.headerSettings = {
      title: this.payment.name,
      navigation: this.props.navigation,
      backPath: "PaymentsIndex",
    }
  }
  render() {
    return (
        <Container>

          <NavHeader settings={this.headerSettings} />

          <Content contentContainerStyle={[GlobalStyles.primaryBackgroundColor,GlobalStyles.contentCenter]}>

            <Card transparent>
              <CardItem style={[GlobalStyles.primaryBackgroundColor,GlobalStyles.clearBorder]}>
                {this.payment_image_url ? <Image source={{uri: this.payment_image_url, isStatic: true}} style={[GlobalStyles.vehicleImage,GlobalStyles.primaryBorder,{height: 400,width: 400}]}/>  : <Image source={{uri:'https://via.placeholder.com/400.png'}} style={[GlobalStyles.vehicleImage,GlobalStyles.primaryBorder,{height: 400,width: 400}]}/> }
              </CardItem>
              <CardItem style={[GlobalStyles.primaryBackgroundColor,GlobalStyles.clearBorder]}>
                <Left>
                  <Text style={[GlobalStyles.labelText,GlobalStyles.primaryTextColor]}>
                    {t(`Payment:analyzed-license-plate`)}
                  </Text>
                </Left>
                <Right>
                  <Text style={[GlobalStyles.labelText,GlobalStyles.secondaryTextColor]}>
                    {this.payment.alpr_license_plate !== "false" ? this.payment.alpr_license_plate : t(`Payment:no-result`)}
                  </Text>
                </Right>
              </CardItem>
              <CardItem style={[GlobalStyles.primaryBackgroundColor,GlobalStyles.clearBorder]}>
                <Left>
                  <Text style={[GlobalStyles.labelText,GlobalStyles.primaryTextColor]}>
                    {t(`Payment:license-plate`)}
                  </Text>
                </Left>
                <Right>
                  <Text style={[GlobalStyles.labelText,GlobalStyles.secondaryTextColor]}>
                    {this.payment.license_plate !== "false" ? this.payment.license_plate : t(`Payment:no-result`)}
                  </Text>
                </Right>
              </CardItem>
              <CardItem style={[GlobalStyles.primaryBackgroundColor,GlobalStyles.clearBorder]}>
                <Left>
                  <Text style={[GlobalStyles.labelText,GlobalStyles.primaryTextColor]}>
                    {t("Global:description")}
                  </Text>
                </Left>
              </CardItem>
              <CardItem style={[GlobalStyles.textCenter,GlobalStyles.primaryBackgroundColor,GlobalStyles.clearBorder]}>
                <Text style={[GlobalStyles.secondaryTextColor]}>
                  {this.payment.description}
                </Text>
              </CardItem>
            </Card>
          </Content>

        </Container>
    );
  }
}