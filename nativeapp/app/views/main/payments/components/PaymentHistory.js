// App Libraries
import React from 'react';

// View Libraries
import { Dimensions, StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import { Body, Button,Card, CardItem, Icon, Left, Right } from 'native-base';

// Translations
import {t} from "../../../../../translations/services/i18n";

// Date Time
var moment = require('moment');

// Global
const GlobalStyles = require('../../../../components/stylesheets/GlobalStyles');
const GlobalTheme = require('../../../../components/stylesheets/GlobalTheme');

export class PaymentHistory extends React.Component {
    constructor(props) {
    super(props);
      this.state = {
          basic: true,
          listData: []
      };
    };

    render() {
        if (this.props.userPaymentData && this.props.userPaymentData.length) {

            return (
                <Card
                    dataArray={this.props.userPaymentData}
                    style={[
                        GlobalStyles.defaultCard,
                        GlobalStyles.mt10
                    ]}
                    renderRow={(data) => (
                        <CardItem style={[
                            GlobalStyles.borderedCardItem,GlobalStyles.mb10
                        ]}>
                            <Left style={[
                                GlobalStyles.ml40
                            ]}>
                                <Text>${data.item.amount/100}</Text>
                            </Left>
                            <Body style={[
                                GlobalStyles.textCenter
                            ]}>
                                <Text note style={[
                                    {textTransform: 'capitalize'},
                                    (data.item.status == "succeeded" ? GlobalStyles.statusPaidColor : GlobalStyles.statusPendingColor)
                                ]}>{data.item.status}</Text>
                            </Body>
                            <Right>
                                <Text>{moment.unix(data.item.created).format('LLLL')}</Text>
                            </Right>
                        </CardItem>

                    )}
                >

                </Card>
            );

        }
        else {
            return (
                <Text>You have no payment history.</Text>
            );
        }

    }

}