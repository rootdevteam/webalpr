// App Libraries
import React from 'react';

// View Libraries
import { Platform, ScrollView, StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import {Body, Button, Card, CardItem, Content, Icon, Left, Picker, Right} from 'native-base';
import Modal from "react-native-modal";

// Translations
import {t} from "../../../../../translations/services/i18n";

// Global
const GlobalStyles = require('../../../../components/stylesheets/GlobalStyles');
const GlobalTheme = require('../../../../components/stylesheets/GlobalTheme');

// Custom Models
import UserPayment from "../../../../models/UserPayment";

// Custom Components
import {NewPaymentCardForm} from "./NewPaymentCardForm";

export class PaymentSettings extends React.Component {
    constructor(props) {
    super(props);
      this.state = {
          userStripeCustomerID: this.props.userStripeCustomerID ? this.props.userStripeCustomerID : undefined,
          userStripeDefaultPaymentSource: undefined,
          userStripeDefaultPaymentSourceType: undefined,
          userStripePaymentSources: undefined,
          isNewPaymentCardModalVisible: false,
          isUpdatePaymentCardModalVisible: false,
      };
    };

    componentDidMount = () => {
        this.refreshComponent();
    };

    async refreshComponent() {
        await UserPayment.getStripeCustomer(this);
        this.props.onSetUserPayment(this.state.userStripeDefaultPaymentSourceType,this.state.userStripeDefaultPaymentSource);
    }

    onPaymentTypeValueChange(value) {
        this.setState({
            userStripeDefaultPaymentSourceType: value
        });
        this.props.onSetUserPayment(this.state.userStripeDefaultPaymentSourceType,this.state.userStripeDefaultPaymentSource);
    }

    onOpenNewPaymentCardModal(){
        this.setState({
            isNewPaymentCardModalVisible: true,
        });
    }

    onCloseNewPaymentCardModal(){
        this.setState({
            isNewPaymentCardModalVisible: false,
        });
    }

    onOpenUpdatePaymentCardModal(newDefaultSource,newDefaultSourceLast4){
        if (this.state.userStripeDefaultPaymentSource !== newDefaultSource)
        {
            this.setState({
                isUpdatePaymentCardModalVisible: true,
                newDefaultSource: newDefaultSource,
                newDefaultSourceLast4: newDefaultSourceLast4
            });
        }
    }

    onCloseUpdatePaymentCardModal(){
        this.setState({
            isUpdatePaymentCardModalVisible: false,
        });
    }

    async onNewPaymentCardSubmit(){
        this.setState({
            isNewPaymentCardModalVisible: false
        });
        await UserPayment.getStripeCustomer(this);
        this.props.onSetUserPayment(this.state.userStripeDefaultPaymentSourceType,this.state.userStripeDefaultPaymentSource);
    }

    async onUpdateDefaultPaymentCard(){
        var customerParams = {
            'default_source': this.state.newDefaultSource
        };
        this.setState({
            isUpdatePaymentCardModalVisible: false
        });
        await UserPayment.updateStripeCustomer(this,customerParams);
        this.refreshComponent();
    }

    async onDeletePaymentCard(userStripeCustomerID,cardToken){
        await UserPayment.deleteCardFromStripeCustomer(userStripeCustomerID,cardToken);
        this.refreshComponent();
    }

    render() {
        return (
            <Card style={[GlobalStyles.defaultCard, GlobalStyles.mt10 ]}>
                <CardItem style={[GlobalStyles.clearBorder]}>
                    <Left><Text>Default payment method:</Text></Left>
                </CardItem>
                <CardItem style={[GlobalStyles.clearBorder]}>
                    <Left>
                        <Picker
                            mode="dropdown"
                            iosHeader="Methods"
                            iosIcon={<Icon name="arrow-down" style={{ color: GlobalTheme.primaryTextColor, fontSize: 25}} />}
                            placeholder="None"
                            placeholderStyle={{ color: "#000000" }}
                            style={ GlobalStyles.defaultPicker }
                            selectedValue={this.state.userStripeDefaultPaymentSourceType}
                            onValueChange={this.onPaymentTypeValueChange.bind(this)}
                        >
                            <Picker.Item label="None" value="none" />
                            <Picker.Item label="Card" value="stripe" />
                            <Picker.Item label="Paypal" value="paypal" />
                        </Picker>
                    </Left>

                </CardItem>

                {this.state.userStripeDefaultPaymentSourceType === "stripe" && // If Payment Type Card

                    <CardItem style={[GlobalStyles.clearBorder, GlobalStyles.mt10]}>
                        <Left><Text>Your Payment Cards:</Text></Left>
                    </CardItem>
                }

                {this.state.userStripeDefaultPaymentSourceType === "stripe" && // If Payment Type Card

                    this.state.userStripePaymentSources && // If Payment Card Sources

                        this.state.userStripePaymentSources.map((data, key) => {
                            return (
                                <CardItem key={key} style={[GlobalStyles.clearBorder]} >
                                    <Left>
                                        <Button onPress={() => this.onOpenUpdatePaymentCardModal(data.id,data.last4)}
                                                style={[GlobalStyles.textButton]}
                                        >
                                            {this.state.userStripeDefaultPaymentSource === data.id &&
                                            <Icon name="checkmark"
                                                  style={[{color: GlobalTheme.primaryTextColor, fontSize: 25},
                                                      GlobalStyles.mr10]}
                                            />
                                            }
                                            <Text>Card: {data.last4} </Text>
                                        </Button>
                                    </Left>

                                    <Right style={{ flex: 0}}>
                                        <Icon name="close" style={{ color: GlobalTheme.statusRedColor, fontSize: 25}}
                                              onPress={() => this.onDeletePaymentCard(this.state.userStripeCustomerID,data.id)}
                                        />
                                    </Right>

                                </CardItem>
                            )
                        })

                }

                {this.state.userStripeDefaultPaymentSourceType === "stripe" && // If Payment Type Card
                    <CardItem style={[GlobalStyles.clearBorder]}>
                        <Body>
                            <Button block onPress={() => this.onOpenNewPaymentCardModal()} style={[GlobalStyles.primaryButton]}>
                                <Text style={[GlobalStyles.primaryTextColor]}>{t("Global:add") + " " + t("Global:card")}</Text>
                            </Button>
                        </Body>
                    </CardItem>
                }

                {this.state.userStripeDefaultPaymentSourceType === "stripe" && // If Payment Type Card
                    <Modal isVisible={this.state.isNewPaymentCardModalVisible}>
                        <View style={[GlobalStyles.secondaryBackgroundColor,GlobalStyles.primaryBorder]}>
                            <Button block onPress={() => this.onCloseNewPaymentCardModal()} style={[GlobalStyles.closeButton]}>
                                <Text style={[GlobalStyles.primaryTextColor,GlobalStyles.closeText]}>{"x"}</Text>
                            </Button>
                            <NewPaymentCardForm
                                userStripeCustomerID={this.state.userStripeCustomerID}
                                onNewPaymentCardSubmit={() => this.onNewPaymentCardSubmit()}
                            />
                        </View>
                    </Modal>
                }

                {this.state.userStripeDefaultPaymentSourceType === "stripe" && // If Payment Type Card
                    <Modal isVisible={this.state.isUpdatePaymentCardModalVisible}>
                        <View style={[GlobalStyles.secondaryBackgroundColor,GlobalStyles.primaryBorder]}>
                            <Text style={[GlobalStyles.primaryTextColor]}>Make {this.state.newDefaultSourceLast4} default?</Text>
                            <Button block onPress={() => this.onUpdateDefaultPaymentCard()} style={[GlobalStyles.primaryButton]}>
                                <Text style={[GlobalStyles.primaryTextColor]}>{t("Global:Yes")}</Text>
                            </Button>
                            <Button block onPress={() => this.onCloseUpdatePaymentCardModal() } style={[GlobalStyles.primaryButton]}>
                                <Text style={[GlobalStyles.primaryTextColor]}>{t("Global:No")}</Text>
                            </Button>
                        </View>
                    </Modal>
                }
            </Card>

        );

    }

}