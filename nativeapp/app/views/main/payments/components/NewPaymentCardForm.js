// App Libraries
import React from 'react';

// Translations
import {t} from "../../../../../translations/services/i18n";

// View Libraries
import {Button, Container, Content, Text, View} from 'native-base';
import {CreditCardInput} from "react-native-credit-card-input";

// Global
const GlobalStyles = require('../../../../components/stylesheets/GlobalStyles');

// Custom Models
import UserPayment from '../../../../models/UserPayment';

export class NewPaymentCardForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            userStripeCustomerID: this.props.userStripeCustomerID ? this.props.userStripeCustomerID : undefined,
            cardData: { valid: false },
            submitted: false,
            error: null
        };
    }

    // Handles submitting the payment request
    async onSubmit(cardData) {
        await UserPayment.getCardStripeToken(this,cardData);
    };

    render() {
        return (

            <View style={ GlobalStyles.contentChildCenter }>
                <View>
                    <CreditCardInput requiresName onChange={(cardData) => this.setState({ cardData })} />
                </View>
                <Button block onPress={() => this.onSubmit(this.state.cardData)} style={[GlobalStyles.primaryButton]}>
                    <Text style={[GlobalStyles.primaryTextColor]}>{t("Global:add") + " " + t("Global:card")}</Text>
                </Button>
            </View>

        );
    }
}