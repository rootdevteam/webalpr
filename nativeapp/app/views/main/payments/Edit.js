// App Libraries
import React from 'react';

// Redux
import { connect } from 'react-redux';
import Auth from '../../../redux/reducers/auth';
import { updateAccessToken } from '../../../redux/actions/auth';

// Translations
import {t} from "../../../../translations/services/i18n";

// View Libraries
import { Container, Content, View } from 'native-base';

// Global
const GlobalStyles = require('../../../components/stylesheets/GlobalStyles');

// Custom Components
import { NavHeader } from '../../../components/main/NavHeader';
import { SimpleForm } from '../../../components/main/SimpleForm';

// Custom Models
import UserPayment from '../../../models/UserPayment';

export class Edit extends React.Component {
  constructor(props){
    super(props);
    // Nav Header Settings
    this.headerSettings = {
      title: t("Global:edit"),
      navigation: this.props.navigation,
      backPath: "PaymentsIndex",
    }
  }

  render() {
      return (

        <Container>

            <NavHeader settings={this.headerSettings} />

            <Content contentContainerStyle={[GlobalStyles.primaryBackgroundColor,GlobalStyles.contentCenter]}>
                <View style={ GlobalStyles.contentChildCenter }>
                    <SimpleForm
                        fields={UserPayment.fields()}
                        imageField={UserPayments.imageField}
                        onSubmit={(data,objectId) => UserPayment.updateRequest(this,data,objectId)}
                        buttonText={t(`Global:update`)}
                        formData={this.props.navigation.state.params.vehicle}
                        navigation={this.props.navigation}
                    />
                </View>
            </Content>

        </Container>
    );
  }
}

// Redux
const mapStateToProps = (state) => ({
    reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
    Auth,
    updateAccessToken
})(Edit);