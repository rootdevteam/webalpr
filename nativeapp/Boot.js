// App Libraries
import { AppLoading, Updates } from 'expo';
import * as Font from 'expo-font';
import React from 'react';
import BootSwitch from './config/routes/boot/BootSwitch';

// Redux
import { connect } from 'react-redux';
import Auth from './app/redux/reducers/auth';
import { updateAccessToken } from './app/redux/actions/auth'

// View Libraries
import { View, StatusBar, I18nManager as RNI18nManager } from 'react-native';

// Translations
import i18n from './translations/services/i18n';

// Global
const GlobalStyles = require('./app/components/stylesheets/GlobalStyles');

// Custom Components
import {PushNotifyManager} from "./app/components/main/PushNotifyManager";

export class Boot extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }
  async componentDidMount() {
    // Initialize internationalization
    i18n.init().then(() => {
      const RNDir = RNI18nManager.isRTL ? 'RTL' : 'LTR';

      // RN doesn't always correctly identify native
      // locale direction, so we force it here.
      if (i18n.dir !== RNDir) {
        const isLocaleRTL = i18n.dir === 'RTL';

        RNI18nManager.forceRTL(isLocaleRTL);

        // RN won't set the layout direction if we
        // don't restart the app's JavaScript.
        Updates.reloadFromCache();
      }
    }).catch((error) => console.warn(error));

      // Formbulder requires some font, there may be forms anywhere in the app so load them here
      await Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        Ionicons: require("@expo/vector-icons/build/vendor/react-native-vector-icons/Fonts/Ionicons.ttf")
    });

    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <AppLoading />;
    }
    return (
      <View style={GlobalStyles.contentCenter}>
        <StatusBar hidden={true} />
        <BootSwitch />
        <PushNotifyManager
            parent={this}
        />
      </View>
    );
  }
}

// Redux
const mapStateToProps = (state) => ({
  reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
  Auth,
  updateAccessToken
})(Boot);

