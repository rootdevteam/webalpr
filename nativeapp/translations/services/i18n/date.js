var moment = require('moment');

const date = {
    /**
     * Load library, setting its initial locale
     *
     * @param {string} locale
     * @return Promise
     */
    init(locale) {
        return new Promise((resolve, reject) => {
            moment.locale(locale);
        });
    },

    /**
     * @param {Date} date
     * @param {string} format
     * @return {string}
     */
    format(date, format) {
        return moment(date).format(format);
    }
};

export default date;