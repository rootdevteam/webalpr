export const fallback = "en";

export const supportedLocales = {
    en: {
        name: "English",
        translationFileLoader: () => require('../lang/en.json'),
    },
    ru: {
        name: "Русский",
        translationFileLoader: () => require('../lang/ru.json'),
    },
};

export const defaultNamespace = "Global";

export const namespaces = [
    "Global",
    "Settings",
    "About",
    "User",
    "Payment",
    "Vehicle",
    "Signin",
    "Signup",
    "Welcome",
    "Home",
];