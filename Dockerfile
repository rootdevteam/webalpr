# Install Ruby & Prepare for Rails Platform
FROM ruby:2.6.1

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    build-essential \
    cmake \
    curl \
    git \
    libcurl3-dev \
    libleptonica-dev \
    liblog4cplus-dev \
    libopencv-dev \
    libtesseract-dev \
    wget \
    libgtk2.0-dev \
    pkg-config \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev



# OpenAlpr Clone Source
RUN git clone https://bitbucket.org/rootdevteam/openalpr.git

# OpenAlpr Setup the build directory
RUN mkdir -p /openalpr/src/build
WORKDIR /openalpr/src/build

# OpenAlpr Build & Install
RUN cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr -DCMAKE_INSTALL_SYSCONFDIR:PATH=/etc .. && \
    make -j2 && \
    make install

WORKDIR /

# Clone Russian ANPR from bitbucket
RUN git clone https://mirabbos@bitbucket.org/mirabbos/rusalpr.git
WORKDIR /rusalpr

# Build source code
RUN make


# Rails Throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

# Rails Install dependencies
RUN  apt-get update -qq && apt-get install -y libffi-dev zlib1g-dev liblzma-dev libgmp-dev nodejs

# Rails Copy Gemfile and install all Rails website dependencies
WORKDIR /usr/src/app
COPY Gemfile Gemfile.lock ./
RUN bundle install

# Rails Copy all files from repo to container
COPY . .

# Start Rails Web App
CMD ["bundle exec puma -C config/puma.rb"]