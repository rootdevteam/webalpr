class CreateServicesTable < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :name
      t.string :description
      t.string :service_image_url
      t.decimal :price, :precision => 8, :scale => 2
      t.datetime :subscription_date
      t.datetime :expiry_date
      t.boolean :is_available, default: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
