class CreateDriversTable < ActiveRecord::Migration[5.2]
  def change
    create_table :drivers do |t|
      t.string :driver_license_number
      t.string :driver_license_img
      t.string :expiry_date
      t.datetime :birth_date
      t.boolean :is_available, default: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
