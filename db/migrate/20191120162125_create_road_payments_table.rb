class CreateRoadPaymentsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :road_payments do |t|
      t.integer :status, default: 0
      t.string :token
      t.string :charge_id
      t.string :error_message
      t.string :customer_id
      t.integer :payment_gateway
      t.references :user, foreign_key: true
      t.references :vehicle, foreign_key: true

      t.timestamps
    end
    add_money :road_payments, :amount, currency: { present: false }
  end
end
