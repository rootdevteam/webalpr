class AddColumnToVehicles < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicles, :alpr_license_plate, :string
  end
end
