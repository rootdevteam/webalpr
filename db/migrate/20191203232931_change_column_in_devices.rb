class ChangeColumnInDevices < ActiveRecord::Migration[5.2]
  def change
    remove_column :devices, :push_token
    add_column :devices, :pushToken, :string
  end
end
