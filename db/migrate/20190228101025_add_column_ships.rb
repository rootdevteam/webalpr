class AddColumnShips < ActiveRecord::Migration[5.2]
  def change
    add_column :ships, :ship_image_url, :string
    add_column :ships, :capacity, :integer
  end
end
