class CreateDocks < ActiveRecord::Migration[5.2]
  def change
    create_table :docks do |t|
      t.string :name
      t.references :ship, foreign_key: true
      t.string :arrival
      t.string :departure

      t.timestamps
    end
  end
end
