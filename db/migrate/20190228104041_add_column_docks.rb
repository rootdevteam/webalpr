class AddColumnDocks < ActiveRecord::Migration[5.2]
  def change
    add_column :docks, :dock_image_url, :string
    add_column :docks, :location, :string
    add_column :docks, :description, :string
  end
end
