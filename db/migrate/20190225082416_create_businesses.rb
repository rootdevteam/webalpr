class CreateBusinesses < ActiveRecord::Migration[5.2]
  def change
    create_table :businesses do |t|
      t.string :name
      t.string :description
      t.string :unique_id         #unique identifier for the company
      t.string :company_type
      t.string :address
      t.string :zip
      t.string :city
      t.string :region
      t.string :email_domain
      t.string :contacts
      t.string :website_url
      t.string :logo_url
      t.datetime :established
      t.datetime :expiry_date
      t.boolean :is_available, default: true
      t.boolean :confirmation, default: false
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
