class ChangeColumnTypeInParkingPayments < ActiveRecord::Migration[5.2]
  def change
    remove_column :parking_payments, :payment_gateway
    add_column :parking_payments, :payment_gateway, :string
    add_column :parking_payments, :business_id, :integer
  end
end
