class AddColumnVehicles < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicles, :vehicle_image_url, :string
  end
end
