class CreateAddress < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :address_primary
      t.string :address_secondary
      t.string :city
      t.string :region
      t.string :zip
      t.string :country
      t.references :business, foreign_key: true

      t.timestamps
    end
  end
end
