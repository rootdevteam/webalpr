class RenameColumnDrivers < ActiveRecord::Migration[5.2]
  def change
    remove_column :drivers, :vgyme_image_url
    add_column :drivers, :license_image_url, :string
  end
end
