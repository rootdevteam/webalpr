class RemoveColumnsFromBussiness < ActiveRecord::Migration[5.2]
  def change
    remove_column :businesses, :address
    remove_column :businesses, :contacts
    remove_column :businesses, :unique_id
    remove_column :businesses, :zip
    remove_column :businesses, :city
    remove_column :businesses, :region
    remove_column :businesses, :website_url
    remove_column :businesses, :expiry_date
    remove_column :businesses, :email_domain
    add_column :businesses, :certificate, :string
    add_column :businesses, :company_reg_number, :string
    add_column :businesses, :auth_token, :string
  end
end
