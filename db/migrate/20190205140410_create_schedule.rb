class CreateSchedule < ActiveRecord::Migration[5.2]
  def change
    create_table :schedules do |t|
      t.references :dock, foreign_key: true
      t.references :ship, foreign_key: true
      t.string :arrival
      t.string :departure
      t.string :description
    end
  end
end
