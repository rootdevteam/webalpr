class AddColumnToTollDeviceRequestHistory < ActiveRecord::Migration[5.2]
  def change
    add_column :toll_device_request_histories, :hours, :"double"
  end
end
