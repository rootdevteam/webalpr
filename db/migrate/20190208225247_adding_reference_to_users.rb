class AddingReferenceToUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :vehicles, :users_id
    add_reference :vehicles, :user, foreign_key: true
  end
end
