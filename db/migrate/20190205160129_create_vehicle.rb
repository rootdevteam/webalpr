class CreateVehicle < ActiveRecord::Migration[5.2]
  def change
    create_table :vehicles do |t|
      t.references :users, foreign_key: true
      t.string :name
      t.string :license_plate
      t.string :description
      t.string :model
      t.string :type
      t.string :vgyme_image_url
    end
  end
end
