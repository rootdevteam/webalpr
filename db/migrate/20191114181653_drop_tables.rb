class DropTables < ActiveRecord::Migration[5.2]
  def change
    drop_table :docks
    drop_table :schedules
    drop_table :ships
  end
end
