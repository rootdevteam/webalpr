class AddPlanToBussiness < ActiveRecord::Migration[5.2]
  def change
    add_column :businesses, :stripe_plan_name, :string
    add_column :businesses, :paypal_plan_name, :string
  end
  add_money :businesses, :price, currency: { present: true }
end
