class AddDeclinedToTollDeviceRequestHistory < ActiveRecord::Migration[5.2]
  def change
    add_column :toll_device_request_histories, :declined_by, :string
    add_column :toll_device_request_histories, :accepted_by, :integer
  end
end
