class RemoveAmountFromParkingPayments < ActiveRecord::Migration[5.2]
  def change
    remove_column :parking_payments, :amount_cents
    remove_column :road_payments, :amount_cents

    add_column :parking_payments, :amount, :decimal, :precision => 8, :scale => 2
    add_column :road_payments, :amount, :decimal, :precision => 8, :scale => 2
  end
end
