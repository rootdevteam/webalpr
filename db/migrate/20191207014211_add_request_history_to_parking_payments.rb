class AddRequestHistoryToParkingPayments < ActiveRecord::Migration[5.2]
  def change
    add_column :parking_payments, :request_history,:integer
  end
end
