class CreateParkingPaymentsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :parking_payments do |t|
      t.decimal :hours
      t.integer :status, default: 0
      t.string :token
      t.string :charge_id
      t.string :error_message
      t.string :customer_id
      t.integer :payment_gateway
      t.references :user, foreign_key: true
      t.references :vehicle, foreign_key: true

      t.timestamps
    end
    add_money :parking_payments, :amount, currency: { present: false }
  end
end
