class CreateBusinessPaymentsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :business_payments do |t|
      t.integer :status, default: 0
      t.string :token
      t.string :charge_id
      t.string :error_message
      t.string :customer_id
      t.integer :payment_gateway
      t.string :subscription_plan
      t.references :business, foreign_key: true
      t.datetime :start_date
      t.datetime :expiry_date
      t.timestamps
    end
    add_money :business_payments, :amount, currency: { present: false }
  end
end
