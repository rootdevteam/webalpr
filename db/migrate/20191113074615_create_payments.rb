class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.references :vehicles, foreign_key: true
      t.references :users, foreign_key: true
      t.integer :status, default: 0
      t.string :token
      t.string :charge_id
      t.string :error_message
      t.string :customer_id
      t.integer :payment_gateway
      t.decimal :hours
      t.timestamps
    end
    add_money :payments, :amount, currency: { present: false }
  end
end
