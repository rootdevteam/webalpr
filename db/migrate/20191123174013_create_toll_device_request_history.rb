class CreateTollDeviceRequestHistory < ActiveRecord::Migration[5.2]
  def change
    create_table :toll_device_request_histories do |t|
      t.timestamp :start_time
      t.timestamp :end_time
      t.integer :vehicle_id

      t.references :toll_device, foreign_key: true
    end
  end
end
