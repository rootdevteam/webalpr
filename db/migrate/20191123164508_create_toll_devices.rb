class CreateTollDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :toll_devices do |t|
      t.string :name
      t.string :description
      t.string :model
      t.string :device_token
      t.string :toll_type
      t.string :device_image_url
      t.decimal :price, :precision => 8, :scale => 2

      t.references :business, foreign_key: true
    end
  end
end
