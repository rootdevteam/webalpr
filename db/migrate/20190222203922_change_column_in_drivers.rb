class ChangeColumnInDrivers < ActiveRecord::Migration[5.2]
  def change
    remove_column :drivers, :driver_license_img
    add_column :drivers, :vgyme_image_url, :string
  end
end
