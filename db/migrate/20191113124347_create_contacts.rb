class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :email_primary
      t.string :email_emergency
      t.string :telephone_primary
      t.string :telephone_emergency
      t.string :website_url
      t.string :fax
      t.references :business, foreign_key: true

      t.timestamps
    end
  end
end
