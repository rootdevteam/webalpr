# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_14_203730) do

  create_table "addresses", force: :cascade do |t|
    t.string "address_primary"
    t.string "address_secondary"
    t.string "city"
    t.string "region"
    t.string "zip"
    t.string "country"
    t.integer "business_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["business_id"], name: "index_addresses_on_business_id"
  end

  create_table "business_payments", force: :cascade do |t|
    t.integer "status", default: 0
    t.string "token"
    t.string "charge_id"
    t.string "error_message"
    t.string "customer_id"
    t.integer "payment_gateway"
    t.string "subscription_plan"
    t.integer "business_id"
    t.datetime "start_date"
    t.datetime "expiry_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "amount_cents", default: 0, null: false
    t.index ["business_id"], name: "index_business_payments_on_business_id"
  end

  create_table "businesses", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "company_type"
    t.string "logo_url"
    t.datetime "established"
    t.boolean "is_available", default: true
    t.boolean "confirmation", default: false
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "certificate"
    t.string "company_reg_number"
    t.string "auth_token"
    t.integer "price_cents", default: 0, null: false
    t.string "price_currency", default: "USD", null: false
    t.string "stripe_plan_name"
    t.string "paypal_plan_name"
    t.index ["user_id"], name: "index_businesses_on_user_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "email_primary"
    t.string "email_emergency"
    t.string "telephone_primary"
    t.string "telephone_emergency"
    t.string "website_url"
    t.string "fax"
    t.integer "business_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["business_id"], name: "index_contacts_on_business_id"
  end

  create_table "devices", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "platform"
    t.string "installationId"
    t.string "expoVersion"
    t.string "appOwnership"
    t.string "systemName"
    t.string "systemVersion"
    t.string "deviceName"
    t.string "deviceYearClass"
    t.string "sessionId"
    t.string "isDevice"
    t.string "udid"
    t.string "pushToken"
    t.index ["user_id"], name: "index_devices_on_user_id"
  end

  create_table "drivers", force: :cascade do |t|
    t.string "driver_license_number"
    t.string "expiry_date"
    t.datetime "birth_date"
    t.boolean "is_available", default: true
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "license_image_url"
    t.index ["user_id"], name: "index_drivers_on_user_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "speed"
    t.string "longitude"
    t.string "latitude"
    t.string "accuracy"
    t.string "heading"
    t.string "altitude"
    t.string "altitudeAccuracy"
    t.integer "device_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["device_id"], name: "index_locations_on_device_id"
  end

  create_table "parking_payments", force: :cascade do |t|
    t.decimal "hours"
    t.integer "status", default: 0
    t.string "token"
    t.string "charge_id"
    t.string "error_message"
    t.string "customer_id"
    t.integer "user_id"
    t.integer "vehicle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "amount", precision: 8, scale: 2
    t.string "payment_gateway"
    t.integer "business_id"
    t.integer "request_history"
    t.index ["user_id"], name: "index_parking_payments_on_user_id"
    t.index ["vehicle_id"], name: "index_parking_payments_on_vehicle_id"
  end

  create_table "road_payments", force: :cascade do |t|
    t.integer "status", default: 0
    t.string "token"
    t.string "charge_id"
    t.string "error_message"
    t.string "customer_id"
    t.integer "payment_gateway"
    t.integer "user_id"
    t.integer "vehicle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "amount", precision: 8, scale: 2
    t.index ["user_id"], name: "index_road_payments_on_user_id"
    t.index ["vehicle_id"], name: "index_road_payments_on_vehicle_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.integer "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "service_image_url"
    t.decimal "price", precision: 8, scale: 2
    t.datetime "subscription_date"
    t.datetime "expiry_date"
    t.boolean "is_available", default: true
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_services_on_user_id"
  end

  create_table "toll_device_request_histories", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer "vehicle_id"
    t.integer "toll_device_id"
    t.float "hours"
    t.string "declined_by"
    t.integer "accepted_by"
    t.index ["toll_device_id"], name: "index_toll_device_request_histories_on_toll_device_id"
  end

  create_table "toll_devices", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "model"
    t.string "device_token"
    t.string "toll_type"
    t.string "device_image_url"
    t.decimal "price", precision: 8, scale: 2
    t.integer "business_id"
    t.index ["business_id"], name: "index_toll_devices_on_business_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.text "tokens"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "username"
    t.string "first_name"
    t.string "last_name"
    t.string "gender"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "profile_image_url"
    t.string "stripe_customer_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  create_table "vehicles", force: :cascade do |t|
    t.string "name"
    t.string "license_plate"
    t.string "description"
    t.string "model"
    t.string "type"
    t.string "vgyme_image_url"
    t.integer "user_id"
    t.string "alpr_license_plate"
    t.string "vehicle_image_url"
    t.index ["user_id"], name: "index_vehicles_on_user_id"
  end

end
