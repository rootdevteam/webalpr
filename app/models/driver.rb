class Driver < ApplicationRecord
  belongs_to :user


  def image_url

    #fetch video image url
    @image_url = self.license_image_url

    #if image is blank or missing use company image
    if @image_url.nil? or @image_url.blank?

      #if video has a company assigned use company image
      if self.company
        @image_url =  self.company.image_url
      end

      #if company image is blank or missing
      if @image_url.nil? or @image_url.blank?

        @image_url = "https://vgy.me/zJB9id.png" #Set no image supplied

      else

        @image_url #return company image

      end

    else
      @image_url #return current image
    end

  end
end