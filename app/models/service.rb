class Service < ApplicationRecord
  belongs_to :user

  validates :user_id, presence: true
  validates :name, presence: true

  #image url method
  def image_url

    #fetch video image url
    @image_url = self.service_image_url

      #if company image is blank or missing
      if @image_url.nil? or @image_url.blank?

        @image_url = "https://vgy.me/zJB9id.png" #Set no image supplied

      else

        @image_url #return company image

      end
  end

end




