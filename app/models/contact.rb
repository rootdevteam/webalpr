class Contact < ApplicationRecord
  belongs_to :business

  validates :business_id, presence: true
  validates :email_emergency, presence: true
  validates :telephone_emergency, presence: true

end