class Address < ApplicationRecord
  belongs_to :business

  validates :business_id, presence: true
  validates :address_primary, presence: true
  validates :city, presence: true
  validates :region, presence: true
  validates :country, presence: true


end