class ParkingPayment < ApplicationRecord
  enum status: { pending: 0, failed: 1, paid: 2, paypal_executed: 3}
    belongs_to :vehicle
    belongs_to :user

    validates :payment_gateway, presence: true
    validates :customer_id, presence: true
    validates :amount, presence: true
    validates :user_id, presence: true
    validates :business_id, presence: true
    validates :token, presence: true
    validates :hours, presence: true
    validates :charge_id, presence: true

    scope :recently_created, ->  { where(created_at: 1.minutes.ago..DateTime.now) }

    def set_paid
      self.status = ParkingPayment.statuses[:paid]
    end
    def set_failed
      self.status = ParkingPayment.statuses[:failed]
    end
    def set_paypal_executed
      self.status = ParkingPayment.statuses[:paypal_executed]
    end
end