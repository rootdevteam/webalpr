class TollDeviceRequestHistory < ApplicationRecord
  belongs_to :toll_device

  validates :toll_device_id, presence: true
  validates :vehicle_id, presence: true

  # Get last toll device request
  def self.get_last_history(vehicle)
    @rh = self.where(:vehicle_id =>  vehicle.id).order(start_time: :desc).first
    puts "LAST REQUEST HISTORY FOUND!", @rh
    return @rh
  end

  # Calculate parking payment amount
  def self.payment_amount(request_history)
    if request_history.instance_of? self
      toll_device = TollDevice.find(request_history.toll_device_id)
      amount = request_history.hours * toll_device.price.truncate(2)
      @amount = (amount*100).to_i
      puts "AMOUNT CALCULATED! " + amount.to_s
      return @amount
    else
      toll_device = TollDevice.find(get_request_history(request_history).toll_device_id)
      amount = get_request_history(request_history).hours * toll_device.price.truncate(4)
      @amount = (amount*100).to_i
      puts "AMOUNT CALCULATED!", @amount.truncate(4).to_s
      return @amount
    end
  end

  # Get toll device by request_history
  def self.request_business(request_history)
    if request_history.present?
      @business = TollDevice.find(get_request_history(request_history).toll_device_id).business_id
      puts "BUSINESS FOUND! " + @business.to_s
      return @business
    else
      puts "ERROR: BUSINESS NOT FOUND! " + @business
    end
  end

  # Get  request history
  def self.get_request_history(request_history)
    if request_history.present?
      @request_history = self.find(request_history)
      puts "REQUEST HISTORY FOUND! " + @request_history.id.to_s
      return @request_history
    else
      puts "ERROR: REQUEST HISTORY NOT FOUND!"
    end
  end

end