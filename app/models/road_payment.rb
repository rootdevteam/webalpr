class RoadPayment < ApplicationRecord
  enum status: {pending: 0, failed: 1, paid: 2, paypal_executed: 3}
  enum payment_gateway: {stripe: 0, paypal: 1}
  belongs_to :vehicle
  belongs_to :user


  scope :recently_created, -> {where(created_at: 1.minutes.ago..Datetime.now)}

  def set_paid
    self.status = Payment.status[:paid]
  end

  def set_failed
    self.status = Payment.status[:failed]
  end

  def set_paypal_executed
    self.status = Payment.status[:paypal_executed]
  end
end