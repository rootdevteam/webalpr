class TollDevice < ApplicationRecord
  belongs_to :business

  validates :device_token, presence: true
  validates :business_id, presence: true
  validates :name, presence: true
  validates :price, presence: true


  require 'open-uri'
  require 'rmagick'
  require 'json'
  # License plate: Rusalpr and analyze
  def self.analyze(url)

    # Get the image url
    @url = url
    home_path = Dir.pwd
    Dir.chdir("config/openalpr/configs/")
    image_path = Dir.pwd
    # Create file with image name
    image = File.basename("analyze.png")

    # Copy image data from url to file
    File.open(image, 'wb') do |f|
      f.write(Base64.decode64(@url['data:image/png;base64,'.length .. -1]))
    end

    # logger.debug "Downloaded File: " + image

    @best_result = {}
    @current_best_result = {}
    # Perform default rusalpr analysis

    #print "HOME PATH: ", Dir.pwd
    if Rails.env.production?
      Dir.chdir("/")
    else
      Dir.chdir(home_path)
    end
    Dir.chdir("rusalpr")
    @rusalpr_result = `./rusalpr #{image_path}/#{image}`
    puts "ANALYZED LICENSE PLATE:  ", @rusalpr_result
    Dir.chdir("#{home_path}")

    if @rusalpr_result.nil? or @rusalpr_result.length < 6
      # Perform default custom Multi Config Alpr Analysis
      @best_result = multiAlprAnalyze(image)

      # Perform Grayscale analysis, if no results above 80 which match the pattern from first analysis
      if (@best_result.empty? ? true : @best_result["confidence"] < 80 || @best_result.empty? ? true : @best_result["matches_template"] != 1)
        logger.debug "No good matches found... Performing Grayscale analysis... "
        grimage = "gr#{image}"
        rm_image = Magick::Image.read(image).first
        rm_image.quantize(256, Magick::GRAYColorspace).normalize.write(grimage)
        @current_best_result = multiAlprAnalyze(grimage)
        File.delete(grimage) if File.exist?(grimage)
        compare_candidates(@best_result,@current_best_result)
      end

      # Set response result
      if @best_result.empty?
        @result = "false"
      else
        @result = @best_result["plate"]
      end
    else
      @result = @rusalpr_result
    end

    # Delete Original image from server
    File.delete("#{image_path}/#{image}") if File.exist?("#{image_path}/#{image}")
    logger.debug "Images cleaned up!"

    return @result
  end

  def multi_alpr_analyze(image)

    best_result = {}
    current_best_result = {}

    # Path to custom configs
    config_path = "config/openalpr/configs/"

    # Main ALPR Algorithm configs
    alpr_configs = ["rualpr1","rualpr2","rualpr3","rualpr4","rualpr5"]

    # Custom Russian country configs
    ru_configs = ["ru1","ru2"]

    # Run through custom configurations and custom results filter
    alpr_configs.each do |alpr_config|
      ru_configs.each do |ru_config|

        # Run Alpr with specific configuration
        current_best_result = runAlpr(config_path+alpr_config,ru_config,"ru",image)
        logger.debug "Current Best Result:  #{current_best_result.to_s}"

        # STOP if a instant match is found
        return current_best_result if current_best_result["match_found"]

        # Compare/Set current best result with all time saved best result
        compare_candidates(best_result,current_best_result)
        logger.debug "Best Result:  #{best_result.to_s}"

      end
    end

    # Return best result
    best_result

  end

  def run_alpr(config,country,pattern,image,json=true)

    logger.debug "Running Alpr on #{image} using Config: #{config} Country: #{country}"

    best_result = {}
    current_best_candidate = {}

    # Run command with JSON response by default, unless disabled
    if json
      json = "-j"
    else
      json = ""
    end

    alpr_command = "alpr --config #{config}.conf -c #{country} -p #{pattern} #{image} #{json}"
    logger.debug "cmd " + alpr_command

    response = `#{alpr_command}`

    #logger.debug "RESPONSE " + @response

    response_JSON = JSON.parse(response)

    results = response_JSON["results"]

    #logger.debug "RESULTS " + @results.to_s

    unless results.empty?

      # For each License plate result found get best candidate and compare it to our current best candidate
      results.each do |result|

        # Find best candidate from current result
        current_best_candidate = find_best_candidate(result["candidates"])

        # STOP if a instant match is found
        return current_best_candidate if current_best_candidate["match_found"]

        # Compare current best to all time best and set if better
        compare_candidates(best_result,current_best_candidate)

      end

    end

    # Return best result
    best_result

  end

  def find_best_candidate(candidates)

    current_candidate = {}
    best_candidate = {}

    candidates.each do |candidate|

      # Set current candidate variable from our object in the loop using custom method
      set_candidate(current_candidate,candidate)

      # Instant match filter, anything matching pattern above 85% is a winner, stop analyzing

      if (current_candidate["confidence"] >= 85) && (current_candidate["matches_template"] == 1)

        set_candidate(best_candidate,current_candidate)
        best_candidate["match_found"] = true

        return best_candidate

      else

        # Compare current candidate to the best and set if it's better
        compare_candidates(best_candidate,current_candidate)
        best_candidate["match_found"] = false

      end

    end

    best_candidate

  end

  def compare_candidates(best_candidate,current_candidate)

    # Skip comparing if no current result exists
    unless current_candidate.empty?

      # If no best, set current as best
      if best_candidate.empty?

        set_candidate(best_candidate,current_candidate)

      else # Filter candidates by pattern match and confidence

        # Only change the best candidate IF..

        # If Current candidate MATCHES pattern and has GREATER confidence than Best, return it
        if current_candidate["matches_template"] == 1 && current_candidate["confidence"] >= best_candidate["confidence"]

          set_candidate(best_candidate,current_candidate)

          # Also If Current candidate matches pattern, and Best does NOT, return it
        elsif current_candidate["matches_template"] == 1 && best_candidate["matches_template"]!= 1

          set_candidate(best_candidate,current_candidate)

          # Also If Current candidate confidence GREATER, and Best does NOT match pattern, return it
        elsif current_candidate["confidence"] >= best_candidate["confidence"] && best_candidate["matches_template"] != 1

          set_candidate(best_candidate,current_candidate)

        end

      end

    end

  end

  def set_candidate(best_candidate,current_candidate)
    best_candidate["plate"] = current_candidate["plate"]
    best_candidate["confidence"] = current_candidate["confidence"].to_f.ceil # Convert to float & always round up
    best_candidate["matches_template"] = current_candidate["matches_template"]
  end


  # Get Toll device by device_token
  def self.get_toll_device(device_token)
    unless device_token.nil?
      @toll_device = self.find_by(:device_token => device_token)
      unless @toll_device.nil?
        @toll_device
      end
    end
  end


  # Get image url from vgyme
  def image_url
    #fetch video image url
    @image_url = self.device_image_url
    #if image is blank or missing use company image
    if @image_url.nil? or @image_url.blank?
        @image_url = "https://vgy.me/zJB9id.png" #Set no image supplied
    else
      @image_url #return current image
    end

  end
end