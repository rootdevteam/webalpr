class Vehicle < ApplicationRecord
  belongs_to :user

  validates :user_id, presence: true
  validates :name, presence: true

  # Get vehicle by license plate
  def self.get_vehicle(url)
    @result = TollDevice.analyze(url).gsub("\n", "")
    unless url.nil? or @result.nil?
      @vehicle = self.find_by(:license_plate => @result)
      unless @vehicle.nil?
        puts "VEHICLE FOUND!", @vehicle
        return @vehicle
      end
    end
  end

  #image url method
  def image_url

    #fetch video image url
    @image_url = self.vgyme_image_url

    #if image is blank or missing use company image
    if @image_url.nil? or @image_url.blank?

      #if video has a company assigned use company image
      if self.company
        @image_url =  self.company.image_url
      end

      #if company image is blank or missing
      if @image_url.nil? or @image_url.blank?

        @image_url = "https://vgy.me/zJB9id.png" #Set no image supplied

      else

        @image_url #return company image

      end

    else
      @image_url #return current image
    end

  end

  #image url method
  def vehicle_img_url

    #fetch video image url
    @image_url = self.vehicle_image_url

    #if image is blank or missing use company image
    if @image_url.nil? or @image_url.blank?

      #if video has a company assigned use company image
      if self.company
        @image_url =  self.company.image_url
      end

      #if company image is blank or missing
      if @image_url.nil? or @image_url.blank?

        @image_url = "https://vgy.me/zJB9id.png" #Set no image supplied

      else

        @image_url #return company image

      end

    else
      @image_url #return current image
    end

  end
end




