class Device < ApplicationRecord
  belongs_to :user
  has_many :location


  # Get device by user and do push notification
  def self.notify_device(users, business, request_history)
    unless user.nil? and business.nil?
      @devices = self.where(:user_id => users.ids)
      puts "Devices", @devices.first.deviceName
      if @devices.count > 0
        @devices.each do |device|
          unless device.pushToken.nil?
            if request_history.hours.nil?
              client = Exponent::Push::Client.new
              messages = [{
                              to: device.pushToken,
                              sound: "default",
                              title: business.name + " Vehicle Entered Parking",
                              body: business.name + " Vehicle Entered Parking",
                              data:  {
                                  parking_state: "entered",
                                  request_history: request_history
                              }
                          }]

              client.publish messages
              puts "VEHICLE ENTERED PARKING! "
              puts "PUSHED NOTIFICATION TO "+ device.deviceName.to_s
            else
              client = Exponent::Push::Client.new
              payment_amount = TollDeviceRequestHistory.payment_amount(request_history).to_s
              messages = [{
                              to: device.pushToken,
                              sound: "default",
                              title: "Vehicle Left Parking",
                              badge: 1,
                              body:  "Hours stayed: " + request_history.hours.to_s + " Amount to be paid: $" + payment_amount,
                              data:  {
                                  parking_state: "exited",
                                  payment_amount: payment_amount,
                                  request_history: request_history
                              }
                          }]

              client.publish messages
              puts "VEHICLE LEFT PARKING! "
              puts "PUSHED NOTIFICATION TO "+ device.deviceName.to_s
            end
          end
        end
      end
    end
  end

  # Push message to device
  def self.push_message(message, pushToken)
    if message.present?
      client = Exponent::Push::Client.new
      messages = [{
                      to: pushToken,
                      sound: "default",
                      data: {
                          message: message
                      }
                  }]
      client.publish messages
    end
  end

end
