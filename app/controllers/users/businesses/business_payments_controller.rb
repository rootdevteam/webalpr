class Users::Businesses::BusinessPaymentsController < ApplicationController
  before_action :authenticate_user!

  def index
    @business_payments = BusinessPayment.all
  end

  def user_payments
    @business_payments = BusinessPayment.where(:users_id => @current_user.id)
  end

  def create

  end

  def show

  end

  def edit

  end

  def update

  end

  private


  def set_payment
    @business_payment = BusinessPayment.find(params[:business_payment_id])
  end


  def payment_params
    if Rails::VERSION::MAJOR < 4
      params[:business_payment].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:business_payment).permit(Contact.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end