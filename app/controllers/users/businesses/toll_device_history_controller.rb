class Users::Businesses::TollDeviceHistoryController < ApplicationController
  before_action :authenticate_user!

  FAILED_ERROR_MESSAGE = "Request failed!"

  # POST /toll_device
  def create
    @url = params[:url]
    @device_token = params[:device_token]
    @toll_device = TollDevice.find_by(:device_token => @device_token)
    @time = DateTime.now
    @result = "E831HM77"
    @vehicle = Vehicle.find_by(:license_plate => @result)
    @user = User.find_by(:id => @vehicle.user_id)
    @tdrh = TollDeviceRequestHistory.where(:vehicle_id => @vehicle.id)
    unless @toll_device.nil?
      unless @vehicle.nil? or @vehicle.blank?
        unless @user.nil? or @user.blank?
          if @tdrh.count > 0
            @tdrh.each do |tdh|
              unless tdh.end_time.nil?
                @req = TollDeviceRequestHistory.create(start_time: @time, vehicle_id: @vehicle.id, toll_device_id: @toll_device.id)
                render device_history_path, locals: { resource:  @req}
              else
                @req = tdh.update(:end_time => DateTime.now)
                render device_history_path, locals: { resource:  @req}
              end
            end
          else
            TollDeviceRequestHistory.create(start_time: @time, vehicle_id: @vehicle.id, toll_device_id: @toll_device.id)
          end
        end
      else
        redirect_to account_path, notice: "Car not found!"
      end
    else
      redirect_to account_path, notice: "Toll device not found!"
    end
  end

  def update

  end


  private

  def get_toll_device(device_token)
    @toll_device = TollDevice.find_by(:device_token => device_token)
    unless @toll_device.nil?
      @toll_device
    else
      FAILED_ERROR_MESSAGE
    end
  end

  def get_request_history(vehicle)
    @request_history = TollDeviceRequestHistory.where(:vehicle_id =>  vehicle.id).order(start_time: :desc).first
  end

  def get_vehicle(url)
    @result = "E831HM77"
    @vehicle = Vehicle.find_by(:license_plate => @result)
    unless @vehicle.nil?
      @vehicle
    else
      FAILED_ERROR_MESSAGE
    end
  end

  def get_business(toll_device)
    @business = Business.find_by(:id => toll_device.business_id)
    unless @business.nil?
      @business
    else
      FAILED_ERROR_MESSAGE
    end
  end

  def get_current_time
    @time = DateTime.now
  end

  def get_vehicle_owner(vehicle)
    @owner = User.find_by(:id => vehicle.user_id)
    unless @owner.nil?
      @owner
    else
      FAILED_ERROR_MESSAGE
    end
  end

end