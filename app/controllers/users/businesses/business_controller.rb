class Users::Businesses::BusinessController < ApplicationController
  before_action :set_business, only: [:show, :update, :destroy]
  before_action :business_only, only: [:new, :create]
  before_action :authenticate_user!

  # GET /business
  def index
    @businesses = Business.where(:user_id => @current_user.id)
  end

  def new
    @business = Business.new
  end


  # GET /businesss/1
  def show

    #render json: @business
  end

  #Edit /businesss/:id/
  def edit

  end

  # POST /businesss
  def create
    @business = Business.new(business_params)
    @business.user_id = @current_user.id
    respond_to do |format|
      if @business.save
        format.html { redirect_to account_path, notice: 'Business was successfully created.' }
        format.json { render :show, status: :created, location: @business }
        @current_user.business!
      else
        format.html { render :new }
        format.json { render json: @business.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /businesss/1
  def update
    if @business.update(business_params)
      redirect_to account_path
      #redirect_to request_user_show_path(@request)
    else
      redirect_back_or_default(root_path, :alert => "Something went wrong!.")
    end
  end

  # DELETE /businesss/1
  def destroy

    @business.destroy
    redirect_to business_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_business
    @business = Business.find(params[:business_id])
  end

  def business_only

    @business = Business.find_by(:user_id => @current_user.id)
    if @business
      redirect_back_or_default(root_path, :alert => t("messages.business.permission_failed"))
    end

  end


  # Only allow a trusted parameter "white list" through.
  def business_params
    if Rails::VERSION::MAJOR < 4
      params[:business].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:business).permit(Business.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end