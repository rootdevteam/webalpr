class Users::Businesses::TollDevicesController < ApplicationController
  before_action :set_business, only: [:create, :new]
  before_action :set_toll_device, only: [:destroy]
  before_action :authenticate_user!

  # GET /toll_device
  def index
    @toll_devices = TollDevice.where(:user_id => @current_user.id)
  end

  def new
    @toll_device = TollDevice.new
  end

  # GET /toll_device/1
  def show
  end

  #Edit /toll_device/:id/
  def edit

  end

  # POST /toll_device
  def create
    @toll_device = TollDevice.new(toll_device_params)
    @toll_device.business_id = @business.id
    @toll_device.device_token = encode(toll_device_id: @toll_device.id)
    respond_to do |format|
      if @toll_device.save
        format.html { redirect_to account_path, notice: 'Business was successfully created.' }
        format.json { render :show, status: :created, location: @toll_device }
      else
        format.html { render :new }
        format.json { render json: @toll_device.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /toll_device/1
  def update
    if @business.update(toll_device_params)
      redirect_to account_path
      #redirect_to request_user_show_path(@request)
    else
      redirect_back_or_default(root_path, :alert => "Something went wrong!.")
    end
  end

  # DELETE /toll_device/1
  def destroy
    @toll_device.destroy
    redirect_to business_path
  end

  def encode(payload, exp = 24.hours.from_now)
    payload[:exp] = exp.to_i
    JWT.encode(payload, ENV["SECRET_KEY_BASE"])
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_business
    @business = Business.find_by(:user_id => @current_user.id)
    if @business.present?
      @business
    else
      redirect_back_or_default(root_path, :alert => "You have to create business first!")
    end
  end

  def set_toll_device
    @toll_device = TollDevice.find_by(:business_id => @business.id)
  end

  # Only allow a trusted parameter "white list" through.
  def toll_device_params
    if Rails::VERSION::MAJOR < 4
      params[:toll_device].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:toll_device).permit(TollDevice.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end