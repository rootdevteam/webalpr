class Users::Businesses::AddressController < ApplicationController
  before_action :authenticate_user!
  before_action :set_business, only: [:create, :new]


  def index
    @addresses = Address.where(:business_id => @current_user.business.ids)
  end

  def new
    @address = Address.new
  end

  def show

  end

  def create
    @address = Address.new(address_params)
    @address.business_id = @current_user.business.first.id
    respond_to do |format|
      if @address.save
        format.html { redirect_to account_path, notice: 'Address was successfully created.', locals: { myclass: 'alert alert-success' } }
        format.json { render :show, status: :created, location: @address }
      else
        format.html { render :new }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit

  end

  def update
    if @business.update(address_params)
      redirect_to account_path
      #redirect_to request_user_show_path(@request)
    else
      redirect_back_or_default(root_path, :alert => "Something went wrong!.")
    end
  end

  private

  def set_address
    @address = Address.find(params[:address_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_business
    @business = Business.find_by(:user_id => @current_user.id)
    if @business.present?
      @business
    else
      redirect_back_or_default(root_path, :alert => "You have to create business first!")
    end
  end

  def address_params
    if Rails::VERSION::MAJOR < 4
      params[:address].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:address).permit(Address.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end