class Users::Businesses::ContactsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_business, only: [:create, :new]


  def index
    @contacts = Contact.where(:business_id => @current_user.business.ids)
  end

  def new
    @contact = Contact.new
  end

  def show

  end

  def create
    @contact = Contact.new(contact_params)
    @contact.business_id = @current_user.business.first.id
    respond_to do |format|
      if @contact.save
        format.html { redirect_to account_path, notice: 'contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit

  end

  def update
    if @contact.update(contact_params)
      redirect_to account_path
      #redirect_to request_user_show_path(@request)
    else
      redirect_back_or_default(root_path, :alert => "Something went wrong!.")
    end
  end

  private

  def set_contact
    @contact = Contact.find(params[:contact_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_business
    @business = Business.find_by(:user_id => @current_user.id)
    if @business.present?
      @business
    else
      redirect_back_or_default(root_path, :alert => "You have to create business first!")
    end
  end

  def contact_params
    if Rails::VERSION::MAJOR < 4
      params[:contact].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:contact).permit(Contact.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end