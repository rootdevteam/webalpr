class Users::DriversController < ApplicationController
  before_action :set_driver, only: [ :update, :destroy]
  before_action :driver_only, only: [:new, :create]
  before_action :authenticate_user!

  # GET /drivers
  def index
    @drivers = Driver.where(:is_available => true)
  end


  def new
    @driver = Driver.new
  end


  # GET /drivers/1
  def show

  end

  #Edit /drivers/:id/
  def edit

  end

  # POST /drivers
  def create

    @driver = Driver.new(driver_params)
    @driver.user_id = @current_user.id
    respond_to do |format|
      if @driver.save
        format.html { redirect_to account_path, notice: 'driver was successfully created.' }
        format.json { render :show, status: :created, location: @driver }
        @current_user.driver!
      else
        redirect_back_or_default(root_path, :alert => "Something went wrong!.")
      end
    end
  end

  # PATCH/PUT /drivers/1
  def update
    if @driver.update(driver_params)
      redirect_to drivers_path
      #redirect_to request_user_show_path(@request)
    else
      redirect_back_or_default(root_path, :alert => "Some thing wrong!.")
    end
  end

  # DELETE /drivers/1
  def destroy

    @driver.destroy
    redirect_to drivers_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_driver
    @driver = Driver.find(params[:driver_id])
  end


  def driver_only

    @driver = Driver.find_by(:user_id => current_user.id)
    if @driver
      redirect_back_or_default(root_path, :alert => "Sorry! You do not have permission to access this content!.")
    end

  end


  # Only allow a trusted parameter "white list" through.
  def driver_params
    if Rails::VERSION::MAJOR < 4
      params[:driver].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:driver).permit(Driver.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end