class Users::ParkingPaymentsController < ApplicationController
  before_action :authenticate_user!

  def index
    @parking_payments = ParkingPayment.all
  end

  def user_payments
    @parking_payments = ParkingPayment.where(:users_id => @current_user.id)
  end

  def new
    @parking_payment = ParkingPayment.new
    @vehicles = Vehicle.all
  end

  def create
    @parking_payment = ParkingPayment.new(payment_params)
    respond_to do |format|
      if @parking_payment.save
        format.html { redirect_to root_path, notice: 'Parking payment was successfully created.' }
      else
        redirect_back_or_default(parking_payments_path, :alert => "Something went wrong!.")
      end
    end

  end

  def submit

  end

  def edit

  end

  def update

  end

  private


  def set_payment
    @parking_payment = ParkingPayment.find(params[:parking_payment_id])
  end


  def payment_params
    if Rails::VERSION::MAJOR < 4
      #In here should be given object name
      params[:parking_payment].permit(:_destroy)
    else
      params.require(:parking_payment).permit(:hours, :status, :token, :customer_id, :payment_gateway, :amount)
    end
  end
end