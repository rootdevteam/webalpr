class Users::ConfirmationsController < Devise::ConfirmationsController
  # GET /resource/confirmation/new
  def new
    self.resource = resource_class.new
  end

  # POST /resource/confirmation
  def create
    self.resource = resource_class.send_confirmation_instructions(resource_params)
    yield resource if block_given?

    if successfully_sent?(resource)
      respond_with({}, location: after_resending_confirmation_instructions_path_for(resource_name))
    else
      respond_with(resource)
    end
  end

  # GET /resource/confirmation?confirmation_token=abcdef
  # Using DeviseTokenAuth Show Method to support redirect to App
  def show
    @resource = resource_class.confirm_by_token(params[:confirmation_token])

    if @resource && @resource.id
      expiry = nil
      if defined?(@resource.sign_in_count) && @resource.sign_in_count > 0
        expiry = (Time.zone.now + 1.second).to_i
      end

      client_id, token = @resource.create_token expiry: expiry

      sign_in(@resource)
      @resource.save!

      yield @resource if block_given?

      redirect_header_options = { account_confirmation_success: true }
      redirect_headers = build_redirect_headers(token,
                                                client_id,
                                                redirect_header_options)

      # give redirect value from params priority
      @redirect_url = params[:redirect_url]

      # fall back to default value if provided
      @redirect_url ||= DeviseTokenAuth.default_confirm_success_url

      set_flash_message!(:success, :confirmed)

      redirect_to(@resource.build_auth_url(@redirect_url, redirect_headers))
    else
      raise ActionController::RoutingError, 'Not Found'
    end
  end

  def build_redirect_headers(access_token, client, redirect_header_options = {})
    {
        DeviseTokenAuth.headers_names[:"access-token"] => access_token,
        DeviseTokenAuth.headers_names[:"client"] => client,
        :config => params[:config],

        # Legacy parameters which may be removed in a future release.
        # Consider using "client" and "access-token" in client code.
        # See: github.com/lynndylanhurley/devise_token_auth/issues/993
        :client_id => client,
        :token => access_token
    }.merge(redirect_header_options)
  end

  protected

  # The path used after resending confirmation instructions.
  def after_resending_confirmation_instructions_path_for(resource_name)
    is_navigational_format? ? new_session_path(resource_name) : '/'
  end

  # The path used after confirmation.
  def after_confirmation_path_for(resource_name, resource)
    if signed_in?(resource_name)
      signed_in_root_path(resource)
    else
      new_session_path(resource_name)
    end
  end

  def translation_scope
    'devise.confirmations'
  end
end
