class Users::VehiclesController < ApplicationController
  before_action :set_vehicle, only: [:show, :edit, :update, :destroy]
  #before_action :driver_only, only: [:new, :create]
  before_action :authenticate_user!

  # GET /vehicles
  def index
    @vehicles = Vehicle.where(:user_id => current_user.id)
  end

  def new
    @vehicle = Vehicle.new
  end


  # GET /vehicles/1
  def show
    #render json: @vehicle
  end

  #Edit /vehicles/:id/
  def edit

  end

  # POST /vehicles
  def create
      @vehicle = Vehicle.new(vehicle_params)
      @vehicle.user_id = @current_user.id
      if @vehicle.save
        redirect_to vehicles_path
      else
        redirect_to vehicle_new_path
      end
  end

  # PATCH/PUT /vehicles/1
  def update
    if @vehicle.update(vehicle_params)
      redirect_to vehicles_path
      #redirect_to request_user_show_path(@request)
    else
      redirect_back_or_default(root_path, :alert => "Some thing wrong!.")
    end
  end

  # DELETE /vehicles/1
  def destroy

    @vehicle.destroy
    redirect_to vehicles_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_vehicle
    @vehicle = Vehicle.find(params[:vehicle_id])
  end


  def driver_only
    unless @current_user.driver?
      redirect_back_or_default(root_path, :alert => "Sorry! You do not have permission to access this content!.")
    end
  end


  # Only allow a trusted parameter "white list" through.
  def vehicle_params
    if Rails::VERSION::MAJOR < 4
      params[:vehicle].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:vehicle).permit(Vehicle.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end
