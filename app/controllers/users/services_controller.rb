class Users::ServicesController < ApplicationController
  before_action :set_service, only: [:show, :edit, :update, :destroy]
  before_action :admin_only, only: [:new, :create]

  # GET /services
  def index
    @services = Service.where(:is_available => true)
  end


  def new
    @service = Service.new
  end


  # GET /services/1
  def show

  end

  #Edit /services/:id/
  def edit

  end

  # POST /services
  def create
    @service = Service.new(service_params)
    @service.user_id = @current_user.id
    respond_to do |format|
      if @service.save
        format.html { redirect_to account_path, notice: 'service was successfully created.' }
        format.json { render :show, status: :created, location: @service }
      else
        redirect_back_or_default(root_path, :alert => "Something went wrong!.")
      end
    end
  end

  # PATCH/PUT /services/1
  def update
    if @service.update(service_params)
      redirect_to services_path
    else
      redirect_back_or_default(root_path, :alert => "Some thing wrong!.")
    end
  end

  # DELETE /services/1
  def destroy

    @service.destroy
    redirect_to services_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_service
    @service = Service.find(params[:service_id])
  end


  def admin_only
    unless @current_user.admin?
      redirect_back_or_default(root_path, :alert => "Sorry! You do not have permission to access this content!.")
    end
  end


  # Only allow a trusted parameter "white list" through.
  def service_params
    if Rails::VERSION::MAJOR < 4
      params[:service].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:service).permit(Service.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end