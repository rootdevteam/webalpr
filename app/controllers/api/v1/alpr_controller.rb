class Api::V1::AlprController < ApiController
  require 'open-uri'
  require 'rmagick'
  require 'json'

  before_action :authenticate_user!, except: [:analyze]

  def analyze

    # Get the image url
    @url = params[:url]

    @result = TollDevice.analyze(@url)
    render json: {
        status: 200,
        message: "Successfully analyzed the image!",
        result: @result
    }.to_json

  end

end