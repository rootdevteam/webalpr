# Override the AuthTokenRegistrationsController
class Api::V1::DeviseTokenAuth::RegistrationsController < ::DeviseTokenAuth::RegistrationsController
  # Disable the authenticate user so user can sign up
  before_action :authenticate_user!, except: [:create]
  skip_before_action :verify_authenticity_token, only: [:create]

  # Prevent registration parameter from being passed
  # Unpermitted parameter: registration
  wrap_parameters format: []
end