# Override the AuthTokenSessionsController
class Api::V1::DeviseTokenAuth::SessionsController < ::DeviseTokenAuth::SessionsController
  protect_from_forgery with: :null_session, only: Proc.new { |c| c.request.format.json? }
  # Disable the authenticate user for user can sign in and validate token
  before_action :authenticate_user!, except: [:new, :create, :validate_token]
  after_action :set_stripe_id, only: [:create]

  # Prevent session parameter from being passed
  # Unpermitted parameter: session
  wrap_parameters format: []

  # Override Devise Sign In method to work with login field, using username & email
  def create
    # Check
    field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first

    @resource = nil
    if field
      q_value = get_case_insensitive_field_from_resource_params(field)

      # OVERRIDE LOGIN FIELD USING EMAIL AND USERNAME
      if field == :login
        @resource = resource_class.where("email = ? OR username = ?", q_value, q_value).first
      else
        @resource = find_resource(field, q_value)
      end
      # OVERRIDE LOGIN FIELD USING EMAIL AND USERNAMED
    end

    if @resource && valid_params?(field, q_value) && (!@resource.respond_to?(:active_for_authentication?) || @resource.active_for_authentication?)
      valid_password = @resource.valid_password?(resource_params[:password])
      if (@resource.respond_to?(:valid_for_authentication?) && !@resource.valid_for_authentication? { valid_password }) || !valid_password
        return render_create_error_bad_credentials
      end
      @client_id, @token = @resource.create_token
      @resource.save

      sign_in(:user, @resource, store: false, bypass: false)

      yield @resource if block_given?

      render_create_success
    elsif @resource && !(!@resource.respond_to?(:active_for_authentication?) || @resource.active_for_authentication?)
      if @resource.respond_to?(:locked_at) && @resource.locked_at
        render_create_error_account_locked
      else
        render_create_error_not_confirmed
      end
    else
      render_create_error_bad_credentials
    end
  end

  # Set stripe ID
  def set_stripe_id
    if @resource && @resource.stripe_customer_id.nil?
      customer = Stripe::Customer.create({
                                             email: @resource.email,
                                             name: @resource.username
                                         })
      unless customer.id.nil?
        @resource.stripe_customer_id = customer.id
        @resource.save
      end
    end
  end

end