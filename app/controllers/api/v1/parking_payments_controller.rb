class Api::V1::ParkingPaymentsController < ApiController
  before_action :authenticate_user!
  # before_action :prepare_new_payment, only: [:paypal_create_payment, :paypal_create_subscription]
  # require 'json'

  def user_payments
    @parking_payments = ParkingPayment.where(:user_id => @current_user.id)
  end

  def submit
    @payment = nil
    # Check which payment gateway is used
    puts "PAYMENT GATEWAY: " +  params[:payment_gateway]
    if params[:payment_gateway] == 'stripe'
      prepare_new_payment
      @payment.payment_gateway = "stripe"
      Payments::Stripe.execute(payment: @payment, user: @current_user)
    elsif  payment_params[:payment_gateway] == 'Paypal'
      # @order = Payments::Stripe.finish(payment_params[:charge_id])
    end
  ensure
    if @payment.save
      puts "NEW PARKING PAYMENT WAS CREATED"
      if @payment.paid?
        render json: {
            status: 200,
            message: "Payment was executed successfully!",
            payment: @payment.to_json
        }
      elsif @payment.failed? !@payment.error_message.blank?
        render json: {
            status: 400,
            message: "Payment execution failed!",
            errors: @error_message
        }
      end
    end
  end

  private
  # Initialize a new payment and and set its params.
  def prepare_new_payment
    @payment = ParkingPayment.new
    @payment.user_id = @current_user.id
    @payment.vehicle_id = TollDeviceRequestHistory.get_request_history(params[:request_history]).vehicle_id
    @payment.hours = TollDeviceRequestHistory.get_request_history(params[:request_history]).hours
    @payment.request_history = TollDeviceRequestHistory.get_request_history(params[:request_history]).id
    @payment.amount = TollDeviceRequestHistory.payment_amount(params[:request_history])
    @payment.business_id = TollDeviceRequestHistory.request_business(params[:request_history])
    @payment.token = params[:card_token]
    puts "CARD TOKEN" + @payment.token
    puts "PAYMENT AMOUNT:  " + @payment.amount.to_s
  end

  # # Check default payment
  # def check_default(card_token)
  #   if card_token == "default"
  #     @token = @current_user.stripe_customer_id
  #   else
  #     @token = Stripe::Customer.update(@current_user.stripe_customer_id, { source: card_token}).id
  #   end
  # end

  # def payment_params
  #   if Rails::VERSION::MAJOR < 4
  #     #In here should be given object name
  #     params[:parking_payment].permit(:_destroy)
  #   else
  #     params.require(:parking_payment).permit(:token, :payment_gateway)
  #   end
  # end
end