class Api::V1::UsersController < ApiController
  before_action :authenticate_user!, except: [:auth_check_confirmable]

  # App Auto Devise Confirmable support
  def auth_check_confirmable
    @user_login = params[:login]
    @user = User.where(["lower(username) = :login OR lower(email) = :login", { :login => @user_login.downcase }]).first
    if @user
      render json: {
          status: 200,
          message: "Response successfully sent!",
          confirmed: @user.confirmed?
      }.to_json
    else
      render json: {
          status: 400,
          message: "Response successfully sent with ERRORS!",
          errors: "User doesn't exist!"
      }.to_json
    end
  end

  # Upgrading user profile and giving driver/business roles
  def upgrade(param)

    if param == "driver"
      @driver = Driver.new(driver_params)
      @driver.user_id = @current_user.id
      respond_to do |format|
        if @driver.save
          format.html { redirect_to account_path, notice: 'driver was successfully created.' }
          format.json { render :show, status: :created, location: @driver }
          @current_user.driver!
        else
          redirect_back_or_default(root_path, :alert => "Something went wrong!.")
        end
      end
    elsif param == "business"
      @business = Business.new(business_params)
      @business.user_id = @current_user.id
      respond_to do |format|
        if @business.save
          format.html { redirect_to business_path, notice: 'business was successfully created.' }
          format.json { render :show, status: :created, location: @business }
          @current_user.business!
        else
          format.html { render :new }
          format.json { render json: @business.errors, status: :unprocessable_entity }
        end
      end
    else
        flash[:notice] = "Bad role!"
    end
  end


  # Only allow a trusted parameter "white list" through.
  def driver_params
    if Rails::VERSION::MAJOR < 4
      params[:driver].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:driver).permit(Driver.new.attributes.keys - protected_attrs, :_destroy)
    end
  end


  # Only allow a trusted parameter "white list" through.
  def business_params
    if Rails::VERSION::MAJOR < 4
      params[:business].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:business).permit(Business.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end