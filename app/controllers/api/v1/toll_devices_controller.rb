class Api::V1::TollDevicesController < ApiController
  #before_action :business_only, only: [:index, :create]
  require 'json'

  # GET /Toll Device
  def index
    @business = Business.find_by(:user_id => @current_user.id)
    if @business.nil?
      render json: {
          status: 400,
          message: "TOLL DEVICE NOT FOUND!",
          toll_devices: @toll_devices
      }.to_json
    else
      @toll_devices = TollDevice.where(:business_id => @business.id)
      render json: {
          status: 200,
          message: "Toll device found!",
          toll_devices: @toll_devices
      }.to_json
    end
  end

  def get_vehicle_history
    puts "Step 1: TollDevice.get_toll_device(params[:device_token])"
    @toll_device = TollDevice.get_toll_device(params[:device_token])
    puts "Step 1: Completed tolldevice = " + @toll_device.to_s


    puts "Step 2: TollDevice.get_toll_device(params[:device_token])"
    @vehicle = Vehicle.get_vehicle(params[:url])
    puts "Step 2: Completed vehicle =  " + @vehicle.to_s

    @time_stayed = params[:hours].to_i

    puts "Step 3: Business.get_business(@toll_device)"
    @business = Business.get_business(@toll_device)
    puts "Step 3: Completed business =  " + @business.to_s

    puts "Step 4: User.get_vehicle_owner(@vehicle)"
    @users = User.get_vehicle_owner(@vehicle)
    puts "Step 4: Completed user =  " + @users.to_s

    if @toll_device.present? and @vehicle.present? and @business.present? and @users.present?
      puts "Step 5: in if statement"

      request_history = TollDeviceRequestHistory.get_last_history(@vehicle)
      if request_history.nil?
        puts "CREATING NEW REQUEST HISTORY!"
        @request_history = TollDeviceRequestHistory.create(start_time: get_current_time, vehicle_id: @vehicle.id, toll_device_id: @toll_device.id)
        Device.notify_device(@user.id, @business, @request_history)
      elsif request_history.end_time.nil? and request_history.accepted_by.present?
        puts "UPDATING THE EXISTING ONE SETTING END TIME AND HOURS"
        time_passed = ((DateTime.now.to_time.utc - request_history.start_time)/60 + 20.minutes)
        if time_passed > 20
          request_history.update(:end_time => get_current_time)
          hours = ((((request_history.end_time - request_history.start_time)/60)+(@time_stayed*60).minutes))/3600
          request_history.update(:hours => hours.truncate(2))
          @request_history = request_history
          Device.notify_device(@user.id, @business, @request_history)
        else
          puts "RETURNING THE NEW REQUESTED HISTORY"
          @request_history = request_history
          Device.notify_device(@user.id, @business, @request_history)
        end
      elsif !check_payment(request_history) and request_history.accepted_by.present?
        @request_history = request_history
        Device.notify_device(@user.id, @business, @request_history)
      elsif request_history.declined_by.present? or request_history.accepted_by.present?
        puts "REQUEST HISTORY WAS EITHER DECLINED OR ACCEPTED, CREATING NEW!"
        @request_history = TollDeviceRequestHistory.create(start_time: get_current_time, vehicle_id: @vehicle.id, toll_device_id: @toll_device.id)
        Device.notify_device(@user.id, @business, @request_history)
      else
        puts "RETURNING INITIAL!"
        @request_history = request_history
      end

      render json: {
          status: 200,
          message: "Successfully analyzed the image!",
          # toll_device: @toll_device.to_json,
          request_history: @request_history.id,
          #business: @business.to_json,
          vehicle: @vehicle.id,
          #user: @user.to_json
      }.to_json
    else
      puts "Step 5: failed if statement"

      render json: {
          status: 400,
          message: "Request failed!",
          errors: "GET VEHICLE HISTORY ERROR!"
      }.to_json
    end
  end



  # PATCH/PUT /toll_devices/:toll_device
  def create
    @business = Business.find_by(:user_id => @current_user.id)
    unless @business.nil?
      @toll_device = TollDevice.find_or_create_by(:business_id => @business.id)
      if @toll_device.update(toll_device_params)
        render json: {
            status: 200,
            message: t("messages.toll_device.success"),
            params: toll_device_params
        }.to_json
      else
        ApplicationController.render_error("Toll device update failed!")
      end
    else
      ApplicationController.render_error("Business not found!")
    end
  end

  def get_current_time
    @time = DateTime.now
  end

  # Checking for business
  def business_only
    unless @current_user.business?
      ApplicationController.render_error("true")
    end
  end

  # Check user accepted/declined the notification
  def check_declined
    declined_by = []
    unless params[:request_history].nil?
      @request_history = TollDeviceRequestHistory.find(params[:request_history].to_i)
      @declined = params[:declined]
      @pushToken = params[:pushToken]
      if @declined
        declined_by.append(@current_user.id)
        @request_history.declined_by = declined_by.to_s
        puts "Declined"
        @request_history.save
      else
        if @request_history.accepted_by.present?
          puts "Message", @current_user.id
          message = "Somebody else has already accepted this request"
          if @pushToken.present?
            Device.push_message(message, @pushToken)
          end
        else
          @request_history.accepted_by = @current_user.id
          @request_history.save
          puts "Accepted", @current_user.id
        end
      end
    else
      ApplicationController.render_error("REQUEST HISTORY NO FOUND!")
    end
  end

  # Check payment execution
  def check_payment(request_history)
    @payment = ParkingPayment.find_by(:request_history=> request_history.id)
    if @payment.nil?
      puts "ERROR: PAYMENT CHECK FAILED!"
      false
    else
      puts "ERROR: PAYMENT CHECK SUCCESS!"
      true
    end
  end

  private
  # Never trust parameters from the scary internet, only allow the white list through.
  def toll_device_params
    if Rails::VERSION::MAJOR < 4
      #In here should be given object name
      params[:toll_device].permit(:_destroy)
    else
      params.require(:toll_device).permit(:name, :description, :model, :toll_type, :device_image_url, :price)
    end
  end

end