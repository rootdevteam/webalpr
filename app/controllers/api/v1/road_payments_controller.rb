class Api::V1::RoadPaymentsController < ApplicationController
  before_action :authenticate_user!

  def index
    @road_payments = RoadPayment.all
    render json: @road_payments
  end

  def user_payments
    @road_payments = RoadPayment.where(:users_id => @current_user.id)
    render json: @road_payments
  end

  def create

  end

  def show

  end

  def edit

  end

  def update

  end

  private


  def set_payment
    @road_payments = RoadPayment.find(params[:road_payment_id])
  end


  def payment_params
    if Rails::VERSION::MAJOR < 4
      params[:road_payment].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:road_payment).permit(Contact.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end