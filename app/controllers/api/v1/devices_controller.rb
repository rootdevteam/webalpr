class Api::V1::DevicesController < ApiController

  # PATCH/PUT /devices/:device
  def create
    @user = current_user
    @device = Device.find_or_create_by(:user_id => @user.id, :udid => device_params[:udid])

    if @device.update(device_params)

      render json: {
                 status: 200,
                 message: "Response successfully sent!",
                 params: device_params
             }.to_json
    else
      render json: {
                 status: 400,
                 message: "User device params failed to save!",
                 error: @device.errors
             }.to_json
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
  def device_params
    if Rails::VERSION::MAJOR < 4
      #In here should be given object name
      params[:device].permit(:_destroy)
    else
      params.require(:device).permit(:installationId, :expoVersion, :appOwnership, :systemName, :systemVersion, :deviceName, :deviceYearClass, :sessionId, :isDevice, :platform, :udid, :pushToken)
    end
  end
end
