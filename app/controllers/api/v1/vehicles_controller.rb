class Api::V1::VehiclesController < ApiController
  before_action :set_vehicle, only: [:show, :update, :destroy]
  #before_action :driver_only, only: [:create, :update]


  # GET /vehicles
  def index
    @vehicles = Vehicle.where(:user_id => @current_user.id)
    render json: {
               status: 200,
               message: "Response successfully sent!",
               vehicles: @vehicles
           }.to_json
  end

  # GET /vehicles/:id
  def show
    render json: {
               status: 200,
               message: "Response successfully sent!",
               vehicle: @vehicle
           }.to_json
  end

  # POST /vehicles/:id
  def create
    @vehicle = Vehicle.new(vehicle_params.merge(user_id: @current_user.id))
    if @vehicle.save
      render json: {
                 status: 200,
                 message: "Vehicle successfully created!",
                 vehicle: @vehicle
             }.to_json
    else
      render :json => @vehicle.errors.full_messages.to_json
    end
  end

  # PATCH/PUT /vehicles/:id
  def update
    if @vehicle.update(vehicle_params)
      render json: {
                 status: 200,
                 message: "Vehicle successfully updated!",
                 vehicle: @vehicle
             }.to_json
    else
      render json: {
                 status: 400,
                 message: "Vehicle was NOT updated!",
                 errors: @vehicle.errors.full_messages.to_json
             }.to_json
    end
  end

  # DELETE /vehicles/:id
  def destroy 
    if @vehicle.destroy
      render json: {
                 status: 200,
                 message: "Vehicle successfully deleted!"
             }.to_json
    else
      render json: {
                 status: 400,
                 message: "Vehicle was NOT deleted!",
                 errors: @vehicle.errors.full_messages.to_json
             }.to_json
    end
  end

  private

  #Set drivers only
  def driver_only
    unless @current_user.driver?
      redirect_to root_path, :alert => "Access denied."
    end
  end


  # Use callbacks to share common setup or constraints between actions.
  def set_vehicle
    @vehicle = Vehicle.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def vehicle_params
    params.require(:vehicle).permit(:name, :alpr_license_plate, :license_plate, :description, :model, :type, :vgyme_image_url, :user_id)
  end
end
