class Payments::Stripe
  INVALID_STRIPE_OPERATION = 'Invalid Stripe Operation'

  def self.execute(payment:, user:)
    # customer =  self.find_or_create_customer(token: payment.token, user: user)
    payment.customer_id = user.stripe_customer_id
    charge = self.execute_charge(payment: payment, user: user)
    unless charge&.id.blank?
      # If there is a charge with id, set payment paid.
      payment.charge_id = charge.id
      payment.set_paid
      puts "SUCCESS: STRIPE MESSAGE  " + payment.charge_id
    end
  rescue Stripe::StripeError => e
    # If a Stripe error is raised from the API,
    # set status failed and an error message
    payment.error_message = INVALID_STRIPE_OPERATION
    payment.set_failed
    puts "ERROR: STRIPE MESSAGE:  " + payment.error_message
  end



  def self.execute_charge(payment:, user:)
    charge = Stripe::Charge.create({
                                       amount: payment.amount.to_i,
                                       currency: 'usd',
                                       source: payment.token,
                                       description: "Charge for " + user.email.to_s,
                                       customer: user.stripe_customer_id
                                   })
    return charge
  end

  # private
  # def self.execute_charge(amount:, description:, card_token:)
  #   Stripe::Charge.create({
  #                             amount: amount.to_s,
  #                             currency: "usd",
  #                             description: description,
  #                             source: card_token
  #                         })
  # end
  #
  #
  # # Adding a new customer if not already exist
  # def self.find_or_create_customer(token:, user:)
  #   if token == "default"
  #     stripe_customer = Stripe::Customer.retrieve(user.stripe_customer_id).id
  #     puts "Stripe customer  " + stripe_customer.to_s
  #   elsif token.present?
  #     stripe_customer = Stripe::Customer.create({
  #                                             source: token,
  #                                             email: user.email,
  #                                         }).id
  #     user.stripe_customer_id = stripe_customer
  #     user.save
  #     puts "Stripe customer  " + stripe_customer
  #   end
  #   stripe_customer
  # end


  # def self.execute_subscription(plan:, customer:)
  #   customer.subscriptions.create({
  #                                     plan: plan
  #                                 })
  # end

  # def self.execute_charge(price_cents:, description:, card_token:)
  #   Stripe::Charge.create({
  #                             amount: price_cents.to_s,
  #                             currency: "usd",
  #                             description: description,
  #                             source: card_token
  #                         })
  # end
end