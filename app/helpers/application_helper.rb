module ApplicationHelper


  #Useful devise methods
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def set_flash_message(key, kind, options = {})
    message = find_message(kind, options)
    if options[:now]
      flash.now[key] = message if message.present?
    else
      flash[key] = message if message.present?
    end
  end

  # Sets flash message if is_flashing_format? equals true
  def set_flash_message!(key, kind, options = {})
    if is_flashing_format?
      set_flash_message(key, kind, options)
    end
  end

  # Get message for given
  def find_message(kind, options = {})
    options[:scope] ||= translation_scope
    options[:default] = Array(options[:default]).unshift(kind.to_sym)
    options[:resource_name] = resource_name
    options = devise_i18n_options(options)
    I18n.t("#{options[:resource_name]}.#{kind}", options)
  end

  # Override for local translations.
  def translation_scope
    "#{controller_name}"
  end

  def devise_i18n_options(options)
    options
  end

  # Check if flash messages should be emitted. Default is to do it on
  # navigational formats
  def is_flashing_format?
    is_navigational_format?
  end
  def is_navigational_format?
    Devise.navigational_formats.include?(request_format)
  end
end
