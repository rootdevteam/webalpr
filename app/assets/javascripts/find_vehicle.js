var FindVehicle = {
    toll_device: "",
    request_history: "",
    business: "",
    vehicle: "",
    user: "",
    find_vehicle: function (url, deviceToken, hours) {
        this.license_plate = "";
        $.ajaxSetup({"async": false});
        var data = new FormData();
        data.append('url', url);
        data.append('device_token', deviceToken);
        data.append('hours', hours);
        $.ajax({
            url: "/api/v1/get_vehicle_history",
            method: 'POST',
            dataType: 'json',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST' // For jQuery < 1.9
        }).done(function (data) {
            // For Single upload
            if(typeof data != "undefined")
            {
                FindVehicle.toll_device = data.toll_device;
                FindVehicle.request_history = data.request_history;
                FindVehicle.business = data.business;
                FindVehicle.vehicle = data.vehicle;
                FindVehicle.user = data.user;
                console.log( "Upload url result : " + data.status);
                console.log( "Upload url result : " + FindVehicle.request_history);
                console.log( "Upload url result : " + FindVehicle.business);
                console.log( "Upload url result : " + FindVehicle.vehicle);
                console.log( "Upload url result : " + FindVehicle.user);
            }
        }).fail(function (data) {
            console.log( "Vgy.Me Upload request failed! " + data.error);
        });
    }
};