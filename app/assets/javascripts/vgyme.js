var Vgyme = {
    user_key: "F3bIZUnPQzsCDaqs4vDRCc7JoO76VmU8",
    image_url: "",
    upload_file: function (file,fileInput,progressBar) {
        this.image_url = "";
        var data = new FormData();
        data.append('file', file);
        data.append('userkey',this.user_key);
        $.ajax({
            url: "https://vgy.me/upload",
            method: 'POST',
            dataType: 'json',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST' // For jQuery < 1.9
        }).done(function (data) {
            // For Single upload
            if(typeof data.image != "undefined")
            {
                Vgyme.image_url = data.image;

                //Get/Create the resource
                var resourceOutput = FileUpload.createHiddenField(fileInput,Vgyme.image_url);

                // Update View
                var vgymeimage = $('<img src="' + Vgyme.image_url + '" style="width:250px; height:250px" ></img>');
                resourceOutput.append(vgymeimage);

                // Update progress
                FileUpload.success(fileInput,progressBar,"Upload finished!");
                //console.log( "Upload url result : " + data.image);

            }
        }).fail(function (data) {
            FileUpload.failed(fileInput,progressBar,"Image upload failed!");
        });
    }
};