var Alpr = {
    license_plate: "",
    analyze: function (url) {
        this.license_plate = "";
        $.ajaxSetup({"async": false});
        var data = new FormData();
        data.append('url', url);
        $.ajax({
            url: "/api/v1/analyze",
            method: 'POST',
            dataType: 'json',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST' // For jQuery < 1.9
        }).done(function (data) {
            // For Single upload
            if(typeof data != "undefined")
            {
                Alpr.license_plate = data.result;
                console.log( "Upload url result : " + Alpr.license_plate);

            }
        }).fail(function (data) {
            //console.log( "Vgy.Me Upload request failed! " + data.error);
        });
    }
};