// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.

//= require jquery
//= require jquery_ujs
//= require bootstrap/bootstrap.min
//= require_tree .
//= require canvas/js/jquery
//= require canvas/js/plugins
//= require canvas/js/chart
//= require canvas/js/chart-utils
//= require canvas/js/moment
//= require canvas/js/daterangepicker
//= require canvas/include/rs-plugin/js/jquery.themepunch.tools.min
//= require canvas/include/rs-plugin/js/jquery.themepunch.revolution.min
//= require canvas/include/rs-plugin/js/extensions/revolution.extension.actions.min
//= require canvas/include/rs-plugin/js/extensions/revolution.extension.carousel.min
//= require canvas/include/rs-plugin/js/extensions/revolution.extension.kenburn.min
//= require canvas/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min
//= require canvas/include/rs-plugin/js/extensions/revolution.extension.migration.min
//= require canvas/include/rs-plugin/js/extensions/revolution.extension.navigation.min
//= require canvas/include/rs-plugin/js/extensions/revolution.extension.parallax.min
//= require canvas/include/rs-plugin/js/extensions/revolution.extension.slideanims.min
//= require canvas/include/rs-plugin/js/extensions/revolution.extension.video.min
//= require formvalidation/formvalidation.min
//= require formvalidation/plugins/bootstrap.min
//= require vimeo/player.min
//= require selectr/selectr.min
//= require plyr/plyr.min.js
//= require plyr/plyr.polyfilled.min.js
