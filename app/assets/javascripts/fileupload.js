function InitFileUpload() {

    $('.directFileUpload').find("input:file").each(function(i, elem) {
        var fileInput    = $(elem);
        var form         = $(fileInput.parents('form:first'));
        var submitButton = form.find('input[type="submit"]');
        var spanWrapper =  fileInput.closest("span.fileinput-button");
        var progressBar  = $("<div class='progress-bar bg-success' role='progressbar'></div>");
        var barContainer = $("<div class='progress'></div>").append(progressBar);
        // dynamic img resource update based on name, for other files do nothing


        $(fileInput).on('change', function(e) {
            var fileResourceName = fileInput.data('resource-name');
            //submitButton.prop('disabled', true);
            spanWrapper.before(barContainer); //Show loading bar 2nd
            progressBar.addClass('progress-bar-striped progress-bar-animated');
            progressBar.css('width', '10%').text("Uploading...");

            var fileSize = this.files[0].size / 1024 / 1024;
            var fileName = this.files[0].name;

            if (fileResourceName == 'image') {
                /*
                 if (hasExtension(fileName, ['.jpeg','.JPEG','.jpg','.JPG','.gif','.GIF','.png','.PNG'])) {
                 //Image file greater than 10 MB
                 if (fileSize >= 10){
                 alert('The size limit for images is 10 MB. This file is ' + (this.files[0].size/1024/1024).toFixed(2) + " MB. Please try to resize, or use a different image!");
                 e.stopImmediatePropagation();
                 }
                 } else {
                 alert('This file type is not supported for this field. Only PNG,GIF, and JPEG image formats are supported!');
                 e.stopImmediatePropagation();
                 }*/
            }

            //Get/Create the resource
            var resourceClass = ".fileinput-" + fileResourceName;
            var resourceOutput = form.find('div'+resourceClass);


            var fileInputFiles = fileInput[0].files;
            var fileCount = fileInputFiles.length;
            var currentfileCount = 0;

            var uploadFiles = function(){

                var currentFile = fileInputFiles[currentfileCount];

                if (fileResourceName == 'image') {
                    //Convert image to Base64
                    var reader  = new FileReader();
                }
                if(currentfileCount+1 == fileCount){
                    clearInterval(xInt);
                    if (fileResourceName == 'image') {

                        // create hidden field
                        reader.onload = function(event) {
                            var image_base = event.target.result;

                            var input = $("<input />", { type:'hidden', name: fileInput.attr('name'), value: image_base });

                            form.append(input);
                            resourceOutput.empty();
                            var lp_warning = $('#vehicle_license_plate').prev();
                            lp_warning.text("Please check your license plate!!!");
                            lp_warning.css('color','red');
                            var base64_image = $('<img src="' + image_base + '" style="width:150px; height:150px" ></img>');
                            resourceOutput.append(base64_image);


                            //var deviceToken="eyJhbGciOiJIUzI1NiJ9.eyJ0b2xsX2RldmljZV9pZCI6bnVsbCwiZXhwIjoxNTc1NjY4ODIwfQ.EA7l6hepCq4rMh5QgY8ODcCjdk-rWQ0OiKxeNsM38kw";
                            //FindVehicle.find_vehicle(image_base, deviceToken, 2);
                            Alpr.analyze(image_base);
                            var license_plate = Alpr.license_plate;
                            if (license_plate != ""){

                                var alpr_lp_input = $('#vehicle_alpr_license_plate');
                                alpr_lp_input.val(license_plate);
                                var lp_input = $('#vehicle_license_plate');
                                lp_input.val(license_plate);

                            }
                            console.log("skds", license_plate);

                            if (image_base != "") // DataUrl image
                            {
                                //Update Progress Bar
                                var currentProgress = 100;
                                if (currentfileCount + 1 != fileCount) {
                                    var fileprogress = currentfileCount + 2; //Compensate for delay
                                    currentProgress = ((fileprogress) / (fileCount)).toFixed(2) * 100;
                                }
                                progressBar.css('width', currentProgress + '%');
                                progressBar.text("Done!");
                            } else { // DataUrl image fail
                                submitButton.prop('disabled', false);
                                progressBar.removeClass("bg-success");
                                progressBar.addClass("bg-danger");
                                progressBar.text("Upload Failed!");
                                clearInterval(xInt);
                            }


                        };
                        if (currentFile) {
                            reader.readAsDataURL(currentFile);
                        }
                    }
                }
                currentfileCount++;
            };
            var xInt = setInterval(uploadFiles, 1000);
        });
    });
}






