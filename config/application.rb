require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Webalpr
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Skip generating uneeded files for full scaffold
    config.generators do |generate|
      generate.assets false
      generate.helper false
      generate.test_framework  :test_unit, fixture: false
    end

    config_file = Rails.application.config_for(:application)
    config_file.each do |key,value|
      ENV[key] = value
    end unless config_file.nil?
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.i18n.available_locales = [:en, :ru]

    #Enable logging through Puma to main rails console
    config.logger = Logger.new(STDOUT)
    config.filter_parameters += [:url]

    #configure global pagination
    Kaminari.configure do |config|
      config.page_method_name = :per_page_kaminari
    end
  end
end
