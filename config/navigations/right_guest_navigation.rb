SimpleNavigation::Configuration.run do |navigation|
  navigation.selected_class = 'current'
  navigation.items do |primary|
    primary.dom_class = 'menu-right'
    primary.item :sign_up, "Join".html_safe,"#user_login_or_signup_mfp", { link_html: { :class => "standard_modal_setup user_sign_up_mfp", :"data-lightbox" => "inline" }, :unless => Proc.new {user_signed_in?} }
    primary.item :login, "My Account".html_safe,"#user_login_or_signup_mfp", { link_html: { :class => "standard_modal_setup user_sign_up_mfp", :"data-lightbox" => "inline" }, :unless => Proc.new {user_signed_in?} }

  end
end
