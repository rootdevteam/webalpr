SimpleNavigation::Configuration.run do |navigation|
  navigation.selected_class = 'active'
  navigation.items do |primary|
    primary.dom_class = 'navbar-nav'
    primary.item :home, t("home").html_safe, root_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    #primary.item :ships, t("ships").html_safe, ship_list_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    #primary.item :docks, t("docks").html_safe, dock_list_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    #primary.item :schedules, t("schedules").html_safe, schedules_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    primary.item :vehicles, "ANPR".html_safe, vehicles_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    primary.item :services, t("services").html_safe, services_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    primary.item :payments, t("payments").html_safe, parking_payments_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    primary.item :home, t("about").html_safe, about_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    primary.item :api, "API".html_safe, api_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }
    primary.item :language, t("language").html_safe, "#", html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link', :"data-toggle" => "modal", :"data-target"=>"#langModal"}



  #Signin & Signout
  if @current_user.user?
    primary.item :admin, t("admin").html_safe, rails_admin_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link last-right', :style => 'margin-right: 190px' }, :if => Proc.new { current_user.admin? }
    #primary.item :upgrade, t("upgrade").html_safe, upgrade_profile_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link last-right', :style => 'margin-right: 195px' }, :unless => Proc.new { current_user.admin? }
    primary.item :account, t("account").html_safe, account_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link last-right', :style => 'margin-right: 90px' }
    primary.item :logout, t("signout").html_safe, destroy_user_session_path, :method => 'delete',html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link last-right' }
  else
    primary.item :signup, t("signup").html_safe, new_user_registration_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link last-right', :style => 'margin-right: 90px' }
    primary.item :signin, t("signin").html_safe, new_user_session_path, html: { :class => 'nav-item', :id=>'', }, link_html: { :class => 'nav-link last-right'}

  end

  end
end



#Admin Button (checking for Admin)
#unless @current_user.blank?
 # if @current_user.admin?
  #  primary.item :admin, "Admin".html_safe, rails_admin_path, html: { :class => 'nav-item', :id=>''}, link_html: { :class => 'nav-link' }, :if => Proc.new { current_user.admin? }
  #end
#end