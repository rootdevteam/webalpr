# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.

  #Required for Canvas template, included at bottom of application.
  Rails.application.config.assets.precompile += %w( canvas/js/functions.js )
  #Add all required Vendor Asset Extensions.
  Rails.application.config.assets.precompile << /\.(?:svg|eot|woff|ttf|cur|png|gif)\z/