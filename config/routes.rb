Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  #Path to root page
  root to: 'public#index', as: 'root'
  get '/about', to: 'public#about', as: 'about'
  get '/api', to: 'public#api', as: 'api'

  #User handling routes
  devise_for :users, skip: [:sessions, :passwords, :confirmations, :registrations, :unlocks]

  match 'lang/:locale', to: 'visitors#change_locale', as: :change_locale, via: [:get]

  #Update user profile
  #get '/user/edit', to: 'users/users#edit', as: 'user_edit'
  put '/user/edit', to: 'users#update', as: 'user_update'

  ###############################################################
  devise_scope :user do

    get '/signin', to: 'users/sessions#new', as: 'new_user_session'
    post '/signin', to: 'users/sessions#create', as: 'user_session'
    get '/signout', to: 'users/sessions#sdestroy', as: 'secure_destroy_user_session'
    delete '/signout', to: 'users/sessions#destroy', as: 'destroy_user_session'

    # registrations
    get '/signup', to: 'users/registrations#new', as: 'new_user_registration'
    post '/signup', to: 'users/registrations#create', as: 'user_registration'

    # user accounts
    scope '/account' do
      # confirmation
      get '/verification', to: 'users/confirmations#verification_sent', as: 'user_verification_sent'
      get '/confirm', to: 'users/confirmations#show', as: 'user_confirmation'
      get '/confirm/resend', to: 'users/confirmations#new', as: 'new_user_confirmation'
      post '/confirm', to: 'users/confirmations#create'

      # passwords
      get '/reset-password', to: 'users/passwords#new', as: 'new_user_password'
      get '/reset-password/change', to: 'users/passwords#edit', as: 'edit_user_password'
      put  '/reset-password', to: 'users/passwords#update', as: 'user_password'
      post '/reset-password', to: 'users/passwords#create'

      # unlocks
      post '/unlock', to: 'users/unlocks#create', as: 'user_unlock'
      get '/unlock/new', to: 'users/unlocks#new', as: 'new_user_unlock'
      get '/unlock', to: 'users/unlocks#show'

      # settings & cancellation
      get '/cancel', to: 'users/registrations#cancel', as: 'cancel_user_registration'
      get '/settings', to: 'users/registrations#edit', as: 'edit_user_registration'
      put '/settings', to: 'users/registrations#update', as: 'update_user_registration'
      # account deletion
      delete '/delete', to: 'users/registrations#destroy', as: 'destroy_user_registration'
    end
  end
  #User account paths
  get '/account', to: 'users#account', as: 'account'
  get '/account/edit_news', to: 'users/contents#edit_news'
  put '/account/update_news', to: 'users/contents#update_news'
  #Upgrade profile
  get '/upgrade', to: 'users#upgrade', as: 'upgrade_profile'
  ################################################################


  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      mount_devise_token_auth_for 'User', at: 'auth',
                                  controllers: {
                                      sessions: 'api/v1/devise_token_auth/sessions',
                                      registrations: 'api/v1/devise_token_auth/registrations',
                                  }

      post '/auth_check_confirmable', to: 'users#auth_check_confirmable', as: 'auth_check_confirmable'

      resources :devices, only: [ :create ]
      resources :toll_devices, only: [ :index ]
      resources :locations, only: [ :create ]
      resources :vehicles, only: [ :index, :show, :create, :update, :destroy ]
      resources :docks, only: [ :index, :show, :create, :update, :destroy ]

      post '/analyze', to: 'alpr#analyze', as: 'alpr_analyze'
      post '/get_vehicle_history', to: 'toll_devices#get_vehicle_history', as: 'get_vehicle_history'
      post '/check_declined', to: 'toll_devices#check_declined', as: 'check_declined'
      post '/submit_payment', to: 'parking_payments#submit', as: 'submit_payment'
    end
  end

  #vehicles index path
  get '/vehicles', to: 'users/vehicles#index', as: 'vehicles'
  #ships index path
  get '/ships', to: 'users/ships#index', as: 'ships'
  #ships list path
  get '/ship/list', to: 'users/ships#list', as: 'ship_list'
  #docks index path
  get '/docks', to: 'users/docks#index', as: 'docks'
  #docks list path
  get '/dock/list', to: 'users/docks#list', as: 'dock_list'
  #shedules index path
  get '/schedules', to: 'users/schedules#index', as: 'schedules'
  #drivers index path
  get '/drivers', to: 'users/drivers#index', as: 'drivers'
  #services index path
  get '/services', to: 'users/services#index', as: 'services'
  # business index path
  get '/businesses', to: 'users/businesses/business#index', as: 'business'
  # payments index path
  get '/payments' => 'users/payments#index', as: 'payment'
  # address index path
  get '/address', to: 'users/businesses/address#index', as: 'address'
  # contacts index path
  get '/contacts', to: 'users/businesses/contacts#index', as: 'contacts'
  # contacts index path
  get '/business_payments', to: 'users/businesses/business_payments#index', as: 'business_payments'
  # contacts index path
  get '/parking_payments', to: 'users/parking_payments#index', as: 'parking_payments'
  # contacts index path
  get '/road_payments', to: 'users/road_payments#index', as: 'road_payments'


  scope :users do

    # Vehicle new routes
    get '/vehicle/new', to: 'users/vehicles#new', as: 'vehicle_new'
    post '/vehicle/new', to: 'users/vehicles#create', as: 'vehicle_create'

    resources :vehicles, only:[] do
      get '/edit', to: 'users/vehicles#edit', as: 'edit'
      put '/edit', to: 'users/vehicles#update', as: 'update'
      delete '/delete', to: 'users/vehicles#destroy', as: 'destroy'
    end

    #Drivers routes
    get '/driver/new', to: 'users/drivers#new', as: 'driver_new'
    post '/driver/new', to: 'users/drivers#create', as: 'driver_create'
    get '/show', to: 'users/drivers#show', as: 'driver_show'
    resources :drivers, only:[] do
      get '/edit', to: 'users/drivers#edit', as: 'edit'
      put '/edit', to: 'users/drivers#update', as: 'update'
      delete '/delete', to: 'users/drivers#destroy', as: 'destroy'
    end

    #Services routes
    get '/service/new', to: 'users/services#new', as: 'service_new'
    post '/service/new', to: 'users/services#create', as: 'service_create'
    get '/show', to: 'users/services#show', as: 'service_show'
    resources :services, only:[] do
      get '/edit', to: 'users/services#edit', as: 'edit'
      put '/edit', to: 'users/services#update', as: 'update'
      delete '/delete', to: 'users/services#destroy', as: 'destroy'
    end

    # parking payments routes
    get '/parking_payment/new', to: 'users/parking_payments#new', as: 'parking_payment_new'
    post '/parking_payment/new', to: 'users/parking_payments#create', as: 'parking_payment_create'

    resources :parking_payments, only:[] do
      get '/show', to: 'users/parking_payments#show', as: 'show'
      get '/edit', to: 'users/parking_payments#edit', as: 'edit'
      put '/edit', to: 'users/parking_payments#update', as: 'update'
      delete '/delete', to: 'users/drivers#destroy', as: 'destroy'
    end

    post '/parking_payments/submit', to: 'users/parking_payments#submit', as: 'payment_submit'

    scope :businesses do
      # business path
      get '/business/new', to: 'users/businesses/business#new', as: 'business_new'
      post '/business/new', to: 'users/businesses/business#create', as: 'business_create'

      resources :business, only:[] do
        #User business update and destroy paths
        get '/show', to: 'users/businesses/business#show', as: 'show'
        get '/edit', to: 'users/businesses/business#edit', as: 'edit'
        put '/edit', to: 'users/businesses/business#update', as: 'update'
        delete '/delete', to: 'users/businesses/business#destroy', as: 'destroy'
      end

      # toll_devices path
      get '/toll_device/new', to: 'users/businesses/toll_devices#new', as: 'toll_device_new'
      post '/toll_device/new', to: 'users/businesses/toll_devices#create', as: 'toll_device_create'

      resources :toll_devices, only:[] do
        #User business update and destroy paths
        get '/show', to: 'users/businesses/toll_devices#show', as: 'show'
        get '/edit', to: 'users/businesses/toll_devices#edit', as: 'edit'
        put '/edit', to: 'users/businesses/toll_devices#update', as: 'update'
        delete '/delete', to: 'users/businesses/toll_devices#destroy', as: 'destroy'
      end

      # Address new,create routes
      get '/address/new', to: 'users/businesses/address#new', as: 'address_new'
      post '/address/new', to: 'users/businesses/address#create', as: 'address_create'

      resources :address, only:[] do
        # Address show, edit, update, delete routes
        get '/show', to: 'users/businesses/address#show', as: 'show'
        get '/edit', to: 'users/businesses/address#edit', as: 'edit'
        put '/edit', to: 'users/businesses/address#update', as: 'update'
        delete '/delete', to: 'users/businesses/address#destroy', as: 'destroy'
      end

      # Contacts new,create routes
      get '/contact/new', to: 'users/businesses/contacts#new', as: 'contact_new'
      post '/contact/new', to: 'users/businesses/contacts#create', as: 'contact_create'

      resources :contacts, only:[] do
        # Address show, edit, update, delete routes
        get '/show', to: 'users/businesses/contacts#show', as: 'show'
        get '/edit', to: 'users/businesses/contacts#edit', as: 'edit'
        put '/edit', to: 'users/businesses/contacts#update', as: 'update'
        delete '/delete', to: 'users/businesses/contacts#destroy', as: 'destroy'
      end

      # Payment new,create routes
      get '/business_payment/new', to: 'users/businesses/business_payments#new', as: 'business_payment_new'
      post '/business_payment/new', to: 'users/businesses/business_payments#create', as: 'business_payment_create'

      resources :business_payments, only:[] do
        # Address show, edit, update, delete routes
        get '/show', to: 'users/businesses/business_payments#show', as: 'business_payment_show'
        get '/edit', to: 'users/businesses/business_payments#edit', as: 'business_payment_edit'
        put '/edit', to: 'users/businesses/business_payments#update', as: 'business_payment_update'
        delete '/delete', to: 'users/businesses/business_payments#destroy', as: 'business_payment_destroy'
      end
    end
  end

end
